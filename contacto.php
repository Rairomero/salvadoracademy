<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Contacto | Salvador Academy</title>

  <?php 
  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  include 'componentes/header.php'; 

  $ubicacion = "contacto";

  include 'componentes/navbar.php'; 
  //cabecera("inicio"); 

  ?>

  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Cuéntanos Tu Experiencia</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Divider: Contact -->
    <section class="divider">
      <div class="container">
        <div class="row pt-30">
          <div class="col-md-4">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box media bg-deep p-30 mb-20"> <a class="media-left pull-left flip" href="#"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
                  <div class="media-body">
                    <h5 class="mt-0">Salvador Academy Cecilio Acosta</h5>
                    <p>#Calle 67 Cecilio Acosta con Av. 8A Edif. Egeón, Maracaibo, Venezuela.</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box media bg-deep p-30 mb-20"> <a class="media-left pull-left flip" href="#"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
                  <div class="media-body">
                    <h5 class="mt-0">Salvador Academy Delicias Norte</h5>
                    <p>#Av. 15 C.C. Delicias Norte 178 Sector Delicias Norte, Maracaibo, Venezuela.</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box media bg-deep p-30 mb-20"> <a class="media-left pull-left flip" href="#"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
                  <div class="media-body">
                    <h5 class="mt-0">Salvador Academy Panamá</h5>
                    <p>#Sortis Hotel, Spa & Casino, calle 56 y 57 local LC-23A Obarrio, Panamá.</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box media bg-deep p-30 mb-20"> <a class="media-left pull-left flip" href="#"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
                  <div class="media-body">
                    <h5 class="mt-0">Salvador Academy Chile <strong>¡Muy Pronto!</strong></h5>
                    <p>Pronto en Santiago de Chile.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <h3 class="line-bottom mt-0 mb-30">Formulario de Contacto</h3>
            <p class="mb-20">Queremos saber si estamos haciendo de tu experiencia en Salvador Academy la mejor posible. Dinos que estamos haciendo y en que podemos mejorar, valoramos tus comentarios y sugerencias:</p>
            
            <!-- Contact Form -->
            <form id="contact_form" name="contact_form" class="" action="includes/sendmail.php" method="post">

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Nombre <small>*</small></label>
                    <input name="form_name" class="form-control" type="text" placeholder="Ingresa tu nombre" required="">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Correo Electrónico <small>*</small></label>
                    <input name="form_email" class="form-control required email" type="email" placeholder="Ingresa tu correo electrónico">
                  </div>
                </div>
              </div>
                
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Asunto <small>*</small></label>
                    <input name="form_subject" class="form-control required" type="text" placeholder="Ingresa un asunto">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Teléfono</label>
                    <input name="form_phone" class="form-control" type="text" placeholder="Ingresa tu teléfono">
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label>Mensaje</label>
                <textarea name="form_message" class="form-control required" rows="5" placeholder="Déjanos un mensaje"></textarea>
              </div>
              <div class="form-group">
                <input name="form_botcheck" class="form-control" type="hidden" value="" />
                <button type="submit" class="btn btn-dark btn-theme-colored btn-flat mr-5" data-loading-text="Por favor espera...">Enviar Mensaje</button>
                <button type="reset" class="btn btn-default btn-flat btn-theme-colored">Reiniciar</button>
              </div>
            </form>

            <!-- Contact Form Validation-->
            <script type="text/javascript">
              $("#contact_form").validate({
                submitHandler: function(form) {
                  var form_btn = $(form).find('button[type="submit"]');
                  var form_result_div = '#form-result';
                  $(form_result_div).remove();
                  form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                  var form_btn_old_msg = form_btn.html();
                  form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                  $(form).ajaxSubmit({
                    dataType:  'json',
                    success: function(data) {
                      if( data.status == 'true' ) {
                        $(form).find('.form-control').val('');
                      }
                      form_btn.prop('disabled', false).html(form_btn_old_msg);
                      $(form_result_div).html(data.message).fadeIn('slow');
                      setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                    }
                  });
                }
              });
            </script>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Call To Action -->
    <section class="bg-theme-color-2">
      <div class="container pt-10 pb-20">
        <div class="row">
          <div class="call-to-action">
            <div class="col-md-6">
              <h3 class="mt-5 mb-5 text-white vertical-align-middle"><i class="pe-7s-mail mr-10 font-48 vertical-align-middle"></i> SUSCRÍBETE A NUESTRO BOLETÍN</h3>
            </div>
            <div class="col-md-6">
              <!-- Mailchimp Subscription Form Starts Here -->
              <form id="mailchimp-subscription-form" class="newsletter-form mt-10">
                <div class="input-group">
                  <input type="email" value="" name="EMAIL" placeholder="Ingresa tu correo" class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-call">
                  <span class="input-group-btn">
                    <button data-height="45px" class="btn bg-theme-colored text-white btn-xs m-0 font-14" type="submit">Suscribir</button>
                  </span>
                </div>
              </form>
              <!-- Mailchimp Subscription Form Validation-->
              <script type="text/javascript">
                $('#mailchimp-subscription-form').ajaxChimp({
                    callback: mailChimpCallBack,
                    url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
                });

                function mailChimpCallBack(resp) {
                    // Hide any previous response text
                    var $mailchimpform = $('#mailchimp-subscription-form'),
                        $response = '';
                    $mailchimpform.children(".alert").remove();
                    if (resp.result === 'success') {
                        $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    } else if (resp.result === 'error') {
                        $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    }
                    $mailchimpform.prepend($response);
                }
              </script>
              <!-- Mailchimp Subscription Form Ends Here -->
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Divider: Google Map -->
    <!--section>
      <div class="container-fluid pt-0 pb-0">
        <div class="row">

          < Google Map HTML Codes >
          <div 
            id="map-canvas-multipointer"
            data-mapstyle="default"
            data-height="500"
            data-zoom="15"
            data-marker="images/map-marker.png">
          </div>
          < Google Map Javascript Codes >
          <script src="http://maps.google.com/maps/api/js?key=AIzaSyAYWE4mHmR9GyPsHSOVZrSCOOljk8DU9B4"></script>
          <script src="js/google-map-init-multilocation.js"></script>

        </div>
      </div>
    </section-->
  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <?php include 'componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="/componentes/js/custom.js"></script>

</body>
</html>