<?php

require_once('phpmailer/class.phpmailer.php');
require_once('phpmailer/class.smtp.php');

$mail = new PHPMailer();


//$mail->SMTPDebug = 3;                               // Enable verbose debug output
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'mail.salvadorhairdressing.com';                  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'academy@salvadoracademy.com';    // SMTP username
$mail->Password = 'Salvador_34';                         // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 465;                                    // TCP port to connect to

$mail->CharSet = 'UTF-8';

$message = "";
$status = "false";

if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    if( $_POST['form_name'] != '' AND $_POST['form_email'] != '' AND $_POST['form_subject'] != '' ) {

        $name = $_POST['form_name'];
        $email = $_POST['form_email'];
        $subject = $_POST['form_subject'];
        $phone = $_POST['form_phone'];
        $message = $_POST['form_message'];

        $subject = isset($subject) ? $subject : 'Nuevo Mensaje | Formulario de Contacto';

        $botcheck = $_POST['form_botcheck'];

        $toemail = 'academy@salvadoracademy.com'; // Your Email Address
        $toname = 'Salvador Academy'; // Your Name

        if( $botcheck == '' ) {

            $mail->SetFrom( $email , $name );
            $mail->AddReplyTo( $email , $name );
            $mail->AddAddress( $toemail , $toname );
            $mail->Subject = $subject;

            $variable = file_get_contents("../componentes/mails/mail-contact.html");
            $variable = str_replace("%name%", $name, $variable);
            $variable = str_replace("%email%", $email, $variable);
            $variable = str_replace("%subject%", $subject, $variable);
            $variable = str_replace("%phone%", $phone, $variable);
            $variable = str_replace("%message%", $message, $variable);

            //$name = isset($name) ? "Nombre: $name<br><br>" : '';
            //$email = isset($email) ? "Correo Electronico: $email<br><br>" : '';
            //$subject = isset($subject) ? "Asunto: $subject<br><br>" : '';
            //$phone = isset($phone) ? "Telefono: $phone<br><br>" : '';
            //$message = isset($message) ? "Mensaje: $message<br><br>" : '';

            $referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>Este formulario de contacto ha sido enviado desde: ' . $_SERVER['HTTP_REFERER'] : '';

            //$body = "$name $email $subject $phone $message $referrer";
            $body = "$variable $referrer";

            $mail->MsgHTML( $body );
            $sendEmail = $mail->Send();

            if( $sendEmail == true ):
                $message = 'Hemos recibido <strong>exitosamente</strong> tú mensaje y te respónderemos tan pronto como sea posible.';
                $status = "true";
            else:
                $message = 'El mensaje <strong>no fue enviado</strong> debido a un error inesperado. Por favor intenta más tarde<br/><br/><strong>Reason:</strong><br/>' . $mail->ErrorInfo . '';
                $status = "false";
            endif;
        } else {
            $message = 'Comportamiento <strong>inusual</strong>.! Mensaje no enviado.!';
            $status = "false";
        }
    } else {
        $message = 'Por favor <strong>rellena</strong> todos los campos e intenta nuevamente.';
        $status = "false";
    }
} else {
    $message = 'Un <strong>error inesperado</strong> ha ocurrido. Por favor intenta más tarde.';
    $status = "false";
}

$status_array = array( 'message' => $message, 'status' => $status);
echo json_encode($status_array);
?>