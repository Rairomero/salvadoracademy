<?php

function infoCursos($idhorario){ // Muestra la informacion del curso en el modal seccion cursos pag principal
	include 'estudios.php';
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT c.*, h.*, u.nombre AS location, u.direccion, u.pais FROM horarios h INNER JOIN cursos c ON h.cursoid=c.id INNER JOIN ubicacion u ON h.ubicacionid=u.id WHERE c.tipo=1 AND h.id_horario=".$idhorario."";

	$final = array();
	$resultado= "";
	$nombrecurso= "";
	$i = 0;
    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
        while ($rw = mysqli_fetch_array($search)) {
	        $turn = $rw['turno'];
	       	if ($turn == 'M') {
	       		$turn = 'Matutino';
	       	} elseif ($turn == 'V') {
	       		$turn = 'Vespertino';
	       	} elseif ($turn == 'F') {
	       		$turn = 'Fin de Semana';
	       	}
	       	$nombrecurso = $rw['nombre'];
	   		$inicio = date_format(date_create($rw['fechainicio']), 'd-m-Y');
	   		$fin = date_format(date_create($rw['fechafin']), 'd-m-Y');
    		$moneda = consultarMoneda($idhorario);
	    	$cupos = $rw['capacidad'] - $rw['inscritos'];
			$resultado = '<div class="row mb-20">
				          <div class="col-md-12">
						    <div class="col-md-12 text-center">
					          <img style="height: 250px;" src="/componentes/images/cursos/'.$rw['imagen'].'">
						    </div>
						  </div>
						</div>
						<div class="row mb-5">
						    <div class="col-md-12">
					          <div class="col-md-12"><b>Descripción:</b> '.ucfirst($rw['descripcion']).'</div>
						    </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						      <div class="col-md-6">
						        <b>Fecha de Inicio:</b> '.$inicio.'
						      </div>
						  	  <div class="col-md-6">
						        <b>Fecha Final:</b> '.$fin.'
						      </div>
						   </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						      <div class="col-md-6">
						        <b>Turno:</b> '.$turn.'
						      </div>
						      <div class="col-md-6">
						        <b>Horario:</b> '.$rw['horario'].'
						      </div>
						  </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						      <div class="col-md-6">
						        <b>Capacidad:</b> '.$rw['capacidad'].' Estudiantes
						      </div>
						      <div class="col-md-6">
						        <b>Cupos Disponibles:</b> '.$cupos.' Estudiantes
						      </div>
						  </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						      <div class="col-md-12">
						        <b>Academia: '.$rw['location'].'</b>
						      </div>
						  </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						    <div class="col-md-12">
						      <b>Dirección:</b> '.$rw['direccion'].'
						  	</div>
						  </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						  	<div class="col-md-12">
						  	  <b>Precio Total del Curso:</b>
						  	</div>
						  </div>
						  <div class="col-md-12">
						    <div class="col-md-6">
						      <b>De Contado:</b> '.number_format($rw['precio1'],2,",",".") ." ". $moneda.'
						    </div>
						    <div class="col-md-6">
						      <b>Dos Cuotas:</b> '.number_format($rw['precio2'],2,",",".") ." ". $moneda.'
						    </div>
						  </div>
						</div>';
	        }
	    } else {
	    	$final[0] = "Información no disponible";
	    	$final[1] = "<div class='alert alert-danger'>Lo sentimos, actualmente la información no se encuentra disponible intenta nuevamente.</div>";
	    }
	    $final[0] = $resultado;
	    $final[1] = $nombrecurso;
	   
	return json_encode($final);

}

function infoTalleres($idhorario){// Muestra la informacion del taller en el modal seccion cursos pag principal
	include 'estudios.php';
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT c.*, h.*, u.nombre AS location, u.direccion, u.pais FROM horarios h INNER JOIN cursos c ON h.cursoid=c.id INNER JOIN ubicacion u ON h.ubicacionid=u.id WHERE c.tipo=2 AND h.id_horario=".$idhorario."";

	$final = array();
	$resultado= "";
	$nombrecurso= "";
	$i = 0;
    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
        while ($rw = mysqli_fetch_array($search)) {
	        $turn = $rw['turno'];
	       	if ($turn == 'M') {
	       		$turn = 'Matutino';
	       	} elseif ($turn == 'V') {
	       		$turn = 'Vespertino';
	       	} elseif ($turn == 'F') {
	       		$turn = 'Fin de Semana';
	       	}
	       	$nombrecurso = $rw['nombre'];
	   		$inicio = date_format(date_create($rw['fechainicio']), 'd-m-Y');
	   		$fin = date_format(date_create($rw['fechafin']), 'd-m-Y');
    		$moneda = consultarMoneda($idhorario);
	    	$cupos = $rw['capacidad'] - $rw['inscritos'];
			$resultado = '<div class="row mb-20">
				          <div class="col-md-12">
						    <div class="col-md-12 text-center">
					          <img style="height: 250px;" src="/componentes/images/cursos/'.$rw['imagen'].'">
						    </div>
						  </div>
						</div>
						<div class="row mb-5">
						    <div class="col-md-12">
					          <div class="col-md-12"><b>Descripción:</b> '.ucfirst($rw['descripcion']).'</div>
						    </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						      <div class="col-md-6">
						        <b>Fecha de Inicio:</b> '.$inicio.'
						      </div>
						  	  <div class="col-md-6">
						        <b>Fecha Final:</b> '.$fin.'
						      </div>
						   </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						      <div class="col-md-6">
						        <b>Turno:</b> '.$turn.'
						      </div>
						      <div class="col-md-6">
						        <b>Horario:</b> '.$rw['horario'].'
						      </div>
						  </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						      <div class="col-md-6">
						        <b>Capacidad:</b> '.$rw['capacidad'].' Estudiantes
						      </div>
						      <div class="col-md-6">
						        <b>Cupos Disponibles:</b> '.$cupos.' Estudiantes
						      </div>
						  </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						      <div class="col-md-12">
						        <b>Academia: '.$rw['location'].'</b>
						      </div>
						  </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						    <div class="col-md-12">
						      <b>Dirección:</b> '.$rw['direccion'].'
						  	</div>
						  </div>
						</div>
						<div class="row mb-5">
						  <div class="col-md-12">
						  	<div class="col-md-12">
						  	  <b>Precio Total del Curso:</b>
						  	</div>
						  </div>
						  <div class="col-md-12">
						    <div class="col-md-6">
						      <b>De Contado:</b> '.number_format($rw['precio1'],2,",",".") ." ". $moneda.'
						    </div>
						    <div class="col-md-6">
						      <b>Dos Cuotas:</b> '.number_format($rw['precio2'],2,",",".") ." ". $moneda.'
						    </div>
						  </div>
						</div>';
	        }
	    } else {
	    	$final[0] = "Información no disponible";
	    	$final[1] = "<div class='alert alert-danger'>Lo sentimos, actualmente la información no se encuentra disponible intenta nuevamente.</div>";
	    }
	    $final[0] = $resultado;
	    $final[1] = $nombrecurso;
	   
	return json_encode($final);

}

function armarCursos(){ // Muestra los cursos en la seccion de cursos pag principal
	require "conexion.php";
	$dbh = dbconnlocal2();
    mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT c.*, h.*, u.nombre AS location, u.direccion, u.pais FROM horarios h INNER JOIN cursos c ON h.cursoid=c.id INNER JOIN ubicacion u ON h.ubicacionid=u.id WHERE c.tipo=1 AND h.capacidad>h.inscritos AND c.estado=0 AND h.fechafin >= CURDATE()";

	$resultado= "";
	$i = 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
	    while ($rw = mysqli_fetch_array($search)) {
	        $filtro = $rw['estado'];
	        $turn = $rw['turno'];
	        if ($turn == 'M') {
	       		$turn = 'Matutino';
	       	} elseif ($turn == 'V') {
	       		$turn = 'Vespertino';
	       	} elseif ($turn == 'F') {
	       		$turn = 'Fin de Semana';
	       	}
	        if ($filtro == 0) {
	        	$filtro = 'disp';
	        } elseif ($filtro == 1) {
	        	$filtro = 'fin';
	        }
	        $city = $rw['pais'];
	    	if ($city == 1) {
	        	$city = 'Maracaibo, Venezuela';
	        } elseif ($city == 2) {
	        	$city = 'Obarrio, Panamá';
	        }
			$resultado .= '<div class="gallery-item '.$filtro.'">
					          <div class="thumb">
					            <a type="button" data-tipo="c" data-texto="'.$rw['id_horario'].'" data-toggle="modal" data-target="#cursoModal" href="">
						          <img class="img-fullwidth" src="/componentes/images/cursos/'.$rw['imagen'].'" alt="img">
						          <div class="overlay-shade"><i class="fa fa-search icon-course"></i></div>
					            </a>
					          </div>
					          <div class="portfolio-description">
					            <h4 class="title"><a type="button" data-toggle="modal" data-target="#cursoModal" href="">'.$rw['nombre'].'</a></h4>
					            <small>'.$turn.'</small>
					            <h5 class="category mt-0 mb-0"><span>'.$city.'</span></h5>
					          </div>
					        </div>';
	    }
	} else {
	    $resultado = "<div class='alert alert-danger'>Lo sentimos, actualmente no disponemos de cursos para mostrar, te invitamos a ingresar nuevamente.</div>";
	}
	return $resultado;
}

function armarTalleres(){ // Muestra los talleres en la seccion de cursos pag principal
	require "conexion.php";
	$dbh = dbconnlocal2();
    mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT c.*, h.*, u.nombre AS location, u.direccion, u.pais FROM horarios h INNER JOIN cursos c ON h.cursoid=c.id INNER JOIN ubicacion u ON h.ubicacionid=u.id WHERE c.tipo=2 AND h.capacidad>h.inscritos AND c.estado=0 AND h.fechafin >= CURDATE()";

	$resultado= "";
	$i = 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
	    while ($rw = mysqli_fetch_array($search)) {
	        $filtro = $rw['estado'];
	        $turn = $rw['turno'];
	        if ($turn == 'M') {
	       		$turn = 'Matutino';
	       	} elseif ($turn == 'V') {
	       		$turn = 'Vespertino';
	       	} elseif ($turn == 'F') {
	       		$turn = 'Fin de Semana';
	       	}
	        if ($filtro == 0) {
	        	$filtro = 'disp';
	        } elseif ($filtro == 1) {
	        	$filtro = 'fin';
	        }
	        $city = $rw['pais'];
	    	if ($city == 1) {
	        	$city = 'Maracaibo, Venezuela';
	        } elseif ($city == 2) {
	        	$city = 'Obarrio, Panamá';
	        }
			$resultado .= '<div class="gallery-item '.$filtro.'">
					          <div class="thumb">
					            <a type="button" data-tipo="t" data-texto="'.$rw['id_horario'].'" data-toggle="modal" data-target="#cursoModal" href="">
						          <img class="img-fullwidth" src="/componentes/images/cursos/'.$rw['imagen'].'" alt="img">
						          <div class="overlay-shade"><i class="fa fa-search icon-course"></i></div>
					            </a>
					          </div>
					          <div class="portfolio-description">
					            <h4 class="title mb-0"><a type="button" data-toggle="modal" data-target="#cursoModal" href="">'.$rw['nombre'].'</a></h4>
					            <small>'.$turn.'</small>
					            <h5 class="category"><span>'.$city.'</span></h5>
					          </div>
					        </div>';
	    }
	} else {
	    $resultado = "<div class='alert alert-danger'>Lo sentimos, actualmente no disponemos de cursos para mostrar, te invitamos a ingresar nuevamente.</div>";
	}
	return $resultado;
}

function armarCyT(){ // Muestra los últimos 4 cursos y talleres agregados en la página de inicio
	require "conexion.php";
		$dbh = dbconnlocal2();
	    mysqli_set_charset($dbh, 'utf8');

	    if (!$dbh) {
		    die('Error en Conexión: ' . mysqli_error($dbh));
		    exit;
		}

	    $sql = "SELECT * FROM cursos INNER JOIN horarios ON cursos.id=horarios.cursoid WHERE estado=0 ORDER BY (fechainicio) DESC LIMIT 4";

	    $resultado= "";
	    $i = 0;
	    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	    $match = mysqli_num_rows($search);
	    if ($match > 0) {
	        while ($rw = mysqli_fetch_array($search)) {
	    		$detalle = $rw['descripcion'];
	    		$string = strip_tags($detalle);
				if (strlen($string) > 120) {
				    $stringCut = substr($string, 0, 120);
				    $endPoint = strrpos($stringCut, ' ');
				    $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
				    $string .= '...';
				}
				$tipocyt = $rw['tipo'];
				if ($tipocyt == 1) {
					$valorcurso = 'curso';
					$linkcurso = 'cursos';
				} else if ($tipocyt == 2) {
					$valorcurso = 'taller';
					$linkcurso = 'talleres';
				}
				$resultado .= '<div class="col-sm-6 col-md-3">
					              <div class="service-block bg-white">
					                <div class="thumb"> <img alt="img" src="/componentes/images/cursos/'.$rw['imagen'].'" class="img-fullwidth">
					                <h4 class="text-white mt-0 mb-0"><span class="price text-capitalize">'.$valorcurso.'</span></h4>
					                </div>
					                <div class="content text-left flip p-25 pt-0">
					                  <h4 class="line-bottom mb-10">'.$rw['nombre'].'</h4>
					                  <p>'.$string.'</p>
					                 <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="/programas/'.$linkcurso.'.php">Detalles</a>
					                </div>
					              </div>
					            </div>';
                        	        	
	        }
	    } else {
	    	echo "<div class='alert alert-danger'>Lo sentimos, actualmente no disponemos de cursos o talleres para mostrar, te invitamos a ingresar nuevamente.</div>";
	    }
	return $resultado;
}

function noticias(){ // Muestra las noticias desde la BD en la pag de inicio
		$dbh = dbconnlocal2();
	    mysqli_set_charset($dbh, 'utf8');

	    if (!$dbh) {
	        die('Error en Conexión: ' . mysqli_error($dbh));
	        exit;
	    }

	    $sql = "select * from noticias order by fecha desc limit 9";

	    $resultado= "";
	    $i = 0;
	    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	    $match = mysqli_num_rows($search);
	    if ($match > 0) {
	        while ($rw = mysqli_fetch_array($search)) {
	        	$date = $rw['fecha'];
	    		$date2 = date_create($date);
	    		$date3 = date_format($date2, 'd-m-Y');
	    		$cont = $rw['contenido'];
	    		$string = strip_tags($cont);
				if (strlen($string) > 160) {

				    // truncate string
				    $stringCut = substr($string, 0, 160);
				    $endPoint = strrpos($stringCut, ' ');

				    //if the string doesn't contain any space then it will cut without word basis.
				    $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
				    $string .= '...';
				}
				$noticiaid = $rw['id'];
				$resultado .= '<div class="item">
								<article class="post clearfix maxwidth600 mb-sm-30">
				                  <div class="entry-header">
				                    <div class="post-thumb thumb"> <img src="'.$rw['foto'].'" alt="" class="img-responsive img-fullwidth"> </div>
				                  </div>
				                  <div class="entry-content border-1px p-20">
				                    <h4 class="entry-title text-center mt-0 pt-0"><a href="#"><b>'.$rw['titulo'].'</b></a></h4>
				                    <p class="text-left mb-20 mt-15 font-13">'.$string.'</p>
				                    <a class="btn btn-flat btn-dark btn-theme-colored btn-sm pull-left" href="/noticias/noticia.php?id='.$noticiaid.'">Leer más</a>
				                    <ul class="list-inline entry-date pull-right font-12 mt-5">
				                      <li><span class="text-theme-colored">'.$date3.'</span></li>
				                    </ul>
				                    <div class="clearfix"></div>
				                  </div>
				                </article>
				              </div>';
                        	        	
	        }
	    } else {
	    	echo "No hay noticias recientes";
	    }
	return $resultado;

}

function noticiasrecientes(){ // Muestra las ultimas 2 noticias desde la BD como recomendacion
		$dbh = dbconnlocal2();
	    mysqli_set_charset($dbh, 'utf8');

	    if (!$dbh) {
	        die('Error en Conexión: ' . mysqli_error($dbh));
	        exit;
	    }

	    $sql = "select * from noticias order by fecha desc limit 2";

	    $resultado= "";
	    $i = 0;
	    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	    $match = mysqli_num_rows($search);
	    if ($match > 0) {
	        while ($rw = mysqli_fetch_array($search)) {
	    		$cont = $rw['contenido'];
	    		$string = strip_tags($cont);
				if (strlen($string) > 60) {

				    // truncate string
				    $stringCut = substr($string, 0, 60);
				    $endPoint = strrpos($stringCut, ' ');

				    //if the string doesn't contain any space then it will cut without word basis.
				    $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
				    $string .= '...';
				}
				$resultado .= '<article class="post media-post clearfix pb-0 mb-10">
			                    <a class="post-thumb" href="#"><img src="'.$rw['foto'].'" alt=""></a>
			                    <div class="post-right">
			                      <h5 class="mt-0"><a href="#"><b>'.$rw['titulo'].'</b></a></h5>
			                      <p>'.$string.'</p>
			                    </div>
			                  </article>';
                        	        	
	        }
	    } else {
	    	echo "No hay noticias recientes";
	    }
	return $resultado;

}

function infonoticia($idnoticia){ // Muestra la información de la noticia seleccionada
	require "conexion.php";
		$dbh = dbconnlocal2();
	    mysqli_set_charset($dbh, 'utf8');

	    if (!$dbh) {
	        die('Error en Conexión: ' . mysqli_error($dbh));
	        exit;
	    }

	    $sql = "select * from noticias where id=$idnoticia";

	    $resultado= "";
	    $i = 0;
	    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	    $match = mysqli_num_rows($search);
	    if ($match > 0) {
	        while ($rw = mysqli_fetch_array($search)) {
	        	setlocale(LC_ALL,"es_ES");
				$date = $rw['fecha'];
	    		$date2 = date_create($date);
	    		$date3 = date_format($date2, 'd');
	    		$date4 = date_format($date2, 'm');
	    		$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
	    		$mespub = $meses[$date4 - 1];
				$resultado .= '<article class="post clearfix mb-0">
				                <div class="entry-header">
				                  <div class="post-thumb thumb"> <img src="'.$rw['foto'].'" alt="" class="img-responsive img-fullwidth"> </div>
				                </div>
				                <div class="entry-content">
				                  <div class="entry-meta media no-bg no-border mt-15 pb-20">
				                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
				                      <ul>
				                        <li class="font-16 text-white font-weight-600">'.$date3.'</li>
				                        <li class="font-12 text-white text-uppercase">'.$mespub.'</li>
				                      </ul>
				                    </div>
				                    <div class="media-body pl-15" style="vertical-align: middle;">
				                      <div class="event-content pull-left flip">
				                        <h3 class="entry-title text-white text-uppercase pt-0 mt-0"><a href="blog-single-right-sidebar.html">'.$rw['titulo'].'</a></h3>
				                      </div>
				                    </div>
				                  </div>
				                  <p class="mb-15">'.$rw['contenido'].'</p>
				                </div>
				              </article>';
                        	        	
	        }
	    } else {
	    	echo "No hay noticias recientes";
	    }
	return $resultado;

}

?>