<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

function selectCT($tipo){// Muestra el select con las opciones de los cursos o talleres en el modulo incripciones
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT c.id, c.nombre AS cursonombre, c.descripcion, c.imagen, c.tipo, h.*, u.* FROM cursos c 
	INNER JOIN horarios h ON c.id = h.cursoid 
	INNER JOIN ubicacion u ON h.ubicacionid = u.id 
	WHERE c.tipo IN('$tipo') AND h.fechainicio >= CURDATE() AND h.inscritos < h.capacidad";
	
	$select = "";
	$resultado= "";
	if ($tipo == 1) {
		$mostrar = 'Seleccione un curso:';
		$tipoct = 'cursos';
	} elseif ($tipo == 2) {
		$mostrar = 'Seleccione un taller:';
		$tipoct = 'talleres';
	} else {
		$mostrar = 'Seleccione una opción:';
		$tipoct = 'cursos o talleres';
	}
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
		  $nombrecurso = $rw['cursonombre'];
		  $idcurso = $rw['cursoid'];
			$resultado .= '<option value='.$idcurso.'>'.$nombrecurso.'</option>';
		}
		$select = "<div class='pt-20 pb-20'><h5 style='font-weight: bold;'><i class='fa fa-dot-circle-o'></i> ".$mostrar."</h5><select onchange='mostrarInfoCT(this.value)' class='form-control' name='curso'><option value=''>Seleccione una opción</option>".$resultado."</select></div>";
	} else {
		$select = '<div class="pt-20 pb-20"><div class="alert alert-danger">Disculpe, actualmente no disponemos de '.$tipoct.'. Intente nuevamente más tarde.</div></div>';
	}
	return $select;
}

function infoMailCT($idhorario, $iduser){ // Envia el correo de inscripcion al usuario
	include_once "correos.php";

	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT c.id, c.nombre AS cursonombre, c.descripcion, c.imagen, c.tipo, c.materiales, c.guia, h.*, u.* FROM cursos c INNER JOIN horarios h ON c.id=h.cursoid INNER JOIN ubicacion u ON h.ubicacionid=u.id WHERE h.id_horario = " . $idhorario;
	$result = "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);

	if ($match > 0) {
	 	$rw = mysqli_fetch_array($search);
		$nombrecurso = $rw['cursonombre'];
		$listademateriales = $rw['materiales'];
		$guia = $rw['guia'];
		$fechainicio = $rw['fechainicio'];
		$fechafin = $rw['fechafin'];
		$user = consultarDatosUser($iduser, 2);
		$result = mailInscripcion($user["nombre"], $user["correo"], $nombrecurso, $listademateriales, $guia, $fechainicio, $fechafin);
		mailInscripcionAdmin($user["nombre"], $user["cedula"], $nombrecurso, $fechainicio, $fechafin);
	}

	return $result;
}

function infoCT($idcurso){ // Muestra la información del curso al seleccionarlo en el modulo inscripción
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}
	if ($idcurso != "") {

	 	$sql = "SELECT c.id, c.nombre as cursonombre, c.descripcion, c.imagen, c.tipo, h.*, u.* FROM cursos c INNER JOIN horarios h ON c.id=h.cursoid INNER JOIN ubicacion u ON h.ubicacionid=u.id WHERE c.id=".$idcurso."";

		$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
		$match = mysqli_num_rows($search);
		$select = "";
		if ($match > 0) {
			while ($rw = mysqli_fetch_array($search)) {
			$turn = $rw['turno'];
			if ($turn == 'M') {
			    $turn = 'Matutino';
			} elseif ($turn == 'V') {
			    $turn = 'Vespertino';
			}
			$nombrecurso = $rw['cursonombre'];
			$inicio = $rw['fechainicio'];
			$inicio2 = date_create($inicio);
			$inicio3 = date_format($inicio2, 'd-m-Y');
			$fin = $rw['fechafin'];
			$fin2 = date_create($fin);
			$fin3 = date_format($fin2, 'd-m-Y');
			$horario = $rw['horario'];
			$idhorario = $rw['id_horario'];
			$pais = $rw['pais'];
			if ($pais == 1) {
				$moneda = 'Bs.';
			} elseif ($pais == 2) {
				$moneda = '$';
			}
			$select = '<div class="w3-card-4 p-20 mt-10">
					<div class="row mb-10">
						<div class="col-md-12">
							<div class="text-center"><h3><b> '.$nombrecurso.' </b></h3></div>
						</div>
					</div>
					<div class="row mb-10">
						<div class="col-md-12">
							<div class="col-md-6">
								<b>Fecha de Inicio:</b> '.$inicio3.'
							</div>
							<div class="col-md-6">
								<b>Fecha Final:</b> '.$fin3.'
							</div>
						</div>
					</div>
					<div class="row mb-10">
						<div class="col-md-12">
							<div class="col-md-6">
								<b>Turno:</b> '.$turn.'
							</div>
							<div class="col-md-6">
								<b>Horario:</b> '.$horario.'
							</div>
						</div>
					</div>
					<div class="row mb-10">
						<div class="col-md-12">
							<div class="col-md-12">
								<b>Ubicación: '.$rw['nombre'].'.</b> '.$rw['direccion'].'
							</div>
						</div>
					</div>
					<div class="row mb-10">
						<div class="col-md-12">
							<div class="col-md-12">
								<b>Precio: Cuota Única:</b> '.number_format($rw['precio1'],2,",","."). ' '.$moneda.' - <b>Dos Cuotas:</b> '.number_format($rw['precio2'],2,",",".").' '.$moneda.'
							</div>
						</div>
					</div>
					<div class="row mb-10">
						<div class="col-md-12">
							<div class="col-md-12"><b>Contenido:</b> '.$rw['descripcion'].'</div>
						</div>
					</div>
					<div class="text-center row mb-10 mt-20">
						<span id="boton-inscribir"><button onclick="paymentCT('.$idhorario.', '.$pais.')" type="button" class="btn btn-colored btn-lg btn-theme-color-2 text-white">Inscribir</button></span>
					</div></div>';
			        }
			    } else {
			    	echo "Disculpe, no disponemos de la información. Intente nuevamente más tarde.";
			    }
	} else {
		$select = '<div class="alert alert-danger">Seleccione una opción para continuar con la inscripción.</div>'; 
	}
	return $select;
}

function precioCT($opcion, $idhorario, $moneda){//
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT h.precio1, h.precio2, c.nombre, c.tipo  FROM horarios h INNER JOIN cursos c ON h.cursoid=c.id WHERE id_horario=".$idhorario."";

	$resultado= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			$tipo = $rw['tipo'];
			$curso = $rw['nombre'];
			if ($tipo == 1) {//Curso
				$tipo = 'Curso';
			} elseif ($tipo == 2) {//Taller
				$tipo = 'Taller';
			}
			$cuotas = "";
			if ($opcion == 1) {// Cuota única
				$precio = $rw['precio1'];
				$metodo = 'cuota única';
			} elseif ($opcion == 2) {// Dos cuotas
				$precio = $rw['precio2'];
				$metodo = 'dos cuotas';
				$ab = $rw['precio2']/2;
				$cuotas = "<div class='row text-center mb-20'><div class='col-md-6'><h5><b>Cuota 1: ".number_format($ab,2,",",".")." ".consultarMoneda($idhorario)."</b></h5></div><div class='col-md-6'><h5><b>Cuota 2: ".number_format($ab,2,",",".")." ".consultarMoneda($idhorario)."</b></h5></div></div>";
			}
			$resultado = '<div class="p-20">
									<div class="w3-card-4 pl-20 pr-20 pt-20 pb-20">
										<div class="row text-center">
											<div class="col-md-12">
												<h3 class="mb-0"><b>Inscribir '.$tipo.':</b> '.$curso.'</h3> 
												<p class="mt-0">Has seleccionado la modalidad de pago de <b>'.$metodo.'</b> generando un monto total a cancelar de:</p>
												<h3 class="mt-10"><b>'.number_format($precio,2,",",".").' '.consultarMoneda($idhorario).'</b></h3>
											</div>
										</div>
										'.$cuotas.'
										<div class="row pl-60 pr-60">
											<div class="alert alert-danger"><strong>¡IMPORTANTE!</strong> Para garantizar la inscripción al curso y disponibilidad de cupo en el mismo debe cancelar la cuota indicada en su totalidad, de lo contrario la inscripción no será finalizada.</div>
										</div>
										<div class="text-center row mb-20">
											<button type="button" onclick="showContent(3);" class="btn btn-default btn-lg">Regresar</button>
											<button type="button" onclick="goPayment('.$idhorario.', '.$opcion.')" class="btn btn-colored btn-lg btn-theme-color-2 text-white">Continuar con el Pago</button>
										</div>
									</div>
								</div>';	
		}
	} else {
		$resultado = '<div class="pt-20 pb-20"><div class="alert alert-danger">Disculpe, actualmente la información no está disponible. Intente nuevamente más tarde.</div></div>';
	}
	
	return $resultado;
}

function regDeuda($idhorario, $opcion, $userid) { // Registra el curso al estudiante
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	if (validarInscripcion($idhorario, $userid) == false) {
		$sql = "INSERT INTO inscripcion (idhorario, idestudiante, status, fecha_insc, hora_insc, opcionpago, inscripcion) VALUES ('".$idhorario."', '".$userid."', 0, CURDATE(), CURTIME(), '".$opcion."', 0)";

		$result = "";
		if (mysqli_query($dbh, $sql)) {
			$result = infoMailCT($idhorario, $userid);
		} else {
			$result = 0;
		}
		return $result;	
	} else {
		return 2;
	}
}

function validarInscripcion($idhorario, $userid) { // Valida si el estudiante tiene horarios inscritos
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}
	$sql = "SELECT * FROM inscripcion i WHERE i.idhorario = " . $idhorario . " AND i.idestudiante = " . $userid;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		return true;
	} else {
		return false;
	}
}

function cargarCTUser($userid){// Carga los cursos que no han sido pagados en el modulo de pago online de los estudiantes
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT i.*, h.*, c.* FROM inscripcion i INNER JOIN horarios h ON i.idhorario=h.id_horario INNER JOIN cursos c ON c.id=h.cursoid WHERE idestudiante=".$userid." AND i.status in(0, 1)";

	$resultado= ""; $cuotainfo = "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$cursos = "";
		$deudatotal = 0;
		while ($rw = mysqli_fetch_array($search)) {
		   $estado = $rw['status'];
		   $idhorario = $rw['idhorario'];
		   $opc = $rw['opcionpago'];
		   $inicio = date_format(date_create($rw['fechainicio']), 'd-m-Y');
		   $fin = date_format(date_create($rw['fechafin']), 'd-m-Y');
		   $date = date_format(date_create($rw['fecha_insc']), 'd-m-Y');	
			$moneda = consultarMoneda($idhorario);
			if ($estado == 0) {
		   	$estado = '<span style="color: #e32028;"><b>Pago pendiente</b> <i class="fa fa-times-circle-o" aria-hidden="true"></i></span>';
		   	$cuota1info = '<span style="color: #e32028;"><i class="fa fa-check-circle-o" aria-hidden="true"></i> <b>Pendiente</b></span>';
		   	$cuota2info = '<span style="color: #e32028;"><i class="fa fa-check-circle-o" aria-hidden="true"></i> <b>Pendiente</b></span>';
		   } elseif ($estado == 1) {
		   	$estado = '<span style="color: green;"><b>Inscrito</b> <i class="fa fa-check-circle-o" aria-hidden="true"></i></span>';
		   	$cuota1info = '<span style="color: green;"><i class="fa fa-check-circle-o" aria-hidden="true"></i> <b>Pagada</b></span>';
		   	$cuota2info = '<span style="color: #e32028;"><i class="fa fa-times-circle-o" aria-hidden="true"></i> <b>Pendiente</b></span>';
		   } elseif ($estado == 2) {
		   	$estado = '<span style="color: green;"><b>Inscrito</b> <i class="fa fa-check-circle-o" aria-hidden="true"></i></span>';
		   	$cuota1info = '<span style="color: green;"><i class="fa fa-check-circle-o" aria-hidden="true"></i> <b>Pagada</b></span>';
		   	$cuota2info = '<span style="color: green;"><i class="fa fa-check-circle-o" aria-hidden="true"></i> <b>Pagada</b></span>';
		   }
			if ($opc == 1) { 
		   	$precio = $rw['precio1']; $opcionp = 'Cuota única.';
		   	$preciocompleto = '<div class="col-md-6"><b>Precio Total:</b> '.number_format($precio,2,",",".").' '.$moneda.'</div>';
		   } elseif ($opc == 2) { 
		   	$precio = $rw['precio2']; $opcionp = 'Dos cuotas.'; $cadacuota = number_format($rw['precio2']/2,2,",",".");
		   	$preciocompleto = '<div class="col-md-6"><b>Precio Total:</b> '.number_format($precio,2,",",".").' '.$moneda.'</div><div class="col-md-6"><b>Cuota 1:</b> '.$cadacuota.' '.$moneda.' '.$cuota1info.'</div><div class="col-md-6"><b>Cuota 2:</b> '.$cadacuota.' '.$moneda.' '.$cuota2info.'</div>';
		   }
			$cursos .= '<div class="panel">
                  	<div class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse" href="#ct'.$rw['inscripcion_id'].'" class="" aria-expanded="true"> <span class="open-sub"></span> '.$rw['nombre'].' <span class="badge">Deuda: '.number_format(consultarSaldoCT($rw['inscripcion_id']),2,",",".").'</span></a> </div>
                    <div id="ct'.$rw['inscripcion_id'].'" class="panel-collapse collapse" role="tablist" aria-expanded="true">
                      <div class="panel-content">
                      	<div class="row mb-10">
		                     <div class="col-md-6"><b>Fecha de Inscripción:</b> '.$date.'</div>
			                  '.$preciocompleto.'
		                     <div class="col-md-6"><b>Opción de Pago:</b> '.$opcionp.'</div>
		                     <div class="col-md-6"></div>
		                     <div class="col-md-6"><b>Inicio:</b> '.$inicio.'</div>
		                     <div class="col-md-6"><b>Fin:</b> '.$fin.'</div>
		                     <div class="col-md-6"><b>Estado de Inscripción:</b> '.$estado.' </div>
	                     </div>
	                     <div class="row text-center">
		                     <h5><b>Seleccione el método de pago:</b></h5>
		                     <button onclick="payMethod(2, '.$rw['inscripcion_id'].')" type="button" class="btn btn-default" style="width:200px;"><img src="/componentes/images/instapago.png"></button>
												 </div>
												 </div>
												 </div>
												 </div>';
												//  <button onclick="payMethod(1, '.$rw['inscripcion_id'].')" type="button" class="btn btn-default" style="width:200px;"><img src="/componentes/images/mp.png"></button>
												 
			$resultado = '<div class="p-20"><h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-20">Paga tus facturas <span class="text-theme-colored">Online</span></h2><p>Aquí puedes realizar el pago de los cursos que has inscrito con tus tarjetas de crédito <b>VISA</b> o <b>MASTERCARD</b> en una o más transacciones de forma rápida y segura a través de la plataforma de Banesco Instapago.</p><center class="mt-30"><h5 style="font-weight: bold;"><i class="fa fa-dot-circle-o"></i> Seleccione el curso o taller que desea pagar:</h5></center><div id="accordion1" class="panel-group accordion"> '.$cursos.'</div></div>';
		}
	} else {
		$resultado = '<div class="p-20"><h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-20">Paga tus facturas <span class="text-theme-colored">Online</span></h2><p>Aquí puedes realizar el pago de los cursos que has inscrito con tus tarjetas de crédito VISA o MASTERCARD en una o más transacciones de forma rápida y segura a través de la plataforma de Banesco Instapago, luego de pagar la totalidad del curso podrás ver la información del mismo en la sección de <b>"Historial de Cursos"</b>.</p></h2><div class="alert alert-danger"><strong>Actualmente no posees alguna deuda pendiente por cancelar.</strong> Te invitamos a ver los cursos disponibles en el módulo de inscripción.</div></div>';
	}
	return $resultado;

}

function consultarMoneda($idhorario){// Consulta la moneda segun el pais donde se dicte el curso
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT hor.*, ubi.* FROM horarios hor INNER JOIN ubicacion ubi ON hor.ubicacionid=ubi.id WHERE id_horario=".$idhorario."";

	$resultado= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
		  $pais = $rw['pais'];
		  if ($pais == 1) {
		  	$resultado = 'Bs.';
		  } elseif ($pais == 2) {
		  	$resultado = '$';
		  }
		}
	} else {
		$resultado = '';
	}
	return $resultado;
}

function consultarSaldoCT($idinscripcion){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT h.*, i.* FROM inscripcion i INNER JOIN horarios h ON i.idhorario=h.id_horario WHERE i.inscripcion_id=".$idinscripcion."";

	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$precio1 = 0; $precio2 = 0; $tipopago = ""; $estado = ""; $pago = 0; $pagototal = 0; $deuda = 0;
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$row = mysqli_fetch_array($search);
		$tipopago = $row['opcionpago'];
		$precio1 = $row['precio1'];
		$precio2 = $row['precio2'];
		$estado = $row['status'];

		$sql2 = "SELECT * FROM pagos WHERE idinscripcion=".$idinscripcion." AND codigo=201 AND respuesta=0";

		$search2 = mysqli_query($dbh, $sql2) or die(mysqli_error($dbh));
		$match2 = mysqli_num_rows($search2);
		if ($match2 > 0) {
			while ($rw = mysqli_fetch_array($search2)) {
				$pago = floatval($rw['total']);
			   $pagototal += $pago;
			}
		} else {
			$pagototal = 0;
		}

		if ($tipopago == 1) {
			$deuda = $precio1 - $pagototal;
		} elseif ($tipopago == 2) {
			$deuda = $precio2 - $pagototal;
	   }	   

	} else {
		$deuda = 0;
	}
	return $deuda;
}


function selectPayment($tipo, $idinscripcion){// Muestra el formulario de pago segun el metodo de pago en el modulo de pago
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT * FROM inscripcion WHERE inscripcion_id=".$idinscripcion."";

	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$idhorario = "";
	$match = mysqli_num_rows($search);
	$horario = "";
	if ($match > 0) {
		$row = mysqli_fetch_array($search);
		$moneda = consultarMoneda($row['idhorario']);
		$tipopago = $row['opcionpago'];
	} else {
		$moneda = "Bs.";
	}

	$pagar = consultarSaldoCT($idinscripcion);

	if ($tipo == 1) {// Mercado Pago
		return array($pagar, $moneda);

		//include_once 'componentes/mp2.php';
		//header("Location: /estudios/pay/index.php?provider=mp");
	} elseif ($tipo == 2) {// Insta Pago
		include_once 'componentes/ip.php';
	}
	return;
}

function guardarTransaccion($idinscripcion, $codigo, $mensaje, $response, $voucher, $monto, $referencia, $usuario, $metodo, $banco) {
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$saldo = number_format(consultarSaldoCT($idinscripcion),2,",",".");
	if ($monto > intval($saldo)) {
		$result = 2;
	} else {
		$sql = "INSERT INTO pagos (idinscripcion, codigo, mensaje, respuesta, voucher, total, fecha, hora, referencia, agregadopor, metodopago, banco) VALUES ('".$idinscripcion."', '".$codigo."', '".$mensaje."', '".$response."', '".$voucher."', '".$monto."', CURDATE(), CURTIME(), '".$referencia."', '".$usuario."', '".$metodo."', '".$banco."')";
	
		$result = "";
		if (mysqli_query($dbh, $sql)) {
			$result = 1;    
			verificarStatusCT($idinscripcion);
		} else {
			$result = 0;
		}
	}
	return $result;
}

function verificarStatusCT($idinscripcion){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT * FROM inscripcion WHERE inscripcion_id=".$idinscripcion."";

	$result= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$idhorario = $rw['idhorario'];
		$opcion = $rw['opcionpago'];
		if ($opcion == 1) {# Contado
			$tipopago = 'precio1';
		} elseif ($opcion == 2) {# Dos cuotas
			$tipopago = 'precio2';
		}
		$precio = consultarPrecioCT($idhorario, $tipopago);		
	} else {
		$precio = '';
	}

	$pagos = consultarPagosCT($idinscripcion);

	if ($opcion == 1) {
		if ($pagos == $precio) {

			$sql2 = "UPDATE inscripcion SET status=2 WHERE inscripcion_id=".$idinscripcion."";

			$result = "";
			if (mysqli_query($dbh, $sql2)) {
			  $result = 1;

			  $inscrito = consultarInscripcion($idinscripcion);
			  if ($inscrito != 1) {
			  		procesarInscripcion($idhorario);
			  }
			  registrarInscripcion($idinscripcion);
			} else {
				$result = 0;
			}

		}
	} elseif ($opcion == 2) {
		$cuota = $precio/2;

		if ($pagos >= $cuota && $pagos < $precio) {

			$sql2 = "UPDATE inscripcion SET status=1 WHERE inscripcion_id=".$idinscripcion."";

			$result = "";
			if (mysqli_query($dbh, $sql2)) {
			  $result = 1;

			  $inscrito = consultarInscripcion($idinscripcion);
			  if ($inscrito != 1) {
			  		procesarInscripcion($idhorario);
			  }
			  registrarInscripcion($idinscripcion);
			} else {
				$result = 0;
			}

		} elseif ($pagos == $precio) {
			
			$sql2 = "UPDATE inscripcion SET status=2 WHERE inscripcion_id=".$idinscripcion."";

			$result = "";
			if (mysqli_query($dbh, $sql2)) {
			  $result = 1;

			  $inscrito = consultarInscripcion($idinscripcion);
			  if ($inscrito != 1) {
			  		procesarInscripcion($idhorario);
			  }
			  registrarInscripcion($idinscripcion);
			} else {
				$result = 0;
			}

		}
	}

	return $result;
}

function consultarPrecioCT($idhorario, $tipopago){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT * FROM horarios WHERE id_horario=".$idhorario."";

	$precio= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$precio = $rw[''.$tipopago.''];
	} else {
		$precio = '';
	}

	return $precio;
}

function consultarPagosCT($idinscripcion){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT * FROM pagos WHERE idinscripcion=".$idinscripcion." AND codigo=201 AND respuesta=0";

	$pagototal= 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			$pago = $rw['total'];
			$pagototal += $pago;
		}
	} else {
		$pagototal = 0;
	}

	return $pagototal;
}

function consultarInscritos($idhorario){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT inscritos FROM horarios WHERE id_horario=".$idhorario."";

	$inscritos= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$inscritos = $rw['inscritos'];
	} else {
		$inscritos = "";
	}

	return $inscritos;
}

function procesarInscripcion($idhorario){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$inscritos = consultarInscritos($idhorario) +1;

	$sql = "UPDATE horarios SET inscritos=".$inscritos." WHERE id_horario=".$idhorario."";

	$result = "";
	if (mysqli_query($dbh, $sql)) {
	    $result = 1;    
	} else {
		$result = 0;
	}
	return $result;
}

function consultarInscripcion($idinscripcion){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT inscripcion FROM inscripcion WHERE inscripcion_id=".$idinscripcion."";

	$inscripcion= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$inscripcion = $rw['inscripcion'];
	} else {
		$inscripcion = "";
	}

	return $inscripcion;
}

function registrarInscripcion($idinscripcion){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "UPDATE inscripcion SET inscripcion=1 WHERE inscripcion_id=".$idinscripcion."";

	$result = "";
	if (mysqli_query($dbh, $sql)) {
	    $result = 1;    
	} else {
		$result = 0;
	}
	return $result;
}

function consultarDatosUser($userid, $origen){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT * FROM estudiantes WHERE id=".$userid."";

	$mostrar= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$nombre = $rw['nombre'];
		$apellido = $rw['apellido'];
		$correo = $rw['correo'];
		$documento = $rw['documentacion'];
		$fn = date_format(date_create($rw['fechadenacimiento']), 'd-m-Y');
		$profesion = $rw['profesion'];
		$genero = $rw['genero'];
		if ($genero == 1) {
			$genero = 'Femenino';
		} elseif ($genero == 2) {
			$genero = 'Masculino';
		}
		$telef = $rw['telef1'] .' - '. $rw['telef2'] .' - '. $rw['telef3'];
		$direccion = $rw['direccion'];
		$civil = $rw['estadocivil'];
		if ($civil == 1) {
			$civil = 'Soltero';
		} elseif ($civil == 2) {
			$civil = 'Casado';
		} elseif ($civil == 3) {
			$civil = 'Concubinato';
		} elseif ($civil == 4) {
			$civil = 'Divorciado';
		} elseif ($civil == 5) {
			$civil = 'Viudo';
		} elseif ($civil == 6) {
			$civil = 'Otro';
		} else {
			$civil = 'S/I';
		}
		$pais = $rw['paisnacimiento'];

		if ($origen ==  1) {
			$mostrar = '<div class="p-20">
						<h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-20">Consulta tus datos <span class="text-theme-colored">Personales</span></h2>
						<p>Esta sección te permite consultar todos los datos de tu cuenta, así como modificarlos en caso de ser necesario y crear una nueva contraseña.</p>
						<div class="row">
	              <div class="col-sm-6">
	                <div class="form-group mb-10">
	                  <b>Documento de Identificación:</b> <div>'.$documento.'</div>
	                </div>
	              </div>
	              <div class="col-sm-6">
	                <div class="form-group mb-10">
	                  <b>Nombre Completo:</b> <div>'.$nombre.' '.$apellido.'</div>
	                </div>
	              </div>
	              <div class="col-sm-6">
	                <div class="form-group mb-10">
	                  <b>E-mail:</b> <div>'.$correo.'</div>
	                </div>
	              </div>
	              <div class="col-sm-6">
	                <div class="form-group mb-10">
	                  <b>Profesión:</b>  <div>'.$profesion.'</div>
	                </div>
	              </div>
	              <div class="col-sm-6">
	                <div class="form-group mb-10">
	                  <b>F.N.:</b> <div>'.$fn.'</div>
	                </div>
	              </div>
	              <div class="col-sm-6">
	                <div class="form-group mb-10">
	                  <b>País de Nacimiento: </b><div><span class="bfh-countries" data-country="'.$pais.'"></span></div>
	                </div>
	              </div>
	              <div class="col-sm-6">
	                <div class="form-group mb-10">
	                  <b>Género:</b> <div>'.$genero.'</div>
	                </div>
	              </div>
	              <div class="col-sm-6">
	                <div class="form-group mb-10">
	                  <b>Estado Civil:</b> <div>'.$civil.'</div>
	                </div>
	              </div>
	              <div class="col-sm-12">
	                <div class="form-group mb-10">
	                  <b>Teléfonos:</b> <div>'.$telef.'</div>
	                </div>
	              </div>
	              <div class="col-sm-12">
	                <div class="form-group mb-30">
	                  <b>Dirección:</b> <div>'.$direccion.'</div>
	                </div>
	              </div>
	              <center>
	              <button type="button" onclick="showContent(21);" class="btn btn-colored btn-theme-color-2 btn-sm text-white">Modificar</button>
	              <a href="cambio.php" class="btn btn-colored btn-theme-color-2 btn-sm text-white" role="button">Cambiar Contraseña</a>
	              </center>
	            </div>
            </div>';
		} else if ($origen == 2) {
			$mostrar = array(
				"nombre" => $nombre . " " .$apellido,
				"correo" => $correo,
				"cedula" => $documento
			);
		}
	} else {
		$mostrar = "";
	}

	return $mostrar;
}

function modificarDatosUser($userid){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT * FROM estudiantes WHERE id=".$userid."";

	$mostrar= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$genero = $rw['genero'];
		if ($genero == 1) {
			$genero = '<option value="">- Sexo -</option>
                  <option value="1" selected>Femenino</option>
                  <option value="2">Masculino</option>';
		} elseif ($genero == 2) {
			$genero = '<option value="">- Sexo -</option>
                  <option value="1">Femenino</option>
                  <option value="2" selected>Masculino</option>';
		} else {
			$genero = '<option value="">- Sexo -</option>
                  <option value="1">Femenino</option>
                  <option value="2">Masculino</option>';
		}
		$civil = $rw['estadocivil'];
		if ($civil == 1) {
			$civil = '<option value="">- Estado Civil -</option>
                  <option value="1" selected> Soltero </option>
                  <option value="2"> Casado </option>
                  <option value="3"> Concubinato </option>
                  <option value="4"> Divorciado </option>
                  <option value="5"> Viudo </option>
                  <option value="6"> Otro </option>';
		} elseif ($civil == 2) {
			$civil = '<option value="">- Estado Civil -</option>
                  <option value="1"> Soltero </option>
                  <option value="2" selected> Casado </option>
                  <option value="3"> Concubinato </option>
                  <option value="4"> Divorciado </option>
                  <option value="5"> Viudo </option>
                  <option value="6"> Otro </option>';
		} elseif ($civil == 3) {
			$civil = '<option value="">- Estado Civil -</option>
                  <option value="1"> Soltero </option>
                  <option value="2"> Casado </option>
                  <option value="3" selected> Concubinato </option>
                  <option value="4"> Divorciado </option>
                  <option value="5"> Viudo </option>
                  <option value="6"> Otro </option>';
		} elseif ($civil == 4) {
			$civil = '<option value="">- Estado Civil -</option>
                  <option value="1"> Soltero </option>
                  <option value="2"> Casado </option>
                  <option value="3"> Concubinato </option>
                  <option value="4" selected> Divorciado </option>
                  <option value="5"> Viudo </option>
                  <option value="6"> Otro </option>';
		} elseif ($civil == 5) {
			$civil = '<option value="">- Estado Civil -</option>
                  <option value="1"> Soltero </option>
                  <option value="2"> Casado </option>
                  <option value="3"> Concubinato </option>
                  <option value="4"> Divorciado </option>
                  <option value="5" selected> Viudo </option>
                  <option value="6"> Otro </option>';
		} elseif ($civil == 6) {
			$civil = '<option value="">- Estado Civil -</option>
                  <option value="1"> Soltero </option>
                  <option value="2"> Casado </option>
                  <option value="3"> Concubinato </option>
                  <option value="4"> Divorciado </option>
                  <option value="5"> Viudo </option>
                  <option value="6" selected> Otro </option>';
		} else {
			$civil = '<option value="">- Estado Civil -</option>
                  <option value="1"> Soltero </option>
                  <option value="2"> Casado </option>
                  <option value="3"> Concubinato </option>
                  <option value="4"> Divorciado </option>
                  <option value="5"> Viudo </option>
                  <option value="6"> Otro </option>';
		}
		$pais = $rw['paisnacimiento'];
		$mostrar = '<div class="p-20"><form action="/api.php" class="reservation-form" method="POST">
            <div class="row">
              <div class="col-md-12 text-center mb-10"><strong><i class="fa fa-user"></i> Datos Personales</strong></div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input type="text" name="ident2" disabled="" value="'.$rw['documentacion'].'" class="form-control">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input type="text" name="name"  required="" value="'.$rw['nombre'].'" class="form-control">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input type="text" name="lastname" required="" value="'.$rw['apellido'].'" class="form-control">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <select name="civil" id="civil" class="form-control" required="">
                     '.$civil.'
                  </select>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input type="text" name="profesion" value="'.$rw['profesion'].'" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input name="ffnn" id="ffnn" class="form-control" type="date" value="'.$rw['fechadenacimiento'].'" required="" aria-required="true">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <select name="country" id="country" class="form-control bfh-countries" data-country="'.$rw['paisnacimiento'].'" required=""></select>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <div class="styled-select">
                    <select name="sexo" id="sexo" class="form-control" required="">
                      '.$genero.'
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 text-center mb-10"><strong><i class="fa fa-phone"></i> Datos de Contacto</strong></div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input type="email" name="email" value="'.$rw['correo'].'" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input type="text" name="hphone" value="'.$rw['telef1'].'" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input type="text" name="cphone1" value="'.$rw['telef2'].'" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input type="text" name="cphone2" value="'.$rw['telef3'].'" class="form-control">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group mb-20">
                  <textarea name="address" class="form-control" required="" cols="20" rows="2"> '.$rw['direccion'].'"</textarea>
                </div>
              </div>
            </div>
    		<input type="hidden" name="tipo" value="updDatos">
    		<input type="hidden" name="usuario" value="'.$userid.'">
            <div class="text-center">
              <button type="submit" name="updDatos" class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-colored-4px">Guardar</button>
            </div>
          </form>
        </div>';
	} else {
		$mostrar = "";
	}

	return $mostrar;
}

function updateDatos($nombre, $apellido, $civil, $profesion, $ffnn, $paises, $sexo, $correo, $telf1, $telf2, $telf3,  $direccion, $idest){#Modifica los datos de estudiante en la BD
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "UPDATE estudiantes SET nombre='".$nombre."', apellido='".$apellido."', estadocivil='".$civil."', profesion='".$profesion."', fechadenacimiento='".$ffnn."', paisnacimiento='".$paises."', genero='".$sexo."', telef1='".$telf1."', telef2='".$telf2."', telef3='".$telf3."', correo='".$correo."', direccion='".$direccion."' WHERE id=".$idest."";

	$result = "";
	if (mysqli_query($dbh, $sql)) {
		header('Location: ../estudios/index.php?updatos=success');
		$result = 1;
	} else {
		header('Location: ../estudios/index.php?updatos=fail');
		$result = 0;
	}
	return $result;
}

function consultarCuotaPagar($idinscripcion, $pagar, $moneda){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT i.*, h.* FROM inscripcion i INNER JOIN horarios h ON i.idhorario=h.id_horario WHERE i.inscripcion_id=".$idinscripcion."";


	$result= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$tipopago = $rw['opcionpago'];
		$estado = $rw['status'];
		if ($tipopago == 1) {
			$precio = $rw['precio1'];
			if ($estado == 0) {
				$result = '<div class="radio">
								<label><input type="radio" name="opc-pago" value="'.$pagar.'" required >Deuda total: <b>'. number_format($pagar,2,",",".") ." ". $moneda .'</b></label>
							</div>
							<div class="radio">
		                  <label><input type="radio" name="opc-pago" value="otro" onclick="notificacionCuotaA('.$pagar.');" required >Otro Monto: </label>
		                  <input type="text" name="monto" value="'.$pagar.'" placeholder="Ingrese el monto">
		               </div>';
			} 
		} elseif ($tipopago == 2) {
			$precio = $rw['precio2'];
			$cuota = $precio/2;
			$pagarc1 = $pagar - $cuota;
			if ($estado == 0) {
				$result = '<div class="radio"><label><input type="radio" name="opc-pago" value="'.$pagarc1.'" required >Cuota 1: <b>'.number_format($pagarc1,2,",",".") ." ". $moneda .'</b></label>
							</div>
							<div class="radio">
		                  <label><input type="radio" name="opc-pago" value="otro" onclick="notificacionCuotaB('.$pagarc1.');" required >Otro Monto: </label>
		                  <input type="text" name="monto" placeholder="Ingrese el monto">
		               </div>';
			} elseif ($estado == 1) {
				$result = '<div class="radio"><label><input type="radio" name="opc-pago" value="'.$pagar.'" required >Cuota 2: <b>'. number_format($pagar,2,",",".") ." ". $moneda.'</b></label>
							</div>
							<div class="radio">
		                  <label><input type="radio" name="opc-pago" value="otro" onclick="notificacionCuotaC('.$pagar.');" required >Otro Monto: </label>
		                  <input type="text" name="monto" placeholder="Ingrese el monto">
		               </div>';
			}
		}
	} else {
		$result = "";
	}

	return $result;
}

function historialCursos($usuario){
	$dbh = dbconnlocal2();
    mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT i.*, h.*, c.id, c.nombre, c.tipo, c.descripcion, c.idmodulo, c.id_usuario, c.estado FROM inscripcion i INNER JOIN horarios h ON i.idhorario = h.id_horario INNER JOIN cursos c ON h.cursoid=c.id WHERE i.idestudiante=".$usuario." AND i.inscripcion IN (1,2)";

	$resultado= "";
	$i = 0;
  $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
	    while ($rw = mysqli_fetch_array($search)) {
	      $tipo = $rw['tipo'];
	      $fecha = $rw['fechafin'];
				$pagos = $rw['status'];
	      if ($pagos == 0) {
	      	$pagos = "<b style='color: #e32028;'>Pago pendiente</b>";
	      } elseif ($pagos == 1) {
	      	$pagos = "<b style='color: #e32028;'>Cuota 2 pendiente</b>";
	      } elseif ($pagos == 2) {
	      	$pagos = "<b style='color: green;'>Pagado</b>";
				}
				if ($rw['inscripcion'] == 2) {
					$pagos = "<b style='color: #e32028;'>Cancelado por pago</b>";
				}
	      if ($fecha > date("Y/m/d")) {
	        	$estadoct = "Finalizado";
	      } else {
	        	$estadoct = "Cursando";
	      }
	      if ($tipo == 1) {
	      $tipo = 'Curso';
	      } elseif ($tipo == 2) {
	       	$tipo = 'Taller';
	      }
			$resultado .= '<tr> 
	                    		<td>'.$tipo.'</td>	
	                    		<td>'.$rw['nombre'].'</td>	
	                    		<td>'.$estadoct.'</td>
	                    		<td>'.$pagos.'</td>
	                   		<td class="text-right">
	                           <a type="button" data-toggle="modal" data-target="#modalInfo" data-tipo="verHC" data-id="'.$rw['inscripcion_id'].'" href="#"><i class="fa fa-search icon-view"></i></a>
	                        </td>
	                    	</tr>';
	    }
	} else {
	    $resultado = "";
	}

	$tabla = '<div class="p-20">
				<h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-20">Consulta tus <span class="text-theme-colored">Cursos y Talleres</span></h2>
				<p>Esta sección te permite consultar todos los cursos y talleres que has inscrito y realizado el pago de la cuota correspondiente.</p>
				<div data-example-id="simple-responsive-table" class="bs-example"> 
	              <div class="table-responsive"> 
	                <table id="hcursos" class="table table-striped table-hover"> 
	                  <thead> 
	                    <tr> 
	                      <th>Tipo</th>
	                      <th>Nombre</th>
	                      <th>Estado</th>
	                      <th>Pago</th>
	                      <th></th> 
	                    </tr> 
	                  </thead> 
	                  <tbody> 
	                  '. $resultado .'
	                  </tbody> 
	                </table> 
	              </div>
	            </div>
			</div>';

	return $tabla;
}

function historialPagos($usuario){
	$dbh = dbconnlocal2();
    mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT i.*, h.*, p.*, c.id, c.nombre, c.tipo, c.descripcion, c.idmodulo, c.id_usuario, c.estado FROM inscripcion i INNER JOIN horarios h ON i.idhorario = h.id_horario INNER JOIN cursos c ON h.cursoid=c.id INNER JOIN pagos p ON p.idinscripcion=i.inscripcion_id WHERE i.idestudiante=".$usuario."";

	$resultado= "";
	$i = 0;
    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
	    while ($rw = mysqli_fetch_array($search)) {
			$resultado .= '<tr> 
	                    		<td>#'.$rw['referencia'].'</td>	
	                    		<td>'.$rw['nombre'].'</td>	
	                    		<td>'.number_format($rw['total'],2,",",".").'</td>
	                    		<td>'.$rw['mensaje'].'</td>
	                   		<td class="text-right">
	                           <a type="button" data-toggle="modal" data-target="#modalInfo" data-tipo="verPago" data-id="'.$rw['id_pagos'].'" href="#"><i class="fa fa-search icon-view"></i></a>
	                        </td>
	                    	</tr>';
	    }
	} else {
	    $resultado = "";
	}

	$tabla = '<div class="p-20">
				<h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-20">Consulta tus <span class="text-theme-colored">Pagos</span></h2>
				<p>Aquí puedes consultar todos los pagos que has realizado a tus cursos - talleres inscrito.</p>
				<div data-example-id="simple-responsive-table" class="bs-example"> 
	              <div class="table-responsive"> 
	                <table id="hpagos" class="table table-striped table-hover"> 
	                  <thead> 
	                    <tr> 
	                      <th>Referencia</th>
	                      <th>Curso - Taller</th>
	                      <th>Pagos</th>
	                      <th>Mensaje</th>
	                      <th></th> 
	                    </tr> 
	                  </thead> 
	                  <tbody> 
	                  '. $resultado .'
	                  </tbody> 
	                </table> 
	              </div>
	            </div>
			</div>';

	return $tabla;
}

function ultimosCyT(){ // Muestra los cursos en la seccion de cursos pag principal
	include_once "conexion.php";
	$dbh = dbconnlocal2();
    mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT c.*, h.*, u.nombre AS location, u.direccion, u.pais FROM horarios h INNER JOIN cursos c ON h.cursoid=c.id INNER JOIN ubicacion u ON h.ubicacionid=u.id WHERE h.capacidad>h.inscritos AND c.estado=0 AND h.fechafin >= CURDATE() ORDER BY (id_horario) DESC LIMIT 6";

	$resultado= "";
	$i = 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
	   while ($rw = mysqli_fetch_array($search)) {
	      $filtro = $rw['estado'];
	      $turn = $rw['turno'];
	      if ($turn == 'M') {
	       	$turn = 'Matutino';
	      } elseif ($turn == 'V') {
	     		$turn = 'Vespertino';
	     	} elseif ($turn == 'F') {
	       	$turn = 'Fin de Semana';
	      }
	      if ($filtro == 0) {
	         $filtro = 'disp';
	      } elseif ($filtro == 1) {
	        	$filtro = 'fin';
	      }
	      $city = $rw['pais'];
	    	if ($city == 1) {
	        	$city = 'Maracaibo, Venezuela';
	      } elseif ($city == 2) {
	        	$city = 'Obarrio, Panamá';
	      }
			$resultado .= '<div class="gallery-item '.$filtro.'">
					          <div class="thumb">
					            <a type="button" data-tipo="c" data-texto="'.$rw['id_horario'].'" data-toggle="modal" data-target="#cursoModal" href="">
						          <img class="img-fullwidth" src="/componentes/images/cursos/'.$rw['imagen'].'" alt="img">
						          <div class="overlay-shade"><i class="fa fa-search icon-course"></i></div>
					            </a>
					          </div>
					          <div class="portfolio-description">
					            <h4 class="title"><a type="button" data-toggle="modal" data-target="#cursoModal" href="">'.$rw['nombre'].'</a></h4>
					            <small>'.$turn.'</small>
					            <h5 class="category mt-0 mb-0"><span>'.$city.'</span></h5>
					          </div>
					        </div>';
	    }
	} else {
	    $resultado = "<div class='alert alert-danger'>Lo sentimos, actualmente no disponemos de cursos para mostrar, te invitamos a ingresar nuevamente.</div>";
	}
	return $resultado;
}

function verInfoHistoryCT($idinscripcion){ // Muestra la información del curso seleccionado en tabla cursos
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT i.*, h.*, c.*, u.nombre AS location, u.direccion, u.pais FROM inscripcion i INNER JOIN horarios h ON i.idhorario = h.id_horario INNER JOIN cursos c ON h.cursoid=c.id INNER JOIN ubicacion u ON h.ubicacionid=u.id WHERE i.inscripcion_id=".$idinscripcion." AND i.inscripcion IN (1,2)";

	$final = array();
	$resultado= "";
	$nombrecurso= "";
	$i = 0;
    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
      while ($rw = mysqli_fetch_array($search)) {
	      $fecha = $rw['fechafin'];
	      $pagos = $rw['status'];
	      $registro = date_format(date_create($rw['fecha_insc']), 'd-m-Y');
	      $inicio = date_format(date_create($rw['fechainicio']), 'd-m-Y');
	      $fin = date_format(date_create($rw['fechafin']), 'd-m-Y');
	      $nombrecurso = $rw['nombre'];
	      $tipopago = $rw['opcionpago'];
	      if ($pagos == 0) {
	      	$pagos = "<b style='color: #e32028;'>Pago pendiente</b>";
	      } elseif ($pagos == 1) {
	      	$pagos = "<b style='color: #e32028;'>Cuota 2 pendiente</b>";
	      } elseif ($pagos == 2) {
	      	$pagos = "<b style='color: green;'>Pagado</b>";
				}
				if ($rw['inscripcion'] == 2) {
					$pagos = "<b style='color: #e32028;'>Cancelado por pago</b>";
				}
	      if ($fecha > date("Y/m/d")) {
	        	$estadoct = "Finalizado";
	      } else {
	        	$estadoct = "Cursando";
	      }
	      if ($rw['tipo'] == 1) {
	         $tipo = 'Curso';
	      } elseif ($rw['tipo'] == 2) {
	     		$tipo = 'Taller';
	     	}
	     	if ($tipopago == 1) {
	     		$precio = $rw['precio1'];
	     		$tipop = 'De contado';
	     	} elseif ($tipopago == 2) {
	     		$precio = $rw['precio2'];
	     		$tipop = 'Dos cuotas';
	     	}
			$resultado = '<div class="row mb-5">
							    <div class="col-md-12">
						          <div class="col-md-6"><b>Fecha de Inscripción:</b> '.$registro.'</div>
						          <div class="col-md-6"><b>Estado:</b> '.$estadoct.'</div>
							    </div>
							</div>
							<div class="row mb-5">
							    <div class="col-md-12">
						          <div class="col-md-6"><b>Opción de Pago:</b> '.$tipop.'</div>
						          <div class="col-md-6"><b>Precio:</b> '.number_format($precio,2,",",".").'</div>
							    </div>
							</div>
							<div class="row mb-5">
							    <div class="col-md-12">
						          <div class="col-md-6"><b>Inicio:</b> '.$inicio.'</div>
						          <div class="col-md-6"><b>Final:</b> '.$fin.'</div>
							    </div>
							</div>
							<div class="row mb-5">
							  <div class="col-md-12">
							      <div class="col-md-6">
							        <b>Tipo:</b> '.$tipo.'
							      </div>
							      <div class="col-md-6">
							        <b>Estado:</b> '.$pagos.'
							      </div>
							  </div>
							</div>
							<div class="row mb-5">
							  <div class="col-md-12">
							      <div class="col-md-12"><b>Ubicación:</b> <b>'.$rw['location'].'.</b> '.$rw['direccion'].'</div>
							  </div>
							</div>
							<div class="row mb-5">
							  <div class="col-md-12">
							      <div class="col-md-12"><b>Lista de Materiales:</b> <div>'.$rw['materiales'].'</div></div>
							  </div>
							</div>
							<div class="row mb-5">
							  <div class="col-md-12">
							      <div class="col-md-12"><b>Guía Instructiva:</b></div> '.donwloadGuia(base64_encode($rw['guia'])).'
							  </div>
							</div>';
	        }
	    } else {
	    	$nombrecurso = "Información no disponible";
	    	$resultado = "<div class='alert alert-danger'>Lo sentimos, actualmente la información no se encuentra disponible intenta nuevamente.</div>";
	    }
	    $final[0] = $resultado;
	    $final[1] = $tipo.": ".$nombrecurso;
	   
	return json_encode($final);
}

function donwloadGuia($file){	
	$file = base64_decode($file);
	$dirname = "componentes/images/cursos/";
	$archivo = glob($dirname.$file);

	$enlace = '<a href="/'.$archivo[0].'" class="btn btn-link" target="_blank"><i class="fa fa-eye"></i> Ver Guía</a> 
				<a href="/'.$archivo[0].'" class="btn btn-link" download><i class="fa fa-download"></i> Descargar Guía</a>';

	return $enlace;
}

function verInfoPago($idpago){ // Muestra la información del curso seleccionado en tabla cursos
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT * FROM pagos WHERE id_pagos = ".$idpago."";

	$final = array();
	$resultado= "";
	$nombrecurso= "";
	$i = 0;
    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
      while ($rw = mysqli_fetch_array($search)) {
	      $nombrecurso = consultarNombreCTI($rw['idinscripcion']);
	      $fecha = date_format(date_create($rw['fecha']), 'd-m-Y');
			$resultado = '<div class="row mb-5">
							    <div class="col-md-12">
						          <div class="col-md-6"><b>Fecha:</b> '.$fecha.'</div>
						          <div class="col-md-6"><b>Hora:</b> '.$rw['hora'].'</div>
							    </div>
							</div>
							<div class="row mb-5">
							    <div class="col-md-12">
						          <div class="col-md-6"><b>Referencia:</b> #'.$rw['referencia'].'</div>
						          <div class="col-md-6"><b>Respuesta:</b> '.$rw['mensaje'].'</div>
							    </div>
							</div>
							<center><div class="row mt-20">
							  <div class="col-md-12">
							      <div class="col-md-12">'.$rw['voucher'].'</div>
							  </div>
							</div></center>';
	        }
	    } else {
	    	$nombrecurso = "Información no disponible";
	    	$resultado = "<div class='alert alert-danger'>Lo sentimos, actualmente la información no se encuentra disponible intenta nuevamente.</div>";
	    }
	    $final[0] = $resultado;
	    $final[1] = $nombrecurso;
	   
	return json_encode($final);
}

function consultarNombreCTI($idinscripcion){#Consulta el nombre del curso segun id inscripción
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT i.idhorario, h.cursoid, c.nombre FROM inscripcion i INNER JOIN horarios h ON i.idhorario=h.id_horario INNER JOIN cursos c ON h.cursoid=c.id WHERE i.inscripcion_id=".$idinscripcion."";
	
	$resultado= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$resultado = $rw['nombre']; 
	} else {
		$resultado = "Información no disponible";
	}
	return $resultado;
}

function enviarComprobante($idinscripcion, $referencia){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT p.*, i.*, e.* FROM pagos p INNER JOIN inscripcion i ON p.idinscripcion=i.inscripcion_id INNER JOIN estudiantes e ON i.idestudiante=e.id WHERE p.idinscripcion='".$idinscripcion."' AND p.referencia='".$referencia."'";
	
	$resultado= "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		list($n1) = explode(" ", $rw['nombre']);
  		list($a1) = explode(" ", $rw['apellido']);
	   $nombre = $n1 ." ". $a1;
		$voucher = $rw['voucher']; 
		$correo = $rw['correo'];
		$resultado = mailPago($nombre, $correo, $voucher);
	} else {
		$resultado = 0;
	}
}

function tipoPago($opcion){
	switch ($opcion) {
		case '1':
			$result = 'Instapago';
			break;

		case '2':
			$result = 'Punto de Venta';
			break;

		case '3':
			$result = 'Transferencia';
			break;

		case '4':
			$result = 'Depósito';
			break;

		case '5':
			$result = 'Efectivo';
			break;

		case '6':
			$result = 'Cheque';
			break;

		case '7':
			$result = 'Otro';
			break;
		
		default:
			$result = '';
			break;
	}

	return $result;
}

?>