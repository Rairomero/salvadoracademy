<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

function cursosDocente($docenteid) { // Consulta los cursos y talleres de la BD
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
		die('Error en Conexión: ' . mysqli_error($dbh));
		exit;
	}

	$sql = "SELECT c.id, c.nombre, h.id_horario, h.horario, h.inscritos, c.tipo, c.descripcion, c.idmodulo, c.id_usuario, c.estado, u.nombre AS ubicacion FROM cursos c
	INNER JOIN horarios h ON c.id = h.cursoid
	INNER JOIN ubicacion u ON h.ubicacionid = u.id
	WHERE h.idprofesor = $docenteid";

	$resultado = "";
	$i = 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			$tipo = $rw['tipo'];
			$estado = $rw['estado'];
			if ($tipo == 1) {
				$tipo = 'Curso';
			} elseif ($tipo == 2) {
				$tipo = 'Taller';
			}
			if ($estado == 0) {
				$estado = 'Activo';
			} elseif ($estado == 1) {
				$estado = 'Inactivo';
			}
			$resultado .= '<tr>
			<td>'.$tipo.'</td>
			<td>'.$rw['nombre'].'</td>
			<td>'.$rw['horario'].'</td>
			<td>'.$estado.'</td>
			<td class="text-right">
			<a type="button" data-toggle="modal" data-target="#modalInfo" data-tipo="verInfoCurso" data-id="'.$rw['id_horario'].'" href="#"><i class="fa fa-search icon-view"></i></a>
			</td>
			</tr>';
		}
	} else {
		$resultado = "";
	}

	$tabla = '<div class="p-20">
	<div class="mb-20">
	<button onclick="showContent(11)" type="button" class="btn btn-default">Ver Cursos Activos</button>
	</div>
	<div data-example-id="simple-responsive-table" class="bs-example">
	<div class="table-responsive">
	<table id="cursos" class="table table-striped table-hover">
	<thead>
	<tr>
	<th>Tipo</th>
	<th>Nombre</th>
	<th>Horario</th>
	<th>Estado</th>
	<th></th>
	</tr>
	</thead>
	<tbody>
	'. $resultado .'
	</tbody>
	</table>
	</div>
	</div>
	</div>';

	return $tabla;
}

function verInfoCurso($idhorario) {
	// include 'estudios.php';
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
		die('Error en Conexión: ' . mysqli_error($dbh));
		exit;
	}

	$sql = "SELECT c.id, c.nombre AS nombrecurso, h.id_horario, h.horario, h.inscritos, c.tipo, c.descripcion, c.idmodulo, c.estado, u.nombre AS ubicacion, u.direccion, h.capacidad FROM cursos c
	INNER JOIN horarios h ON c.id = h.cursoid
	INNER JOIN ubicacion u ON h.ubicacionid = u.id
	WHERE h.id_horario = $idhorario";

	$final = array();
	$resultado = "";
	$nombre = "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$nombre = "".$rw['nombrecurso'];
		$tablaEstudiantes = consultarEstudiantesCurso($rw['id_horario']);
		$tipo = $rw['tipo'];
		$estado = $rw['estado'];
		if ($tipo == 1) {
			$tipo = 'Curso';
		} elseif ($tipo == 2) {
			$tipo = 'Taller';
		}
		if ($estado == 0) {
			$estado = 'Activo';
		} elseif ($estado == 1) {
			$estado = 'Inactivo';
		}
		$resultado = '<div class="row mb-5">
		<div class="col-md-12">
		<div class="col-md-6"><b>Nombre:</b> '.$rw['nombrecurso'].'</div>
		<div class="col-md-6"><b>Tipo:</b> '.$tipo.'</div>
		</div>
		</div>
		<div class="row mb-5">
		<div class="col-md-12">
		<div class="col-md-6"><b>Horario:</b> '.$rw['horario'].'</div>
		<div class="col-md-6"><b>Estado:</b> '.$estado.'</div>
		</div>
		</div>
		<div class="row mb-5">
		<div class="col-md-12">
		<div class="col-md-6"><b>Capacidad:</b> '.$rw['capacidad'].'</div>
		<div class="col-md-6"><b>Inscritos:</b> '.$rw['inscritos'].'</div>
		</div>
		</div>
		<div class="row mb-5">
		<div class="col-md-12">
		<div class="col-md-6"><b>Ubicacion:</b> '.$rw['horario'].'</div>
		<div class="col-md-6"><b>Direccion:</b> '.$rw['direccion'].'</div>
		</div>
		</div>
		<div class="row mb-5">
		<div class="col-md-12">
		<div class="col-md-12"><b>Estudiantes:</b> </div>
		<div class="col-md-12 mt-10">'.$tablaEstudiantes.' </div>
		</div>
		</div>
		<div class="row mb-5">
		<div class="col-md-12">
		<a style="cursor: pointer;" data-idct="'.$rw['id'].'" class="btn-link" onclick="showContent(111)"><i class="fa fa-book"></i> Actualizar Guía</a>
		</div>
		</div>';
	} else {
		$nombre = 'Información no disponible';
		$resultado = "<div class='alert alert-danger'>Lo sentimos, actualmente la información no está disponible intente más tarde nuevamente.</div>";
	}

	$final[0] = $resultado;
	$final[1] = $nombre;

	return json_encode($final);
}

function consultarEstudiantesCurso($idhorario) { // Consultar una tabla con los estudiantes de un curso, actual uso: modal en seccion cursos del docente
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
		die('Error en Conexión: ' . mysqli_error($dbh));
		exit;
	}

	$sql = "SELECT e.*, i.* FROM estudiantes e
	INNER JOIN inscripcion i ON e.id = i.idestudiante
	WHERE i.idhorario = $idhorario";

	$result = "";
	$tabla = "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			list($n1) = explode(" ", $rw['nombre']);
			list($a1) = explode(" ", $rw['apellido']);
			$user = $n1 ." ". $a1;
			if ($rw['inscripcion'] == 0) {
				$insc = 'No Inscrito';
			} elseif ($rw['inscripcion'] == 1) {
				$insc = 'Inscrito';
			} elseif ($rw['inscripcion'] == 2) {
				$insc = 'Cancelado';
			}
			$result .= '<tr><td>'.$rw['documentacion'].'</td>
			<td>'.$user.'</td>
			<td>'.$rw['correo'].'
			<td>'.$insc.'</td></tr>';
		}
	}
	$tabla = '<table class="table table-striped table-bordered table-hover">
	<tr>
	<th>ID</th>
	<th>Nombre</th>
	<th>Correo</th>
	<th>Estado</th>
	</tr>
	'.$result.'
	</table>';

	return $tabla;
}

function consultarCursosActivos($docenteid) {
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
		die('Error en Conexión: ' . mysqli_error($dbh));
		exit;
	}

	$sql = "SELECT c.id, c.nombre, h.id_horario, h.horario, h.inscritos, c.tipo, c.descripcion, c.idmodulo, c.id_usuario, c.estado, u.nombre AS ubicacion FROM cursos c
	INNER JOIN horarios h ON c.id = h.cursoid
	INNER JOIN ubicacion u ON h.ubicacionid = u.id
	WHERE h.idprofesor = $docenteid AND c.estado = 0";

	$resultado = "";
	$i = 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			$tipo = $rw['tipo'];
			$estado = $rw['estado'];
			if ($tipo == 1) {
				$tipo = 'Curso';
			} elseif ($tipo == 2) {
				$tipo = 'Taller';
			}
			if ($estado == 0) {
				$estado = 'Activo';
			} elseif ($estado == 1) {
				$estado = 'Inactivo';
			}
			$resultado .= '<tr>
			<td>'.$tipo.'</td>
			<td>'.$rw['nombre'].'</td>
			<td>'.$rw['horario'].'</td>
			<td>'.$estado.'</td>
			<td class="text-right">
			<a type="button" data-toggle="modal" data-target="#modalInfo" data-tipo="verInfoCurso" data-id="'.$rw['id_horario'].'" href="#"><i class="fa fa-search icon-view"></i></a>
			</td>
			</tr>';
		}
	} else {
		$resultado = "";
	}

	$tabla = '<div class="p-20">
	<div class="mb-20">
	<button onclick="showContent(1)" type="button" class="btn btn-default">Ver Todos</button>
	</div>
	<div data-example-id="simple-responsive-table" class="bs-example">
	<div class="table-responsive">
	<table id="estudiantes" class="table table-striped table-hover">
	<thead>
	<tr>
	<th>Tipo</th>
	<th>Nombre</th>
	<th>Horario</th>
	<th>Estado</th>
	<th></th>
	</tr>
	</thead>
	<tbody>
	'. $resultado .'
	</tbody>
	</table>
	</div>
	</div>
	</div>';

	return $tabla;
}

function consultarEstudiantesDocente($docenteid) {#Muestra la tabla de estudiantes
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
		die('Error en Conexión: ' . mysqli_error($dbh));
		exit;
	}

	$sql = "SELECT DISTINCT e.* FROM estudiantes e INNER JOIN inscripcion i ON e.id = i.idestudiante
	INNER JOIN horarios h ON i.idhorario = h.id_horario
	WHERE h.idprofesor = $docenteid";

	$resultado = "";
	$i = 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			$documento = $rw['documentacion'];
			list($n1) = explode(" ", $rw['nombre']);
			list($a1) = explode(" ", $rw['apellido']);
			$nombre = $n1 ." ". $a1;
			if ($rw['estado'] == 0) {
				$estado = 'Inactivo';
			} elseif ($rw['estado'] == 1) {
				$estado = 'Activo';
			} elseif ($rw['estado'] == 2) {
				$estado = 'Suspendido';
			}
			$resultado .= '<tr>
			<td>'.$documento.'</td>
			<td>'.$nombre.'</td>
			<td><small>'.$rw['correo'].'</small></td>
			<td>'.$estado.'</td>
			<td class="text-right">
			<a type="button" data-toggle="modal" data-target="#modalInfo" data-tipo="verEstDocente" data-id="'.$rw['id'].'" data-id-docente="'.$docenteid.'" href="#"><i class="fa fa-search icon-view"></i></a>
			</td>
			</tr>';
		}
	} else {
		$resultado = "";
	}

	$tabla = '<div class="p-20">
	<div class="mb-20">
	<button onclick="showContent(21)" type="button" class="btn btn-default">Ver Morosos</button>
	</div>
	<div data-example-id="simple-responsive-table" class="bs-example">
	<div class="table-responsive">
	<table id="estudiantes" class="table table-striped table-hover">
	<thead>
	<tr>
	<th>ID</th>
	<th>Nombre</th>
	<th>Correo</th>
	<th>Estado</th>
	<th></th>
	</tr>
	</thead>
	<tbody>
	'. $resultado .'
	</tbody>
	</table>
	</div>
	</div>
	</div>';

	return $tabla;
}

function consultarEstudiantesMorososDocente($docenteid) {#Muestra la tabla de estudiantes
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
		die('Error en Conexión: ' . mysqli_error($dbh));
		exit;
	}

	$sql = "SELECT DISTINCT e.* FROM estudiantes e INNER JOIN inscripcion i ON e.id = i.idestudiante
	INNER JOIN horarios h ON i.idhorario = h.id_horario
	WHERE h.idprofesor = $docenteid AND i.status = 1 AND CURDATE() >= ( DATE_ADD(h.fechainicio, INTERVAL ROUND(datediff(h.fechafin, h.fechainicio)/2, 0) DAY )) AND h.fechafin > CURDATE()";

	$resultado = "";
	$i = 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			$documento = $rw['documentacion'];
			list($n1) = explode(" ", $rw['nombre']);
			list($a1) = explode(" ", $rw['apellido']);
			$nombre = $n1 ." ". $a1;
			if ($rw['estado'] == 0) {
				$estado = 'Inactivo';
			} elseif ($rw['estado'] == 1) {
				$estado = 'Activo';
			} elseif ($rw['estado'] == 2) {
				$estado = 'Suspendido';
			}
			$resultado .= '<tr>
			<td>'.$documento.'</td>
			<td>'.$nombre.'</td>
			<td><small>'.$rw['correo'].'</small></td>
			<td>'.$estado.'</td>
			<td class="text-right">
			<a type="button" data-toggle="modal" data-target="#modalInfo" data-tipo="verEstDocente" data-id="'.$rw['id'].'" data-id-docente="'.$docenteid.'" href="#"><i class="fa fa-search icon-view"></i></a>
			</td>
			</tr>';
		}
	} else {
		$resultado = "";
	}

	$tabla = '<div class="p-20">
	<div class="mb-20">
	<button onclick="showContent(2)" type="button" class="btn btn-default">Ver Todos</button>
	</div>
	<div data-example-id="simple-responsive-table" class="bs-example">
	<div class="table-responsive">
	<table id="estudiantes" class="table table-striped table-hover">
	<thead>
	<tr>
	<th>ID</th>
	<th>Nombre</th>
	<th>Correo</th>
	<th>Estado</th>
	<th></th>
	</tr>
	</thead>
	<tbody>
	'. $resultado .'
	</tbody>
	</table>
	</div>
	</div>
	</div>';

	return $tabla;
}

function verInfoEstudianteDocente($iddocente, $idestudiante) {
	$dbh = dbconnlocal2();
    mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT * FROM estudiantes WHERE id = ".$idestudiante."";

	$final = array();
	$resultado = "";
    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
	    $rw = mysqli_fetch_array($search);
	    $nombre = $rw['nombre']." ".$rw['apellido'];
	    $doc = $rw['documentacion'];
	   	if ($rw['estado'] == 0) {
	   		$estado = 'Inactivo';
	   	} elseif ($rw['estado'] == 1) {
	   		$estado = 'Activo';
	    } elseif ($rw['estado'] == 2) {
	    	$estado = 'Suspendido';
	    }
	   	if ($rw['genero'] == 1) {
	   		$genero = 'Femenino';
	   	} elseif ($rw['genero'] == 2) {
	   		$genero = 'Masculino';
		}
		$tablaCursos = consultarCursosEstudianteDocente($iddocente, $idestudiante);
    	$resultado = '<div class="row mb-5">
						<div class="col-md-12">
					      <div class="col-md-6"><b>Documento:</b> '.$doc.'</div>
					      <div class="col-md-6"><b>Estado:</b> '.$estado.'</div>
					    </div>
					</div>
					<div class="row mb-5">
						<div class="col-md-12">
				          <div class="col-md-6"><b>Nombre:</b> '.$rw['nombre'].'</div>
					      <div class="col-md-6"><b>Apellido:</b> '.$rw['apellido'].' </div>
				 	    </div>
					</div>
					<div class="row mb-5">
					  <div class="col-md-12">
					  	  <div class="col-md-6">
		    			  	<b>Fecha de Nacimiento:</b>
						  	  '.date_format(date_create($rw['fechadenacimiento']), 'd-m-Y').'
					  	  </div>
					      <div class="col-md-6">
					        <b>Género:</b> '.$genero.'
						  </div>
					  </div>
					</div>
					<div class="row mb-5">
					  <div class="col-md-12">
			   	        <div class="col-md-6">
				          <b>Fecha de Registro:</b> '.date_format(date_create($rw['fecha_reg']), 'd-m-Y').'
	  			        </div>
					    <div class="col-md-6">
			   	          <b>Hora de Registro:</b> '.$rw['hora_reg'].'
					    </div>
					  </div>
					</div>
					<div class="row mb-5">
					  <div class="col-md-12">
					      <div class="col-md-12"><b>Profesión:</b> '.$rw['profesion'].' </div>
					  </div>
					</div>
					<div class="row mb-5">
					  <div class="col-md-12">
					      <div class="col-md-12"><b>Correo Electrónico:</b> '.$rw['correo'].' </div>
					  </div>
					</div>
					<div class="row mb-5">
					  <div class="col-md-12">
					      <div class="col-md-12">
					        <b>Teléfonos:</b> '.$rw['telef1'].' - '.$rw['telef2'].' - '.$rw['telef3'].'
					      </div>
					  </div>
					</div>
					<div class="row mb-5">
					  <div class="col-md-12">
					      <div class="col-md-12">
					        <b>Dirección:</b> '.ucfirst($rw['direccion']).'
					      </div>
					  </div>
					</div>
					<div class="row mb-5">
						<div class="col-md-12">
							<div class="col-md-12"><b>Cursos:</b> </div>
							<div class="col-md-12 mt-10">'.$tablaCursos.' </div>
						</div>
					</div>';
	} else {
	    $resultado = "<div class='alert alert-danger'>Lo sentimos, actualmente la información no está disponible intente más tarde nuevamente.</div>";
	}

	$final[0] = $resultado;
	$final[1] = $nombre;

	return json_encode($final);
}

function consultarCursosEstudianteDocente($iddocente, $idestudiante) {
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
		die('Error en Conexión: ' . mysqli_error($dbh));
		exit;
	}

	$sql = "SELECT c.nombre, h.horario, c.tipo, c.estado FROM cursos c
	INNER JOIN horarios h ON c.id = h.cursoid
	INNER JOIN inscripcion i ON h.id_horario = i.idhorario
	INNER JOIN estudiantes e ON i.idestudiante = e.id
	WHERE e.id = $idestudiante AND h.idprofesor = $iddocente";

	$result = "";
	$tabla = "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			$tipo = $rw['tipo'];
			$estado = $rw['estado'];
			if ($tipo == 1) {
				$tipo = 'Curso';
			} elseif ($tipo == 2) {
				$tipo = 'Taller';
			}
			if ($estado == 0) {
				$estado = 'Activo';
			} elseif ($estado == 1) {
				$estado = 'Inactivo';
			}
			$result .= '<tr>
			<td>'.$tipo.'</td>
			<td>'.$rw['nombre'].'</td>
			<td>'.$rw['horario'].'</td>
			<td>'.$estado.'</td>
			</tr>';
		}
	}
	$tabla = '<table class="table table-striped table-bordered table-hover">
	<tr>
	<th>ID</th>
	<th>Nombre</th>
	<th>Correo</th>
	<th>Estado</th>
	</tr>
	'.$result.'
	</table>';

	return $tabla;
}

function cargarCursosNotas($docente) {
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
		die('Error en Conexión: ' . mysqli_error($dbh));
		exit;
	}

	$sql = "SELECT c.id, c.nombre, h.id_horario, h.horario, h.inscritos, c.tipo, c.descripcion, c.idmodulo, c.id_usuario, c.estado FROM cursos c
	INNER JOIN horarios h ON c.id = h.cursoid
	WHERE h.idprofesor = $docente AND c.estado = 0";

	$resultado = "";
	$i = 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			$tipo = $rw['tipo'];
			$estado = $rw['estado'];
			if ($tipo == 1) {
				$tipo = 'Curso';
			} elseif ($tipo == 2) {
				$tipo = 'Taller';
			}
			if ($estado == 0) {
				$estado = 'Activo';
			} elseif ($estado == 1) {
				$estado = 'Inactivo';
			}
			$resultado .= '<tr class="clickable" onclick="showContent(31)" data-tipo="prueba" data-id="'.$rw['id_horario'].'">
			<td>'.$tipo.'</td>
			<td>'.$rw['nombre'].'</td>
			<td>'.$rw['horario'].'</td>
			<td>'.$estado.'</td>
			<td class="text-right">
			</td>
			</tr>';
		}
	} else {
		$resultado = "";
	}

	$tabla = '<div class="p-20">
	<div class="mb-20">
	<h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-20">Selecciona el curso para subir <span class="text-theme-colored">Notas</span></h2>
	</div>
	<div data-example-id="simple-responsive-table" class="bs-example">
	<div class="table-responsive">
	<table id="cursos" class="table table-striped table-hover">
	<thead>
	<tr>
	<th>Tipo</th>
	<th>Nombre</th>
	<th>Horario</th>
	<th>Estado</th>
	<th></th>
	</tr>
	</thead>
	<tbody>
	'. $resultado .'
	</tbody>
	</table>
	</div>
	</div>
	</div>';

	return $tabla;
}

function cargarEstudiantesNota($horario) {
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
		die('Error en Conexión: ' . mysqli_error($dbh));
		exit;
	}

	$sql = "SELECT e.*, i.*, c.*, n.* FROM estudiantes e
	INNER JOIN inscripcion i ON e.id = i.idestudiante
	INNER JOIN horarios h ON i.idhorario = h.id_horario
	INNER JOIN cursos c ON h.cursoid = c.id
	LEFT JOIN notas n ON i.inscripcion_id = n.id_inscripcion
	WHERE i.idhorario = $horario";

	$resultado = "";
	$tabla = "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		while ($rw = mysqli_fetch_array($search)) {
			if ($rw['inscripcion'] == 1) {
				list($n1) = explode(" ", $rw['nombre']);
				list($a1) = explode(" ", $rw['apellido']);
				$user = $n1 ." ". $a1;
				$nota = "S/I";
				if (isset($rw['final'])) {
					$nota = $rw['final'];
				}
				$resultado .= '<tr>
				<td>'.$rw['documentacion'].'</td>
				<td>'.$user.'</td>
				<td>'.$rw['correo'].'
				<td>'.$nota.'</td>
				<td class="text-right">
				<a type="button" data-toggle="modal" data-target="#modalInfo" data-tipo="editarNotas" data-id="'.$rw['inscripcion_id'].'" href="#"><i class="fa fa-edit icon-edit"></i></a>
				</td>
				</tr>';
			}
		}
	}
	$tabla = '<div class="p-20">
	<div class="mb-20">
	<h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-20">Selecciona el Estudiante a <span class="text-theme-colored">Evaluar</span></h2>
	</div>
	<div data-example-id="simple-responsive-table" class="bs-example">
	<div class="table-responsive">
	<table id="estudiantes" class="table table-striped table-hover">
	<thead>
	<tr>
	<th>Documento</th>
	<th>Nombre</th>
	<th>Correo</th>
	<th>Nota Actual</th>
	<th></th>
	</tr>
	</thead>
	<tbody>
	'. $resultado .'
	</tbody>
	</table>
	</div>
	</div>
	</div>';

	return $tabla;
}

function editarNotas($inscripcionid) {
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	$sql = "SELECT i.*, n.*, n.id as idnota, c.*, c.id as idcurso FROM inscripcion i
	INNER JOIN horarios h ON i.idhorario = h.id_horario
	INNER JOIN cursos c ON h.cursoid = c.id
	LEFT JOIN notas n ON i.inscripcion_id = n.id_inscripcion
	WHERE i.inscripcion_id = $inscripcionid";

	$final = array();
	$resultado = "";
	$inputs = "";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$nombrecurso = "";
		$rw = mysqli_fetch_array($search);
		if (isset($rw['idnota'])) {
			$idnota = $rw['idnota'];
		} else {
			$idnota = null;
		}
		$i = 1;
		while ($i <= $rw['cant_notas']) {
			$inputs .= '<div class="col-sm-12">
			<div class="form-group mb-10">
			<label>Nota '.$i.'</label>
			<input value="'.$rw["nota$i"].'" type="number" name="nota'.$i.'" id="nota'.$i.'" class="form-control">
			</div>
			</div>';
			$i++;
		}
		$resultado = '<div class="p-30">
		<form class="mb-0" action="/api.php" method="POST" enctype="multipart/form-data">
		<div class="row">
			'.$inputs.'
		</div>
		<input type="hidden" name="tipo" value="modificatNotas">
		<input type="hidden" name="idcurso" value="'.$rw["idcurso"].'">
		<input type="hidden" name="idinscripcion" value="'.$rw["inscripcion_id"].'">
		<input type="hidden" name="idnotas" value="'.$idnota.'">
		<input type="hidden" name="cant_notas" value="'.$rw["cant_notas"].'">
		<input type="hidden" name="porc_nota1" value="'.$rw["porc_nota1"].'">
		<input type="hidden" name="porc_nota2" value="'.$rw["porc_nota2"].'">
		<input type="hidden" name="porc_nota3" value="'.$rw["porc_nota3"].'">
		<input type="hidden" name="porc_nota4" value="'.$rw["porc_nota4"].'">
		<input type="hidden" name="porc_nota5" value="'.$rw["porc_nota5"].'">
		<input type="hidden" name="tipo_eval" value="'.$rw["tipo_eval"].'">
		<center class="mt-20">
			<button type="submit" name="modificarNotas" id="modificatNotas" class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-colored-4px">Guardar</button>
		</center>
		</form></div>';
	} else {
		$nombrecurso = "Información no disponible";
		$resultado = "<div class='alert alert-danger'>Lo sentimos, actualmente la información no se encuentra disponible intenta nuevamente.</div>";
	}
	$final[0] = $resultado;
	$final[1] = $nombrecurso;

	return json_encode($final);
}

function modificatNotas($idcurso, $idinscripcion, $idnotas, $nota1, $nota2, $nota3, $nota4, $nota5, $cant_notas, $porc_nota1, $porc_nota2, $porc_nota3, $porc_nota4, $porc_nota5, $tipo_eval) {
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	if ($tipo_eval == 'porcentual') {
		if ($cant_notas == 1) {
			$final = ($nota1 * ($porc_nota1 / 100));
		} else if ($cant_notas == 2) {
			$final = ($nota1 * ($porc_nota1 / 100)) + ($nota2 * ($porc_nota2 / 100));
		} else if ($cant_notas == 3) {
			$final = ($nota1 * ($porc_nota1 / 100)) + ($nota2 * ($porc_nota2 / 100)) + ($nota3 * ($porc_nota3 / 100));
		} else if ($cant_notas == 4) {
			$final = ($nota1 * ($porc_nota1 / 100)) + ($nota2 * ($porc_nota2 / 100)) + ($nota3 * ($porc_nota3 / 100)) + ($nota4 * ($porc_nota4 / 100));
		} else if ($cant_notas == 5) {
			$final = ($nota1 * ($porc_nota1 / 100)) + ($nota2 * ($porc_nota2 / 100)) + ($nota3 * ($porc_nota3 / 100)) + ($nota4 * ($porc_nota4 / 100)) + ($nota5 * ($porc_nota5 / 100));
		}
	} else {
		if ($cant_notas == 1) {
			$final = $nota1;
		} else if ($cant_notas == 2) {
			$final = $nota1 + $nota2;
		} else if ($cant_notas == 3) {
			$final = $nota1 + $nota2 + $nota3;
		} else if ($cant_notas == 4) {
			$final = $nota1 + $nota2 + $nota3 + $nota4;
		} else if ($cant_notas == 5) {
			$final = $nota1 + $nota2 + $nota3 + $nota4 + $nota5;
		}
		$final = $final / $cant_notas;
	}

	$sql = "SELECT * FROM notas WHERE id = $idnotas";
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$sql = "UPDATE notas SET nota1 = $nota1, nota2 = $nota2, nota3 = $nota3, nota4 = $nota4, nota5 = $nota5, final = $final WHERE id = $idnotas";
		if (mysqli_query($dbh, $sql)) {
			header('Location: ../docente/index.php?updatingNotas=success&tipo=1');
			die();
		} else {
			header('Location: ../docente/index.php?updatingNotas=fail&tipo=1');
			die();
		}
	} else {
		$sql = "INSERT INTO notas(id_inscripcion, id_curso, nota1, nota2, nota3, nota4, nota5, final) VALUES($idinscripcion, $idcurso, $nota1, $nota2, $nota3, $nota4, $nota5, $final)";
		if (mysqli_query($dbh, $sql)) {
			header('Location: ../docente/index.php?updatingNotas=success&tipo=1');
			die();
		} else {
			header('Location: ../docente/index.php?updatingNotas=fail&tipo=1');
			die();
		}
	}
}

function formGuiaDocente($idcurso) {
	$archivos = showArchivosDocente($idcurso);
	$formulario = '<div class="p-20">
		<ul class="list-group">
		<a class="list-group-item disabled">
		<b>Listado de archivos cargados</b>
		</a>'.$archivos.'
		</ul>
		<form action="/api.php" method="POST" enctype="multipart/form-data">
		<div class="row">
		<div class="col-sm-12">
		<div class="form-group mb-10">
		<label for="imagen">Imagen del curso o taller</label>
		<input type="file" name="guia" id="guia" required="">
		<p class="help-block">Seleccione una guia para el curso.</p>
		</div>
	  </div>
		</div>
		<input type="hidden" name="tipo" value="updateGuiaDocente">
		<input type="hidden" name="idcurso" id="idcurso" value="'.$idcurso.'">
		<center class="mt-20">
		<button type="submit" name="savect" id="savect" class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-colored-4px mt-0">Guardar</button>
		</center>
		</form>
		</div>';

	return $formulario;
}

function showArchivosDocente($idcurso){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "SELECT * FROM guias WHERE id_curso = $idcurso";

	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	$lista = "";
	if ($match > 0) {
		$dirname = "componentes/cursos/";
		chdir($dirname);
		$archivos = glob("*.{pdf,doc,docx,txt}", GLOB_BRACE);
		while ($rw = mysqli_fetch_array($search)) {
			$lista .= '<li class="list-group-item"><a href="/'.$dirname.$rw["nombre"].'" download><i class="glyphicon glyphicon-download-alt" style="cursor: pointer;"></i></a> <span onclick="deleteFileDocente('.$rw["id"].')" class="badge" style="cursor: pointer;"><i class="glyphicon glyphicon-trash"></i></span>'.$rw["nombre"].'</li>';
		}
	} else {
	}
	return $lista;
}

function updateGuiaDocente($idcurso, $guia){
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$guia = uploadFileDocente($guia);

	if ($guia[0] == 1) {
		$sql = "INSERT INTO guias (nombre, id_curso) values ('$guia[1]', $idcurso)";

		$result = "";
		if (mysqli_query($dbh, $sql)) {
			header('Location: ../docente/index.php?ctupd=success&msg='.$guia[0].'');
			die();
			$result = 1;
		} else {
			header('Location: ../docente/index.php?ctupd=fail&msg='.$guia[0].'');
			die();
			$result = 0;
		}
	} else {
		header('Location: ../docente/index.php?ctupd=fail&msg='.$guia[0].'');
		die();
	}

	return $result;
}

function uploadFileDocente($archivo){
	$resultado = array();
	$nombre = "";
	if (isset($archivo)) {
    $target_dir = "componentes/cursos/";
    if (!file_exists($target_dir)) {
      mkdir($target_dir, 0777, true);
    }
    $target_file = $target_dir . basename($archivo["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if file already exists
    if (file_exists($target_file)) {
      $msg = 7;
      //echo "Sorry, file already exists.";
      $uploadOk = 0;
    }
    // Check file size
    if ($archivo["size"] > 5000000) {
      //echo "Sorry, your file is too large.";
      $msg = 8;
      $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "pdf" ) {
      //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $msg = 5;
      $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      //echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
    } else {
      if (move_uploaded_file($archivo["tmp_name"], $target_file)) {
        //REGISTRO en BASE DE DATOS
        $nombre = $archivo["name"];
        $msg = 1;
      } else {
        //echo "Sorry, there was an error uploading your file.";
        $msg = 2;
      }
    }
  }

	$resultado[0] = $msg;
	$resultado[1] = $nombre;

  return $resultado;
}

function deleteFileDocente($id) {
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "DELETE FROM guias  WHERE id = $id";

	mysqli_autocommit($dbh, FALSE);

	$result = "";
	$fileDeleted = findFileAndDeleteDocente($id);
	if (mysqli_query($dbh, $sql)) {
		if ($fileDeleted) {
			mysqli_commit($dbh);
			$result = '<div class="mt-30 mb-30"><div class="alert alert-success"><strong>¡Eliminado exitosamente!</strong> se eliminó la guía seleccionada exitosamente.</div></div>';
		}
	} else {
		mysqli_rollback($dbh);
		$result = '<div class="alert alert-danger"><strong>¡Error!</strong> no se eliminó la guía. </div>';
	}

	mysqli_close($dbh);
	return $result;
}

function findFileAndDeleteDocente($id) {
	if (isset($id)) {
		$dbh = dbconnlocal2();
		mysqli_set_charset($dbh, 'utf8');

		if (!$dbh) {
			die('Error en Conexión: ' . mysqli_error($dbh));
			exit;
		}

		$sql = "SELECT * FROM guias WHERE id = $id";
		$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
		$match = mysqli_num_rows($search);
		if ($match > 0) {
			$rw = mysqli_fetch_array($search);
			$target_dir = "componentes/cursos/";
			if (file_exists($target_dir)) {
				$target_file = $target_dir . basename($rw["nombre"]);
				if (file_exists($target_file)) {
					if (unlink($target_file)) {
						return true;
					} else {
						return false;
					}
				} else {
					return true;
				}
			} else {
				return true;
			}
		} else {
			return false;
		}
  } else {
		return false;
	}
}
?>