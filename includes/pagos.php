<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
error_log(1);

function instaPago($idinscripcion){
    include_once 'correos.php';
	include_once 'estudios.php';
	$pagar = $_POST["opc-pago"];
	if ($pagar == 'otro') {
		$monto = $_POST["monto"];
	} else {
		$monto = $pagar;
	}
	$nombre = $_POST["nombre"];
	$cedula = $_POST["cedula"];
	$tdc = $_POST["tdc"];
	$cod = $_POST["cod"];
	$month = $_POST["month"];
	$year = $_POST["year"];
	$ip = get_client_ip_server();
	$resulta = "";
	$transaccion = true;
    $datei = date_create("10-".$month."-".$year."");
    $datef = date_format($datei,"Y-m-t");
    $i = 0;

    if (is_numeric($monto)) {
        $i++;
    } else{
        header('Location: ../estudios/index.php?errorTDC=01');
        exit;
    }
    if (preg_match("/^[a-zA-Z ]*$/",$nombre)) {
        $i++;
    } else{
        header('Location: ../estudios/index.php?errorTDC=02');
        exit;
    }
    if (filter_var($cedula, FILTER_VALIDATE_INT)) {
        $i++;
    } else{
        header('Location: ../estudios/index.php?errorTDC=03');
        exit;
    }
    if (filter_var($tdc, FILTER_VALIDATE_INT)) {
        $i++;
    } else{
        header('Location: ../estudios/index.php?errorTDC=04');
        exit;
    } 
    if (filter_var($cod, FILTER_VALIDATE_INT)) {
        $i++;
    } else{
        header('Location: ../estudios/index.php?errorTDC=05');
        exit;
    } 
    if ($datef >= date('Y-m-d')) {
        $i++;
    } else{
        header('Location: ../estudios/index.php?errorTDC=06');
        exit;
    }

    if ($i == 6) {
        
        $error = hacerpost("https://api.instapago.com/payment?", "KeyId=E707AD37-C03F-44FF-B638-7814C7BB1A60&PublicKeyId=3e58f791ef149bec1f7c7154321fb808&Amount=".$monto."&Description=Pago+De+Curso&CardHolder=".$nombre."&CardHolderId=".$cedula."&CardNumber=".$tdc."&CVC=".$cod."&ExpirationDate=".$month."%2F".$year."&StatusId=2&IP=".$ip."", $resulta);

        if ($error == ""){
            header("Location: estudios/index.php?pay=error");
        } else {
            $respuesta = json_decode($error, true);
            $transaccion = $respuesta["success"];
            $voucher = $respuesta["voucher"];

            if ($transaccion === true) {
                include_once 'estudios.php';
                $codigo = $respuesta['code']; 
                $mensaje = $respuesta['message'];
                $response = $respuesta['responsecode'];
                $referencia = $respuesta['reference'];
                $user = null;
                $metodo = 1;
                $banco = 'Banesco';
                $voucher = html_entity_decode($respuesta['voucher']);
                guardarTransaccion($idinscripcion, $codigo, $mensaje, $response, $voucher, $monto, $referencia, $user, $metodo, $banco);
                echo enviarComprobante($idinscripcion, $referencia);
                header("Location: estudios/index.php?pay=success&ref=".$referencia."");
            } elseif ($transaccion === false) {
                $codigo = $respuesta['code']; 
                $mensaje = $respuesta['message'];
                $response = "";
                $voucher = "";
                $referencia = "";
                $metodo = 1;
                $user = null;
                $banco = 'Banesco';
                guardarTransaccion($idinscripcion, $codigo, $mensaje, $response, $voucher, $monto, $referencia, $user, $metodo, $banco);
                header("Location: estudios/index.php?pay=fail&msg=".$mensaje."");
            }
        }

    } else {
        header('Location:  ../estudios/index.php?errorTDC=99');
        exit;
    }

	return;
}

function hacerpost($url, $parametros, &$resultado){
    //La funcion devuelve espacio en blanco si la variable resultado va cargada, de lo contrario devuelve error
    // abrimos la sesion cURL
    $ch = curl_init();
    // definimos la URL a la que hacemos la peticion
    curl_setopt($ch, CURLOPT_URL,$url);
    // indicamos el tipo de peticion: POST
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $parametros);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    // recibimos la respuesta y la guardamos en una variable
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $remote_server_output = curl_exec ($ch);
    // cerramos la sesion cURL
    curl_close ($ch);
    //Comenzamos el manejo de errores
    if ($remote_server_output == "") {
        $remote_server_output = "Error 1: Fallo de conexion.";
    }
    if (strtoupper(substr($remote_server_output, 0, 1)) == "["){
        $resultado = $remote_server_output;
        return "";
    }
    else {
        //Dio error
        $resultado = "";
        return $remote_server_output;
    }
}

// Function to get the client ip address
function get_client_ip_server() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
 
    return $ipaddress;
}

?>