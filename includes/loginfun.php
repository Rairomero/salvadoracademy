<?php

function entrar($user, $password, $posicion){ // Login (Estudiantes - Profesores - Administradores)
		$dbh = dbconnlocal2();
	    mysqli_set_charset($dbh, 'utf8');

	    header('Content-Type: text/html; charset=UTF-8');

	    if (!$dbh) {
	        die('Error en Conexión: ' . mysqli_error($dbh));
	        exit;
	    }

	    if ($posicion == 'est') {
	    	$sql = "SELECT * FROM estudiantes WHERE correo = '$user' AND password = '$password'";
	    } else if ($posicion == 'adm' || $posicion == 'doc') {
	    	$sql = "SELECT * FROM usuarios WHERE correo = '$user' AND password = '$password'";
	    }

	    $result = array();
	    $i = 0;
	    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	    $match = mysqli_num_rows($search);
	    if ($match > 0) {
	        while ($rw = mysqli_fetch_array($search)) {
	        	switch ($posicion) {
	        		case 'est':
	        			$status = $rw['estado'];
	        			$idestudiante = $rw['id'];
	        			if ($status == 1) {
	        				if (session_status() === PHP_SESSION_NONE) {
								session_start();
								$_SESSION["academy-usuario"] = $rw['correo'];
								$_SESSION["academy-id"] = $rw['id'];
								$_SESSION["academy-nombre"] = $rw['nombre'];
								$_SESSION["academy-apellido"] = $rw['apellido'];
								$_SESSION["academy-estado"] = $rw['estado'];
								$_SESSION["acesso"] = $posicion;
							}
	        				header('Location: /estudios/index.php');
	        			} elseif ($status == 0) {
	        				if (session_status() === PHP_SESSION_NONE) {
	        					session_start();
								$_SESSION["academy-usuario"] = $rw['correo'];
								$_SESSION["academy-id"] = $rw['id'];
								$_SESSION["academy-nombre"] = $rw['nombre'];
								$_SESSION["academy-apellido"] = $rw['apellido'];
								$_SESSION["academy-estado"] = $rw['estado'];
								$_SESSION["acesso"] = $posicion;
	        				}
	        				header('Location: /estudios/cambio.php?e=2');
	        			} elseif ($status == 2) {
	        				header('Location: /estudios/login.php?e=3');
	        			} 
	        			break;
	        		case 'doc':
	        			$permiso = $rw['permisos']; // Verificar si usuario tiene permisos
	        			$status = $rw['estado']; // Verificar estado de la cuenta = 0: activo, 1: inactivo
	        			if ($permiso == 10) {
	        				if ($status == 1) {
	        					if (session_status() === PHP_SESSION_NONE) {
									session_start();
									$_SESSION["academy-usuario"] = $rw['correo'];
									$_SESSION["academy-id"] = $rw['id'];
									$_SESSION["academy-nombre"] = $rw['nombre'];
									$_SESSION["academy-apellido"] = $rw['apellido'];
									$_SESSION["academy-estado"] = $rw['estado'];
	        						$_SESSION["acesso"] = $posicion;
								}
	        					header('Location: /docente/index.php');
	        				} else if ($status == 0) {
	        					if (session_status() === PHP_SESSION_NONE) {
									session_start();
									$_SESSION["academy-usuario"] = $rw['correo'];
									$_SESSION["academy-id"] = $rw['id'];
									$_SESSION["academy-nombre"] = $rw['nombre'];
									$_SESSION["academy-apellido"] = $rw['apellido'];
									$_SESSION["academy-estado"] = $rw['estado'];
									$_SESSION["acesso"] = $posicion;
								}
	        					header('Location: /docente/cambio.php?e=2');
	        				} else if ($status == 2) {
	        					header('Location: /docente/login.php?e=9');
	        				}
	        			} else if ($permiso == 20) {
	        				if ($status == 1) {
	        					if (session_status() === PHP_SESSION_NONE) {
									session_start();
									$_SESSION["academy-usuario"] = $rw['correo'];
									$_SESSION["academy-id"] = $rw['id'];
									$_SESSION["academy-nombre"] = $rw['nombre'];
									$_SESSION["academy-apellido"] = $rw['apellido'];
									$_SESSION["academy-estado"] = $rw['estado'];
									$_SESSION["acesso"] = $posicion;
								}
	        					header('Location: /admin/index.php');
	        				} else if ($status == 0) {
	        					if (session_status() === PHP_SESSION_NONE) {
									session_start();
									$_SESSION["academy-usuario"] = $rw['correo'];
									$_SESSION["academy-id"] = $rw['id'];
									$_SESSION["academy-nombre"] = $rw['nombre'];
									$_SESSION["academy-apellido"] = $rw['apellido'];
									$_SESSION["academy-estado"] = $rw['estado'];
									$_SESSION["acesso"] = $posicion;
								}
	        					header('Location: /admin/cambio.php?e=2');
	        				} else if ($status == 2) {
	        					header('Location: /admin/login.php?e=9');
	        				}
	        			}
	        			break;
	        		default:
	        			# code...
	        			break;
	        	} 	        	
	        }
	    } else {
	    	switch ($posicion) {
	        	case 'est':
	        		header('Location: /estudios/login.php?e=1');
	        		break;
	        	case 'doc':
	        		header('Location: /docente/login.php?e=1');
	        		break;
	        	default:
	        		# code...
	        	break;
	        } 
		} 
	return;
	
}

function reguser($info2){ // Registro de Usuario Nuevo
		$dbh = dbconnlocal2();
	    mysqli_set_charset($dbh, 'utf8');

	    if (!$dbh) {
	        die('Error en Conexión: ' . mysqli_error($dbh));
	        exit;
	    }

	    $password = rand(9999, 999999);
	    $origen = 1; # Registro

	    $sql = "INSERT INTO estudiantes (nombre, apellido, correo, documentacion, fechadenacimiento, genero, telef1, telef2, telef3, direccion, password, fecha_reg, hora_reg, estado, profesion, tipo_doc, estadocivil, paisnacimiento) VALUES ('".$info2[2]."', '".$info2[3]."', '".$info2[8]."', '".$info2[1]."', '".$info2[5]."', '".$info2[7]."', '".$info2[9]."', '".$info2[10]."', '".$info2[11]."', '".$info2[12]."', $password, CURDATE(), CURTIME(), 0, '".$info2[13]."', '".$info2[0]."', '".$info2[4]."', '".$info2[6]."')";

	    $result = array();
	    $i = 0;
	    if (mysqli_query($dbh, $sql)) {
	    	include 'correos.php';
	    	$nombre = $info2[2] ." ". $info2[3];
	    	$correo = $info2[8];
	    	mailRegistro($nombre, $correo, $password, $origen);
	    	if ($info2[14] == 1) {
	        	$mensaje = "<div class='text-center mt-50 mb-50'><h1><i class='fa fa-envelope'></i></h1><h3>Gracias por registrate, tu registro ha sido satisfactorio.</h3><h4>Se ha enviado un nuevo mensaje a tu correo electrónico con los datos de acceso.</h4></div>";
	    	} elseif ($info2[14] == 2) {
	    		header('Location: ../admin/index.php?addest=success');
	    	}
	    } else {
	    	if ($info2[14] == 1) {
				$mensaje = "<div class='text-center mt-50 mb-50'><h1><i class='fa fa-times'></i></h1><h3>Lo sentimos mucho, ha ocurrido un error inesperado.</h3><h4>Te invitamos a realizar el proceso de registro nuevamente para disfrutar de nuestros servicios.</h4></div>";
			} elseif ($info2[14] == 2) {
				header('Location: ../admin/index.php?addest=fail');
			}
	    }
	return $mensaje;
	
}

function reguser2($info2){ // Verifica si ID o Correo en DB ya esta registrado
		$dbh = dbconnlocal2();
	    mysqli_set_charset($dbh, 'utf8');

	    if (!$dbh) {
	        die('Error en Conexión: ' . mysqli_error($dbh));
	        exit;
	    }

	    $correo = $info2[8];
	    $id2 = $info2[1];

	    $sql = "SELECT * FROM estudiantes WHERE documentacion=$id2 OR `correo`='$correo'";

	    $resultado= "";
	    $i = 0;
	    $search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	    $match = mysqli_num_rows($search);
	    if ($match > 0) {
	        while ($rw = mysqli_fetch_array($search)) {

	        	$email = $rw['correo'];
	        	$cedula = $rw['documentacion'];
	        	if ($email == $correo && $cedula == $id2) {
	        		$resultado = "<div class='text-center mt-50 mb-50'><h1><i class='fa fa-times'></i></h1><h4>El correo y la cedula que has ingresado ya están registrado. Si no recuerdas tus datos de acceso, selecciona la opción olvidé mi contraseña en la sección de estudiantes.</h4><br><a href='/index.php' class='btn btn-default'>Intentar de Nuevo</a></div>";
	        	} elseif ($email == $correo) {
	        		$resultado = "<div class='text-center mt-50 mb-50'><h1><i class='fa fa-times'></i></h1><h4>El correo ingresado ya está registrado. Si no recuerdas tus datos de acceso, selecciona la opción olvidé mi contraseña en la sección de estudiantes.</h4><br><a href='/index.php' class='btn btn-default'>Intentar de Nuevo</a></div>";
	        	} elseif ($cedula == $id2) {
	        		$resultado = "<div class='text-center mt-50 mb-50'><h1><i class='fa fa-times'></i></h1><h4>El documento de identificación que has ingresado ya esta registrado. Si no recuerdas tus datos de acceso, selecciona la opción olvidé mi contraseña en la sección de estudiantes.</h4><br><a href='/index.php' class='btn btn-default'>Intentar de Nuevo</a></div>";
	        	}
                        	        	
	        }
	    } else {
	    	$resultado = reguser($info2);
	    }
	return $resultado;

}

function changePassword($iduser, $current, $new1, $new2, $origen){ # Comprobacion de la informacion ingresada para cambio de contraseña
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

	if ($origen == 'est') {
		$enlace = 'estudios'; $consulta = 'estudiantes';
	} elseif ($origen == 'adm' || $origen == 'doc') {
		$consulta = 'usuarios';
	} 

	$sql = "SELECT * FROM ".$consulta." WHERE id=".$iduser."";

	$resultado= "";
    $i = 0;
	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
	    while ($rw = mysqli_fetch_array($search)) {
	    	if (isset($rw['permisos'])) {
	    		$permisos = $rw['permisos'];
		    	if ($permisos == 10) {
		    		$enlace = 'docente';
		    	} elseif ($permisos == 20) {
		    		$enlace = 'admin';
		    	}
	    	}
	        $password = $rw['password'];
	        if ($password == $current) {
	        	if ($new1 == $new2) {
	        		$valor = savePassword($iduser, $new1, $consulta);
	        		if ($valor == 1) {
	        			$cambio = registrarCambio($iduser, $consulta);
	        			if ($cambio == 1) {
	    					header('Location: /'.$enlace.'/index.php?change=true');
	        			} else {
	    					header('Location: /'.$enlace.'/login.php?e=4');
	        			}
	        		} else {
	    				header('Location: /'.$enlace.'/login.php?e=4');
	        		}
	        	} else {
	        		header('Location: /'.$enlace.'/cambio.php?e=7');
	        	}
	        } else {
	        	header('Location: /'.$enlace.'/cambio.php?e=6');
	        }                        	        	
      	}
	} else {
	    header('Location: /'.$enlace.'/cambio.php?e=99');
	}
	return $resultado;
}

function savePassword($iduser, $new, $consulta){ # Actualiza la contraseña en la BD 
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "UPDATE ".$consulta." SET password='".$new."' WHERE id=".$iduser."";

	$result = "";
	if (mysqli_query($dbh, $sql)) {
	    $result = 1;    
	} else {
		$result = 0;
	}
	return $result;
}

function registrarCambio($iduser, $consulta){ # Actualiza el campo que indica si fue cambiada la contraseña
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	$sql = "UPDATE ".$consulta." SET estado=1 WHERE id=".$iduser."";

	$result = "";
	if (mysqli_query($dbh, $sql)) {
	    $result = 1;    
	} else {
		$result = 0;
	}
	return $result;
}

function recoverPassword($documento, $correo, $origen){// Verificacion de Datos para recuperar contraseña
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	  die('Error en Conexión: ' . mysqli_error($dbh));
	  exit;
	}

	if ($origen == 'est') {
		$sql = "SELECT * FROM estudiantes WHERE documentacion='".$documento."'"; $consulta = 'estudios';
	} else if ($origen == 'adm') {
    	$sql = "SELECT * FROM usuarios WHERE documentacion='".$documento."'"; $consulta = 'admin';
	} elseif ($origen == 'doc') {
		$sql = "SELECT * FROM usuarios WHERE documentacion='".$documento."'"; $consulta = 'docente';
	}

	$search = mysqli_query($dbh, $sql) or die(mysqli_error($dbh));
	$match = mysqli_num_rows($search);
	if ($match > 0) {
		$rw = mysqli_fetch_array($search);
		$email = $rw['correo'];
		$nombre = $rw['nombre'] .' '. $rw['apellido'];
		$id = $rw['id'];
		if (isset($rw['permisos'])) {
			if ($rw['permisos'] == 10) {
				$consulta = 'docente';
			} elseif ($rw['permisos'] == 20) {
				$consulta = 'admin';
			}
		}
		if ($email == $correo) {
			recoverPassword2($id, $nombre, $correo, $origen);
	    	header('Location: /'.$consulta.'/login.php?e=5');
		} else {
	    	header('Location: /'.$consulta.'/recuperar.php?e=8');
		}
	} else {
		header('Location: /'.$consulta.'/recuperar.php?e=9');
	}
	return;
}

function recoverPassword2($id, $nombre, $correo, $posicion){ // Actualiza contraseña luego de recuperar y estado=0 para que cambie contraseña
	include 'correos.php';
	$dbh = dbconnlocal2();
	mysqli_set_charset($dbh, 'utf8');

	if (!$dbh) {
	    die('Error en Conexión: ' . mysqli_error($dbh));
	    exit;
	}

    $password = rand(9999, 999999);

    if ($posicion == 'est') {
		$sql = "UPDATE estudiantes SET estado=0, password='".$password."' WHERE id=".$id."";
	} else if ($posicion == 'adm') {
    	$sql = "UPDATE usuarios SET estado=0, password='".$password."' WHERE id=".$id."";
	} elseif ($posicion == 'doc') {
		$sql = "UPDATE usuarios SET estado=0, password='".$password."' WHERE id=".$id."";
	}

	$result = "";
	if (mysqli_query($dbh, $sql)) {
	    $origen = 2; # Registro
	    $result = mailRegistro($nombre, $correo, $password, $origen);   
	} else {
		$result = 0;
	}
	return $result;
}

?>