<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Nosotros | Salvador Academy</title>

  <?php 
  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  include 'componentes/header.php'; 

  $ubicacion = "nosotros";

  include 'componentes/navbar.php'; 
  //cabecera("inicio"); 

  ?>

  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Nosotros</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: About  -->
    <section class="bg-lighter">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 pb-sm-20 mb-40">
              <h2 class="line-bottom font-25 text-theme-colored text-uppercase mb-10 mt-0"><span class="text-theme-color-2">MISIÓN</span></h2>
              <!--p class="lead font-18">SALVADOR ACADEMY</p-->
              <p>Liderizar en el mercado la capacitación y formación de capital humano, con sello de excelencia, en servicios de belleza integral, con iniciativas innovadoras que mejoren la calidad de vida y estilo de los profesionales.</p>
              <!--a class="btn btn-colored btn-theme-colored btn-sm" href="#">View Details</a-->
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 pb-sm-20 mb-40">
              <h2 class="line-bottom font-25 text-theme-colored text-uppercase mb-10 mt-0"><span class="text-theme-color-2">VISIÓN</span></h2>
              <!--p class="lead font-18">SALVADOR ACADEMY</p-->
              <p>Formar especialistas en estilismo con sello de excelencia, tanto en la dimensión humana como profesional, sustentados en los valores del Grupo Salvador, para ser promotores-generadores de nuevos conocimientos, investigación y tendencias que favorezcan el cuidado de la belleza integral de las personas.</p>
              <!--a class="btn btn-colored btn-theme-colored btn-sm" href="#">View Details</a-->
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 pb-sm-20">
              <div class="image-box-thum">
                <img class="img-fullwidth" alt="" src="/componentes/images/objetivos.jpg">
              </div>
              <div class="image-box-details pt-20 pb-sm-20">
                <h4 class="title text-uppercase font-22 text-theme-color-2 line-bottom mt-0">OBJETIVOS</h4>
                <p class="desc mb-10">
                  <li>Aportar oportunidades de formación como base y garantía de un buen desarrollo profesional.</li>
                  <li>Pulir el talento personal con amplios conocimientos, destrezas y herramientas para ser excelentes estilistas.</li>
                  <li>Consolidar valores humanos y éticos en los presentes y futuros empleados y asociados.</li>
                  <li>Contribuir al mejoramiento de la calidad de vida de los estudiantes.</li>
                  <li>Ser el centro de profesionalización de estilistas más importante en el ámbito nacional e internacional.</li>
                  <li>Hacer del estilismo un arte y una profesión.</li>
                </p>
                <!--a href="#" class="btn btn-xs btn-theme-colored">Read more</a-->
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 pb-sm-20">
              <div class="image-box-thum">
                <img class="img-fullwidth" alt="" src="/componentes/images/alumno.jpg">
              </div>
              <div class="image-box-details pt-20 pb-sm-20">
                <h4 class="title text-uppercase font-22 text-theme-color-2 line-bottom mt-0">PERFIL DEL ALUMNO</h4>
                <p class="desc mb-10">Debe ser una persona con 17 años cumplidos. No es necesario que sea estilista, manicurista, esteticista, pero debe sentir una inclinación y vocación natural para este arte. Que le guste, le apasione la belleza o desee aprender una destreza concreta del ámbito de la peluquería.</p>
                <!--a href="#" class="btn btn-xs btn-theme-colored">Read more</a-->
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="image-box-thum">
                <img class="img-fullwidth" alt="" src="/componentes/images/egresado.jpg">
              </div>
              <div class="image-box-details pt-20 pb-sm-20">
                <h4 class="title text-uppercase font-22 text-theme-color-2 line-bottom mt-0">PERFIL DEL EGRESADO</h4>
                <p class="desc mb-10">El egresado de la Academia Salvador será un Profesional Integral de la Belleza. Será, primeramente, una persona educada y capaz de atender al público de manera excelente y profesional. Estará en capacidad de atender a un cliente en cualquiera de los servicios de peluquería: análisis capilar, corte, secado, barbería, maquillaje, depilación y manicure y pedicure.  Podrá ser asociado de una de las peluquerías de Salvador; podrá abrir su propio negocio y prestar un servicio excelente; estará preparado para ser dueño de una de nuestras franquicias.</p>
                <!--a href="#" class="btn btn-xs btn-theme-colored">Read more</a-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Features -->
    <section id="features">
      <div class="container">
        <div class="section-title">
          <div class="row">
            <div class="col-md-12">
              <h2 class="text-uppercase text-theme-colored font line-bottom line-height-1 mt-0 mb-0">Nuestros <span class="text-theme-color-2 font-weight-400">Valores</span></h2>
            </div>
          </div>
        </div>
        <div class="row mtli-row-clearfix">
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-handshake-o"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">HONESTIDAD</h4>
                <p class="text-gray font-13 mb-0">Hacer que nuestro actuar sea recto, justo, decente, razonable. Nuestro pensar y actuar van en absoluta concordancia.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-signing"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">ENTUSIASMO</h4>
                <p class="text-gray font-13 mb-0">Hacemos nuestro trabajo con inspiración, convencidos de que todas las causas propuestas se logran con esfuerzo, dedicación y buen ánimo.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="pe-7s-light"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">RESPETO</h4>
                <p class="text-gray font-13 mb-0">Nos relacionamos con consideración al otro, acatamos las normas de cortesía porque el otro es importante, no por mero cumplimiento.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-hourglass-start"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">PUNTUALIDAD</h4>
                <p class="text-gray font-13 mb-0">Hacemos las cosas a su debido tiempo, con precisión y para el fin para el que las cosas son requeridas.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-users"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">COMPAÑERISMO</h4>
                <p class="text-gray font-13 mb-0">Trabajamos en armonía y con un vínculo de buena correspondencia: respeto, confianza y ayuda mutua.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-hand-peace-o"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">SINCERIDAD</h4>
                <p class="text-gray font-13 mb-0">Nos expresamos con sencillez, con criterios basados en la verdad y libres de fingimiento. Tenemos una sola cara.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-refresh"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">PERSEVERANCIA</h4>
                <p class="text-gray font-13 mb-0">Somos constantes en nuestro ser y hacer, sin vacilaciones, pues solo así se logran todos los objetivos.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-spinner"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">CONSTANCIA</h4>
                <p class="text-gray font-13 mb-0">Permanecemos firmes, perseverantes y animosos en todo trabajo que emprendemos.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="pe-7s-light"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">LEALTAD</h4>
                <p class="text-gray font-13 mb-0">Somos fieles y agradecidos porque recibimos y aportamos lo mejor día a día.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-check"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">HUMILDAD</h4>
                <p class="text-gray font-13 mb-0">Conocemos y reconocemos nuestras limitaciones, debilidades y potenciales para ponernos al servicio haciendo honor a la verdad.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-heart-o"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">PASIÓN</h4>
                <p class="text-gray font-13 mb-0">Creemos en este proyecto corporativo y realizamos nuestro trabajo con toda la fuerza, amor, ímpetu y vida de la que somos capaces.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-smile-o"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">ALEGRÍA</h4>
                <p class="text-gray font-13 mb-0">Es nuestra actitud vital; la alegría define todo lo que hacemos y somos, se manifiesta en la interacción positiva con los compañeros y con los clientes.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="fa fa-star-o"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">EXCELENCIA</h4>
                <p class="text-gray font-13 mb-0">Nuestra medida es la obtención de la calidad superior, por eso estamos en constante actualización. Queremos ofrecer el mejor servicio, la mejor atención, los mejores productos para ser productores creativos de belleza y bienestar.</p>
              </div>
            </div>    
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Clients -->
    <section class="clients bg-mingallery">
      <div class="pt-10 pb-0">
        <div class="row">
          <div class="col-md-12">
            <div class="owl-carousel-6col clients-logo transparent text-center owl-nav-top">
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-01.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-02.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-03.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-04.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-05.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-06.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-07.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-08.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-09.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img class="border-mingallery" src="/componentes/images/mingallery/small-10.jpg" alt=""></a></div>
            </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <?php include 'componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Custom script for all pages --> 
<script src="/componentes/js/custom.js"></script>

</body>
</html>