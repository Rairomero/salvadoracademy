<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Talleres | Salvador Academy</title>

  <?php 
  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  include '../componentes/header.php'; 

  $ubicacion = "programas";

  include '../componentes/navbar.php'; 
  //cabecera("inicio"); 

  ?>

  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Talleres</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>
   
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- Works Filter -->
            <!-- <div class="portfolio-filter font-alt align-center">
              <a href="#" class="active" data-filter="*">Todos</a>
              <a href="#disp" class="" data-filter=".disp">Disponibles</a>
              <a href="#finalizado" class="" data-filter=".finalizado">Finalizados</a>
            </div> -->
            <!-- End Works Filter -->

            
            <!-- Portfolio Gallery Grid -->
            <div id="grid" class="gallery-isotope grid-4 gutter clearfix">

              <?php include '../includes/funciones.php';
                    echo armarTalleres();
               ?>

            </div>
            <!-- End Portfolio Gallery Grid -->
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Call To Action -->
    <section class="bg-theme-color-2">
      <div class="container pt-10 pb-20">
        <div class="row">
          <div class="call-to-action">
            <div class="col-md-6">
              <h3 class="mt-5 mb-5 text-white vertical-align-middle"><i class="pe-7s-mail mr-10 font-48 vertical-align-middle"></i> SUSCRÍBETE A NUESTRO BOLETÍN</h3>
            </div>
            <div class="col-md-6">
              <!-- Mailchimp Subscription Form Starts Here -->
              <form id="mailchimp-subscription-form" class="newsletter-form mt-10">
                <div class="input-group">
                  <input type="email" value="" name="EMAIL" placeholder="Ingresa tu correo" class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-call">
                  <span class="input-group-btn">
                    <button data-height="45px" class="btn bg-theme-colored text-white btn-xs m-0 font-14" type="submit">Suscribir</button>
                  </span>
                </div>
              </form>
              <!-- Mailchimp Subscription Form Validation-->
              <script type="text/javascript">
                $('#mailchimp-subscription-form').ajaxChimp({
                    callback: mailChimpCallBack,
                    url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
                });

                function mailChimpCallBack(resp) {
                    // Hide any previous response text
                    var $mailchimpform = $('#mailchimp-subscription-form'),
                        $response = '';
                    $mailchimpform.children(".alert").remove();
                    if (resp.result === 'success') {
                        $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    } else if (resp.result === 'error') {
                        $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    }
                    $mailchimpform.prepend($response);
                }
              </script>
              <!-- Mailchimp Subscription Form Ends Here -->
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->

  <!-- Modal -->
<div class="modal fade" id="cursoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-theme-colored">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-white">Curso: <span id="myModalLabel"></span></h4>
      </div>
      <div class="modal-body">
        
        <span id="parte1"></span>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-colored-4px" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
  
  <!-- Footer -->
  <?php include '../componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Custom script for all pages --> 
<script src="/componentes/js/custom.js"></script>




</body>
</html>