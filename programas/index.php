<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Programas de Formación | Salvador Academy</title>

  <?php 
  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  include '../componentes/header.php'; 

  $ubicacion = "programas";

  include '../componentes/navbar.php'; 
  //cabecera("inicio"); 

  ?>

  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Programas de Formación</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: About -->
    <section class="bg-lighter">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-6">
              <h2 class="text-uppercase font-weight-600 mt-0 font-28 line-bottom">Programas de Formación <br>de <span class="text-theme-color-2 font-weight-400">Salvador Academy</span></h2>
              <h4 class="text-theme-colored">Nuestra oferta académica es de carácter profesional en el arte de la belleza.</h4>
              <p>Uno de los aspectos más interesantes de este programa es el abordaje no sólo de la formación específicamente profesional, sino también de la formación humana, porque no queremos formar máquinas, sino personas de una calidad humana impecable y conscientes de la necesidad de prestar un servicio excelente. El curso completo constará de tres módulos, que abarca tres aspectos fundamentales en el concepto, misión y visión de nuestro Grupo: </p>
            </div>
            <div class="col-md-6">
              <div class="video-popup">                
                <a href="/componentes/images/programas.jpg" data-lightbox-gallery="youtube-video">
                  <img alt="" src="/componentes/images/programas.jpg" class="img-responsive img-fullwidth">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Section: Services -->
    <section id="services">
      <div class="container">
        <!--div class="section-title">
          <div class="row">
            <div class="col-md-12">
              <h2 class="text-uppercase text-theme-colored title line-bottom">Our <span class="text-theme-color-2 font-weight-400">Features</span></h2>
            </div>
          </div>
        </div-->
        <div class="row mtli-row-clearfix">
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="pe-7s-scissors"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">El ontológico</h4>
                <p class="text-gray font-13 mb-0">Alcanzar el desarrollo humano, afectivo, psicosocial y autoestima de los estudiantes. La insistencia en el aspecto ontológico favorecerá el desarrollo integral, no sólo profesional, de las personas que se formen en nuestra academia.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="pe-7s-pen"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">El estilo salvador y sus productos</h4>
                <p class="text-gray font-13 mb-0">Lograr la identificación corporativa con el Grupo Salvador, con sus propuestas en belleza integral, los productos exclusivos que comercializa y el servicio excelente que presta en sus unidades de negocio a cada uno de sus clientes.</p>
              </div>
            </div>    
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="icon-box iconbox-theme-colored bg-white p-15 mb-30 border-1px">
              <a class="icon icon-dark border-left-theme-color-2-3px pull-left flip mb-0 mr-0 mt-5" href="#">
                <i class="pe-7s-tools"></i>
              </a>
              <div class="icon-box-details">
                <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">El de la gama de todos los servicios ofrecidos por nosotros</h4>
                <p class="text-gray font-13 mb-0">Enseñar las técnicas, herramientas y procedimientos para realizar todos los servicios que se prestan en las unidades de negocio.</p>
              </div>
            </div>    
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container pt-0">
        <div class="col-md-12 pl-0 pr-0">
          <div class="col-md-6 pl-0">
            <a class="style_prevu_kit" href="/programas/cursos.php"><img src="/componentes/images/programas/cursos.jpg"></a>
          </div>
          <div class="col-md-6 pr-0">
            <a class="style_prevu_kit" href="/programas/talleres.php"><img src="/componentes/images/programas/talleres.jpg"></a>
          </div>
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <?php include '../componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Custom script for all pages --> 
<script src="/componentes/js/custom.js"></script>

</body>
</html>