<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Ubicaciones | Salvador Academy</title>

  <?php 
  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  include 'componentes/header.php'; 

  $ubicacion = "ubicaciones";

  include 'componentes/navbar.php'; 
  //cabecera("inicio"); 

  ?>

  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Ubícanos</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Divider: Contact -->
    <section class="divider">
      <div class="container">
        <div class="row pt-30">

          <div class="col-md-4">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <img src="/componentes/images/egeon.jpg">
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box media bg-deep p-30 mb-20"> 
                  
                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-map-2 text-theme-colored pr-10"></i> <b> Salvador Academy Cecilio Acosta</b></a>
                    <div class="col-md-12 pl-0 pr-0 mb-0">
                      <p class="font-12">Av. 8A con calle 67 edificio Egeón primer piso, sector Cecilio Acosta. Maracaibo, Venezuela.</p>
                    </div>
                  </div>

                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-call text-theme-colored pr-10"></i> +58 261-797.25.96</a>
                  </div>

                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-mail text-theme-colored pr-10"></i>academy@salvadoracademy.com</a>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <img src="/componentes/images/norte.jpg">
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box media bg-deep p-30 mb-20"> 
                  
                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-map-2 text-theme-colored pr-10"></i> <b> Salvador Academy Delicias Norte</b></a>
                    <div class="col-md-12 pl-0 pr-0 mb-0">
                      <p class="font-12">Av. 15 Las Delicias C.C. Delicias Norte Local Nro. 178 Sector Delicias Norte. Maracaibo, Venezuela.</p>
                    </div>
                  </div>

                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-call text-theme-colored pr-10"></i> +58 261-797.25.96</a>
                  </div>

                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-mail text-theme-colored pr-10"></i>academy@salvadoracademy.com</a>
                  </div>

                </div>
              </div>
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <img src="/componentes/images/sortis.jpg">
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box media bg-deep p-30 mb-20"> 
                  
                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-map-2 text-theme-colored pr-10"></i> <b>Salvador Academy Panamá</b></a>
                    <div class="col-md-12 pl-0 pr-0 mb-0">
                      <p class="font-12">Sortis Hotel, Spa & Casino, calle 56 y 57 primer piso local LC-23A. Obarrio, Panamá.</p>
                    </div>
                  </div>

                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-call text-theme-colored pr-10"></i> +325 12345 65478</a>
                  </div>

                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-mail text-theme-colored pr-10"></i> academy@salvadoracademy.com</a>
                  </div>

                </div>
              </div>

            </div>
          </div>

        </div>

        <div class="col-md-4">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <img src="/componentes/images/chile.jpg">
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box media bg-deep p-30 mb-20"> 
                  
                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-map-2 text-theme-colored pr-10"></i> <b> Salvador Academy Chile</b></a>
                    <div class="col-md-12 pl-0 pr-0 mb-0">
                      <p class="font-12">Pronto en Santiago de Chile</p>
                    </div>
                  </div>

                  <!--div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-call text-theme-colored pr-10"></i> +56 123-456.78.90</a>
                  </div-->

                  <div class="media">
                    <a class="media-left pull-left flip" href="#"> <i class="pe-7s-mail text-theme-colored pr-10"></i>academy@salvadoracademy.com</a>
                  </div>

                </div>
              </div>
              
            </div>
          </div>


      </div>
    </section>
    
    <!-- Divider: Google Map -->
    <section>
      <div class="container-fluid pt-0 pb-0">
        <div class="row">

          <!-- Google Map HTML Codes -->
          <div 
            id="map-canvas-multipointer"
            data-mapstyle="default"
            data-height="500"
            data-zoom="15"
            data-marker="/componentes/images/map-marker.png">
          </div>
          <!-- Google Map Javascript Codes -->
          <script src="http://maps.google.com/maps/api/js?key=AIzaSyAYWE4mHmR9GyPsHSOVZrSCOOljk8DU9B4"></script>
          <script src="/componentes/js/google-map-init-multilocation.js"></script>

        </div>
      </div>
    </section>

    <!-- Divider: Call To Action -->
    <section class="bg-theme-color-2">
      <div class="container pt-10 pb-20">
        <div class="row">
          <div class="call-to-action">
            <div class="col-md-6">
              <h3 class="mt-5 mb-5 text-white vertical-align-middle"><i class="pe-7s-mail mr-10 font-48 vertical-align-middle"></i> SUSCRÍBETE A NUESTRO BOLETÍN</h3>
            </div>
            <div class="col-md-6">
              <!-- Mailchimp Subscription Form Starts Here -->
              <form id="mailchimp-subscription-form" class="newsletter-form mt-10">
                <div class="input-group">
                  <input type="email" value="" name="EMAIL" placeholder="Ingresa tu correo" class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-call">
                  <span class="input-group-btn">
                    <button data-height="45px" class="btn bg-theme-colored text-white btn-xs m-0 font-14" type="submit">Suscribir</button>
                  </span>
                </div>
              </form>
              <!-- Mailchimp Subscription Form Validation-->
              <script type="text/javascript">
                $('#mailchimp-subscription-form').ajaxChimp({
                    callback: mailChimpCallBack,
                    url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
                });

                function mailChimpCallBack(resp) {
                    // Hide any previous response text
                    var $mailchimpform = $('#mailchimp-subscription-form'),
                        $response = '';
                    $mailchimpform.children(".alert").remove();
                    if (resp.result === 'success') {
                        $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    } else if (resp.result === 'error') {
                        $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    }
                    $mailchimpform.prepend($response);
                }
              </script>
              <!-- Mailchimp Subscription Form Ends Here -->
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <?php include 'componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="/componentes/js/custom.js"></script>

</body>
</html>