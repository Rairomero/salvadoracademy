<div class="modal fade" id="modal-text" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-theme-colored">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-white" id="modal-label"></h4>
      </div>
      <div class="modal-body">

        <span id="cont1"></span>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-colored-4px" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>