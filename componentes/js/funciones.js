  $("#cursoModal").on('shown.bs.modal', function (event) {
      $("#parte1").html('<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>');
      var button = $(event.relatedTarget);
      var texto = button.data('texto');
      var tipo = button.data('tipo');
      if (tipo == 'c') {
				$.ajax({method: 'POST', url: "/api.php", data: {idc:texto, tipo:tipo}, success: function(result){
					$variable = JSON.parse(result);
					$contenido = $variable[0];
					$titulo = $variable[1];
				    $("#parte1").html($contenido);
				    $("#myModalLabel").html($titulo);
				}});
			}	else if (tipo == 't') {
					$.ajax({method: 'POST', url: "/api.php", data: {idc:texto, tipo:tipo}, success: function(result){
						$variable = JSON.parse(result);
						$contenido = $variable[0];
						$titulo = $variable[1];
					    $("#parte1").html($contenido);
					    $("#myModalLabel").html($titulo);
					}});
			}
	});

	$("#modal-text").on('shown.bs.modal', function (event) {
            $("#cont1").html('<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>');
            alert ('Funciona');
            //var button = $(event.relatedTarget);

			$.ajax({method: 'POST', url: "/api.php", data: {tipo:'add-curso'}, success: function(result){
				$variable = (result);
				console.log($variable);
				$contenido = $variable;
			    $("#cont1").html($contenido);
			    //$("#modal-label").html($titulo);
			}});
	});

	$("#regModal").submit(function(event) {
            var button = $(event.relatedTarget);
            var ident1 = $("#ident1").val();
            var ident2 = $("#ident2").val();
            var name = $("#name").val();
            var lastname = $("#lastname").val();
            var civil = $("#civil").val();
            var ffnn = $("#ffnn").val();
            var country = $("#country").val();
            var sexo = $("#sexo").val();
            var email = $("#email").val();
            var profesion = $("#profesion").val();
            var hphone = $("#hphone").val();
            var cphone1 = $("#cphone1").val();
            var cphone2 = $("#cphone2").val();
            var address = $("#address").val();
            var origen = 1;
            var info = [ident1, ident2, name, lastname, civil, ffnn, country, sexo, email, hphone, cphone1, cphone2, address, profesion, origen];
            var infoc = b64EncodeUnicode(JSON.stringify(info));

            $("#var").html('<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>');

			$.ajax({method: 'POST', url: "/api.php", data: {all_info:infoc, tipo:'te'}, success: function(result){
				$("#var").html(result);
				refrescame(3000);
			}});
	});

	function b64EncodeUnicode(str) {
	    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
	        return String.fromCharCode(parseInt(p1, 16))
	    }))
	}

	function b64DecodeUnicode(str) {
	    return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
	        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
	    }).join(''))
	}

	function refrescame($tiempo){
		setTimeout(function(){
			location.reload();
		},+$tiempo);
	}