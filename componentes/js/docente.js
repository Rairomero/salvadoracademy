;$("#modalInfo").on('shown.bs.modal', function (event) {
  $("#parte1").html('<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>');
  var button = $(event.relatedTarget);
  var tipo = button.data('tipo');
  if (tipo == 'verInfoCurso') { // Muestra informacion del curso para el docente
    var idhorario = button.data('id');
    $.ajax({ method: 'POST', url: "/api.php", data: { id: idhorario, tipo: tipo }, success: function(result) {
      $variable = JSON.parse(result);
      $contenido = $variable[0];
      $titulo = $variable[1];
      $("#parte1").html($contenido);
      $("#myModalLabel").html($titulo);
    }});
  } else if (tipo == 'verEstDocente') {
    var idestudiante = button.data('id');
    var iddocente = button.data('id-docente');
    $.ajax({ method: 'POST', url: "/api.php", data: { id: idestudiante, iddocente: iddocente, tipo: tipo }, success: function(result) {
      $variable = JSON.parse(result);
      $contenido = $variable[0];
      $titulo = $variable[1];
      $("#parte1").html($contenido);
      $("#myModalLabel").html($titulo);
    }});
  } else if (tipo == 'editarNotas') {
    var idestudiante = button.data('id');
    $.ajax({ method: 'POST', url: "/api.php", data: { id: idestudiante, tipo: tipo }, success: function(result) {
      $variable = JSON.parse(result);
      $contenido = $variable[0];
      $titulo = $variable[1];
      $("#parte1").html($contenido);
      $("#myModalLabel").html($titulo);
    }});
  }
});

function showContent($valor) {// Muestra el contenido según modulo seleccionado en sección de docentes
  if ($valor == 1) { // Seccion de Cursos lista los cursos del docente en primera instancia
    document.querySelector(".active-int").classList.remove('active-int');
    document.getElementById('opt-1').classList.add('active-int');
    document.getElementById('content-section').innerHTML = '<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
    document.getElementById('title-section').innerHTML = '<i class="fa fa-book"></i> Bienvenido a la sección de Cursos - Talleres';
    var userid = document.getElementById('userid').value;
    $.ajax({method: 'POST', url: "/api.php", data: { tipo: "consultarctDocente", user: userid }, success: function(result) {
      document.getElementById('content-section').innerHTML = result;
      $(document).ready( function () {
        $("#cursos").DataTable({
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'copy',
              title: null,
            }, {
              extend: 'excel',
              title: 'Salvador Academy - Listado de Cursos y Talleres',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
            },
            {
              extend: 'pdf',
              title: 'Salvador Academy - Listado de Cursos y Talleres',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            },
            {
              extend: 'print',
              title: '<div><img src="/componentes/images/logos/logo.png"></div>',
              messageTop: 'Reporte: Listado de Cursos - Talleres <br> Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            }
          ],
          "order": [[ 0, "asc" ], [2, "asc"], [1, "asc"]]
        });
      } );
    }});
  } else if ($valor == 11) { // Consulta los cursos activos asignados al docente
    document.querySelector(".active-int").classList.remove('active-int');
    document.getElementById('opt-1').classList.add('active-int');
    document.getElementById('content-section').innerHTML = '<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
    document.getElementById('title-section').innerHTML = '<i class="fa fa-book"></i> Bienvenido a la sección de Cursos - Talleres';
    var userid = document.getElementById('userid').value;
    $.ajax({method: 'POST', url: "/api.php", data: { tipo: "consultarCursosActivos", user: userid }, success: function(result) {
      document.getElementById('content-section').innerHTML = result;
      $(document).ready( function () {
        $("#cursos").DataTable({
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'copy',
              title: null,
            }, {
              extend: 'excel',
              title: 'Salvador Academy - Listado de Cursos - Talleres Activos',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
            },
            {
              extend: 'pdf',
              title: 'Salvador Academy - Listado de Cursos - Talleres Activos',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            },
            {
              extend: 'print',
              title: '<div><img src="/componentes/images/logos/logo.png"></div>',
              messageTop: 'Reporte: Listado de Cursos - Talleres Activos <br> Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            }
          ],
          "order": [[ 3, "asc" ]]
        });
      } );
    }});
  } else if ($valor == 111) {
    var idcurso = $(event.target).data('idct');
    $.ajax({ method: 'POST', url: "/api.php", data: { tipo: "formGuiaDocente", idcurso: idcurso }, success: function(result){
      document.getElementById('parte1').innerHTML = result;
    }});
  } else if ($valor == 2) { // Seccion de Estudiantes, lista los estudiantes del docente en primera instancia
    document.querySelector(".active-int").classList.remove('active-int');
    document.getElementById('opt-2').classList.add('active-int');
    document.getElementById('content-section').innerHTML = '<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
    document.getElementById('title-section').innerHTML = '<i class="fa fa-users"></i> Bienvenido a la sección de Estudiantes';
    var userid = document.getElementById('userid').value;
    $.ajax({method: 'POST', url: "/api.php", data: { tipo: "consultarEstudiantesDocente", user: userid }, success: function(result) {
      document.getElementById('content-section').innerHTML = result;
      $(document).ready( function () {
        $("#estudiantes").DataTable({
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'copy',
              title: null,
            }, {
              extend: 'excel',
              title: 'Salvador Academy - Listado de Estudiantes',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
            },
            {
              extend: 'pdf',
              title: 'Salvador Academy - Listado de Estudiantes',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            },
            {
              extend: 'print',
              title: '<div><img src="/componentes/images/logos/logo.png"></div>',
              messageTop: 'Reporte: Listado de Estudiantes<br> Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            }
          ],
          "order": [[ 0, "asc" ], [2, "asc"], [1, "asc"]]
        });
      } );
    }});
  } else if ($valor == 21) {
    document.querySelector(".active-int").classList.remove('active-int');
    document.getElementById('opt-2').classList.add('active-int');
    document.getElementById('content-section').innerHTML = '<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
    document.getElementById('title-section').innerHTML = '<i class="fa fa-users"></i> Bienvenido a la sección de Estudiantes';
    var userid = document.getElementById('userid').value;
    $.ajax({method: 'POST', url: "/api.php", data: { tipo: "consultarEstudiantesMorososDocente", user: userid }, success: function(result){
      document.getElementById('content-section').innerHTML = result;
      $(document).ready( function () {
        $("#estudiantes").DataTable({
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'copy',
              title: null,
            }, {
              extend: 'excel',
              title: 'Salvador Academy - Listado de Estudiantes',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
            },
            {
              extend: 'pdf',
              title: 'Salvador Academy - Listado de Estudiantes',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            },
            {
              extend: 'print',
              title: '<div><img src="/componentes/images/logos/logo.png"></div>',
              messageTop: 'Reporte: Listado de Estudiantes <br> Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            }
          ],
          "order": [[ 3, "asc" ]]
        });
      } );
    }});
  } else if ($valor == 3) {
    document.querySelector(".active-int").classList.remove('active-int');
    document.getElementById('opt-3').classList.add('active-int');
    document.getElementById('content-section').innerHTML = '<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
    document.getElementById('title-section').innerHTML = '<i class="fa fa-users"></i> Cargar notas del estudiante';
    var userid = document.getElementById('userid').value;
    $.ajax({method: 'POST', url: "/api.php", data: { tipo: "cargarCursosNotas", id: userid }, success: function(result){
      document.getElementById('content-section').innerHTML = result;
      $(document).ready( function () {
        $("#cursos").DataTable({
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'copy',
              title: null,
            }, {
              extend: 'excel',
              title: 'Salvador Academy - Listado de Cursos',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
            },
            {
              extend: 'pdf',
              title: 'Salvador Academy - Listado de Cursos',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            },
            {
              extend: 'print',
              title: '<div><img src="/componentes/images/logos/logo.png"></div>',
              messageTop: 'Reporte: Listado de Cursos <br> Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            }
          ],
          "order": [[ 3, "asc" ]]
        });
      } );
    }});
  } else if ($valor == 31) {
    document.querySelector(".active-int").classList.remove('active-int');
    document.getElementById('opt-3').classList.add('active-int');
    document.getElementById('content-section').innerHTML = '<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
    document.getElementById('title-section').innerHTML = '<i class="fa fa-users"></i> Cargar notas del estudiante';
    var horario = $(event.target.parentElement).data('id');
    $.ajax({method: 'POST', url: "/api.php", data: { tipo: "cargarEstudiantesNota", id: horario }, success: function(result){
      document.getElementById('content-section').innerHTML = result;
      $(document).ready( function () {
        $("#estudiantes").DataTable({
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'copy',
              title: null,
            }, {
              extend: 'excel',
              title: 'Salvador Academy - Listado de Estudiantes',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
            },
            {
              extend: 'pdf',
              title: 'Salvador Academy - Listado de Estudiantes',
              messageTop: 'Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            },
            {
              extend: 'print',
              title: '<div><img src="/componentes/images/logos/logo.png"></div>',
              messageTop: 'Reporte: Listado de Estudiantes <br> Fecha: '+ new Date().toLocaleString() +'',
              messageBottom: 'Este reporte fue generado desde el Administrador de Salvador Academy'
            }
          ],
          "order": [[ 3, "asc" ]]
        });
      } );
    }});
  }
}

function deleteFileDocente(id) {
  document.getElementById('parte1').innerHTML = '<div class="text-center"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
  $.ajax({method: 'POST', url: "/api.php", data: {tipo: "deleteFileDocente", id: id}, success: function(result){
    document.getElementById('parte1').innerHTML = result;
  }});
}
