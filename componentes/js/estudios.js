	$("#modalInfo").on('shown.bs.modal', function (event) {
    $("#body-modal").html('<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>');
    var button = $(event.relatedTarget);
    var idct = button.data('id');
    var tipo = button.data('tipo');
    if (tipo == 'verHC') { // Muestra el modal con la informacion del curso seleccionado en historial cursos
      $.ajax({method: 'POST', url: "/api.php", data: {id:idct, tipo: tipo}, success: function(result){
        $variable = JSON.parse(result);
        $contenido = $variable[0];
        $titulo = $variable[1];
        $("#body-modal").html($contenido);
        $("#title-modal").html($titulo);
      }});
    } else if (tipo == 'verPago') { // Para modificar cursos y talleres en el administrador
      $.ajax({method: 'POST', url: "/api.php", data: {id:idct, tipo: tipo}, success: function(result){
        $variable = JSON.parse(result);
        $contenido = $variable[0];
        $titulo = $variable[1];
        $("#body-modal").html($contenido);
        $("#title-modal").html($titulo);
      }});
    }
  });

  function showContent($valor){// Muestra el contenido según modulo seleccionado en sección de estudiantes
    if ($valor == 2) {
      document.querySelector(".active-int").classList.remove('active-int');
      document.getElementById('opt-2').classList.add('active-int');
      document.getElementById('title-section').innerHTML = '<i class="fa fa-info-circle"></i> Bienvenido a la Sección de Datos Personales';
      document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
      var userid = document.getElementById('userid').value;
      $.ajax({method: 'POST', url: "/api.php", data: {tipo: "datos", user: userid}, success: function(result){
        document.getElementById('content-section').innerHTML = result;
        $('form select.bfh-countries, span.bfh-countries, div.bfh-countries').each(function () {
              var $countries;
              $countries = $(this);
              if ($countries.hasClass('bfh-selectbox')) {
                  $countries.bfhselectbox($countries.data());
              }
              $countries.bfhcountries($countries.data());
            });
      }});
    } else if ($valor == 21) {
      document.getElementById('title-section').innerHTML = '<i class="fa fa-info-circle"></i> Modificar Datos Personales';
      document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
      var userid = document.getElementById('userid').value;
      $.ajax({method: 'POST', url: "/api.php", data: {tipo: "modDatos", user: userid}, success: function(result){
        document.getElementById('content-section').innerHTML = result;
        $('form select.bfh-countries, span.bfh-countries, div.bfh-countries').each(function () {
              var $countries;
              $countries = $(this);
              if ($countries.hasClass('bfh-selectbox')) {
                  $countries.bfhselectbox($countries.data());
              }
              $countries.bfhcountries($countries.data());
            });
      }});
    } else if ($valor == 3) {
      document.querySelector(".active-int").classList.remove('active-int');
      document.getElementById('opt-3').classList.add('active-int');
      document.getElementById('title-section').innerHTML = '<i class="fa fa-pencil-square-o"></i> Bienvenido a la Sección de Inscripción';
      document.getElementById('content-section').innerHTML = '<div class="p-20"><h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-20">Inscríbete y Aprende un <span class="text-theme-colored">Arte</span></h2><p>En esta sección podrás encontrar todos los programas (Cursos o Talleres) disponibles para inscribir.</p><center class="mt-20"><h5 style="font-weight: bold;"><i class="fa fa-dot-circle-o"></i> Seleccione una opción para continuar con la inscripción:</h5><div class="row"><div class="col-md-6"><a class="pointer style_prevu_kit" onclick="showContent(31)"><img src="/componentes/images/cursos/cursos-talleres1.jpg" /></a></div><div class="col-md-6"><a class="pointer style_prevu_kit" onclick="showContent(32)"><img src="/componentes/images/cursos/cursos-talleres2.jpg" /></a></div></div></center><div id="seleccion"></div><div id="infoseleccion"></div></div>';
    } else if ($valor == 31) {
      document.getElementById('title-section').innerHTML = "Cursos";
      if (document.body.contains(document.getElementById('seleccion'))) {
        document.getElementById('seleccion').innerHTML = "";
      } if (document.body.contains(document.getElementById('infoseleccion'))) {
        document.getElementById('infoseleccion').innerHTML = "";
      } if (document.body.contains(document.getElementById('boton-inscribir'))) {
        document.getElementById('boton-inscribir').innerHTML = "";
      }
      consultaCT(1); // Valor 1 para cursos
    } else if ($valor == 32) {
      document.getElementById('title-section').innerHTML = "Talleres";
      if (document.body.contains(document.getElementById('seleccion'))) {
        document.getElementById('seleccion').innerHTML = "";
      } if (document.body.contains(document.getElementById('infoseleccion'))) {
        document.getElementById('infoseleccion').innerHTML = "";
      } if (document.body.contains(document.getElementById('boton-inscribir'))) {
        document.getElementById('boton-inscribir').innerHTML = "";
      }
      consultaCT(2); // Valor 2 para talleres
    } else if ($valor == 4) {
      document.querySelector(".active-int").classList.remove('active-int');
      document.getElementById('opt-4').classList.add('active-int');
      document.getElementById('title-section').innerHTML = '<i class="fa fa-bar-chart"></i> Bienvenido a la Sección de Notas';
      document.getElementById('content-section').innerHTML = "Notas";
    } else if ($valor == 5) {
      document.querySelector(".active-int").classList.remove('active-int');
      document.getElementById('opt-5').classList.add('active-int');
      document.getElementById('title-section').innerHTML = '<i class="fa fa-credit-card"></i> Bienvenido a la Sección de Pago Online';
      document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
      var userid = document.getElementById('userid').value;
      $.ajax({method: 'POST', url: "/api.php", data: {tipo: "saldo", user: userid}, success: function(result){
        document.getElementById('content-section').innerHTML = result;
      }});
    } else if ($valor == 51) { //Mercado Pago Content
      document.getElementById('title-section').innerHTML = "Mercado Pago";
      document.getElementById('content-section').innerHTML = '<iframe height="400px" src="/componentes/mp.php"></iframe>';
    } else if ($valor == 52) { //InstaPago Content
      document.getElementById('title-section').innerHTML = "Insta Pago";
      document.getElementById('content-section').innerHTML = '<iframe height="400px" src="/componentes/ip.php"></iframe>';
    } else if ($valor == 6) {
      document.querySelector(".active-int").classList.remove('active-int');
      document.getElementById('opt-6').classList.add('active-int');
      var userid = document.getElementById('userid').value;
      document.getElementById('title-section').innerHTML = '<i class="fa fa-history"></i> Bienvenido a la Sección de Historial de Cursos';
      document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
      $.ajax({method: 'POST', url: "/api.php", data: {tipo: "hcursos", user: userid}, success: function(result){
        document.getElementById('content-section').innerHTML = result;
        $(document).ready( function () {
          $("#hcursos").DataTable();
        });
      }});
    } else if ($valor == 7) {
      document.querySelector(".active-int").classList.remove('active-int');
      document.getElementById('opt-7').classList.add('active-int');
      var userid = document.getElementById('userid').value;
      document.getElementById('title-section').innerHTML = '<i class="fa fa-money"></i> Bienvenido a la Sección de Historial de Pagos';
      document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
      $.ajax({method: 'POST', url: "/api.php", data: {tipo: "hpagos", user: userid}, success: function(result){
        document.getElementById('content-section').innerHTML = result;
        $(document).ready( function () {
          $("#hpagos").DataTable();
        });
      }});
    } else if ($valor == 10) {
      document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
      document.getElementById('title-section').innerHTML = "Cursos";
      $.ajax({method: 'POST', url: "/api.php", data: {tipo: "consultarct"}, success: function(result){
        document.getElementById('content-section').innerHTML = result;
        $(document).ready( function () {
          $("#cursos").DataTable();
        } );
      }});
    } else if ($valor == 101) {
      document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
      document.getElementById('title-section').innerHTML = "Agregar Curso";
      document.getElementById('content-section').innerHTML = '<div class="p-20"><form action="/api.php" method="POST" enctype="multipart/form-data"><div class="text-center mb-20"><h4 style="font-weight: bold;"><i class="fa fa-book pr-15"></i> Agregar: Curso - Taller</h4></div><div class="row"><div class="col-sm-6"><div class="form-group mb-10"><div class="styled-select"><label>Seleccione el tipo</label><select name="tipoct" id="tipoct" class="form-control" required=""><option value="">- Seleccione una opción -</option><option value="1">Curso</option><option value="2">Taller</option></select></div></div></div><div class="col-sm-6"><div class="form-group mb-10"><label>Nombre del curso o taller</label><input placeholder="Nombre del curso o taller" type="text" name="nombrect" id="nombrect" required="" class="form-control"></div></div><div class="col-sm-12"><div class="form-group mb-10"><label>Descripción</label><textarea name="descripcion" id="descripcion" class="form-control" rows="3" placeholder="Ingresa una breve descripción del curso o taller..."></textarea></div></div><div class="col-sm-12"><div class="form-group mb-10"><label for="imagen">Imagen del curso o taller</label><input type="file" name="image" id="image" onchange="checkSize()" accept="image/*" required=""><p class="help-block">La imagen no debe pesar más de <b>1mb</b> y debe tener una resolución de <b>435 x 320px</b>.</p></div></div></div><input type="hidden" name="tipo" value="guardarCT"><center class="mt-20"><button type="submit" name="savect" id="savect" class="btn btn-default">Guardar</button></center></form></div>';
    } else if ($valor == 102) {
      document.getElementById('title-section').innerHTML = "Agregar Taller";
      document.getElementById('content-section').innerHTML = "Agregar Taller";
    }
  }


  function consultaCT(valor){// Consulta cursos o talleres en modulo de inscripción en sección de estudiantes
    document.getElementById('seleccion').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
    $.ajax({method: 'POST', url: "/api.php", data: {tipo: "sel", option: valor}, success: function(result){
      document.getElementById('seleccion').innerHTML = result;
    }});
  }

  function mostrarInfoCT(id){ // Muestra la información del curso o taller seleccionado en modulo de inscripción en sección de estudiantes
    document.getElementById('infoseleccion').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
    $.ajax({method: 'POST', url: "/api.php", data: {tipo: "info", idcurso: id}, success: function(result){
      document.getElementById('infoseleccion').innerHTML = result;
    }});
  }

  function paymentCT(idhorario, pais){// Muestra opción de pago (1 o 2 cuotas) en módulo de inscripción en sección de estudiantes
    document.getElementById('boton-inscribir').innerHTML = '<span class="mr-30"><button onclick="confirmarPaymentCT(1, '+idhorario+', '+pais+')" type="button" class="btn btn-colored btn-lg btn-theme-color-2 text-white pr-20">Cuota Única</button></span><span><button onclick="confirmarPaymentCT(2, '+idhorario+', '+pais+')" type="button" class="btn btn-colored btn-lg btn-theme-color-2 text-white">Dos Cuotas</button><span>';
  }

  function confirmarPaymentCT(opcion, idhorario, pais){// Muestra opción de pago (1 o 2 cuotas) en módulo de inscripción en sección de estudiantes
    document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
    $.ajax({method: 'POST', url: "/api.php", data: {tipo: "monto", option: opcion, id: idhorario, moneda: pais}, success: function(result){
      document.getElementById('content-section').innerHTML = result;
    }});
  }

  function goPayment(idhorario, opcion){// Registra la deuda al estudiante sin procesar inscripcion hasta el pago
    document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
      var userid = document.getElementById('userid').value;
   $.ajax({method: 'POST', url: "/api.php", data: {tipo: "deuda", id: idhorario, option: opcion, user: userid}, success: function(result){
	      if (result === 1 || result === "1") {// Se guardó curso en DB
	      	showContent(5);
	      } else if (result === 0 || result === "0") {// No se guardó curso en DB
	      	document.getElementById('content-section').innerHTML = '<div class="alert alert-danger"><strong>¡Ha ocurrido un error inesperado!</strong> No se ha registrado el curso seleccionado, te invitamos a realizar el proceso nuevamente.</div>';
	      } else if (result === 2 || result === "2") { // Ya tiene un horario inscrito
          document.getElementById('content-section').innerHTML = '<div class="alert alert-danger"><strong>¡Ya estás registrado en este horario!</strong> Valida la información en la sección "Historial de Cursos" o "Pago Online"</div>';
        }
    }});
  }

  function payMethod(valor, idinscripcion){// Muestra el formulario de pago según método seleccionado en modulo de pago online en sección de estudiantes
    document.getElementById('content-section').innerHTML = '<div class="text-center mt-100 mb-100"><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
   if (valor == 1) {
        //document.getElementById('title-section').innerHTML = "Mercado Pago";
        window.location.replace("/estudios/pay/index.php?method=mp&sessioni="+idinscripcion);


      } else if (valor == 2) {
        document.getElementById('title-section').innerHTML = "InstaPago";

        $.ajax({
          method: 'POST', url: "/api.php", data: {tipo: "pay", option: valor, idinsc: idinscripcion}, success: function(result){
            document.getElementById('content-section').innerHTML = result;
          }
        });
      }
  }

  function isDateGreaterThanToday() {// Valida la fecha de vencimiento
    var month = document.getElementById('month').value;
    var year = document.getElementById('year').value;
    var d1 = new Date(year, month, 0);
    var today = new Date();
    if (d1 < today) {
      alert("Verifique la información ingresada");
      document.getElementById("month").value = ""
      document.getElementById("year").value = ""
    }
  }

  function instaPago(valor){// Desactiva el boton de pago despues de hacer click
    document.getElementById(""+valor+"").disabled;
    document.getElementById("btn-pagar").innerHTML = '<div class="text-center"><h5>Procesando el pago...</h5><img src="/componentes/ajax-load/ajax-loader.gif"></div>';
  }

  function notificacionCuotaA(monto){
    alert('Recuerda que al seleccionar la opción otro monto podrás cancelar el monto correspondiente en varias transacciones, sin embargo, debes alcanzar el monto deudor del curso: "'+monto.toLocaleString()+'" para que la inscripción sea válida.');
  }

  function notificacionCuotaB(monto){
    alert('Recuerda que al seleccionar la opción otro monto podrás cancelar la primera cuota en varias transacciones, sin embargo, debes alcanzar el monto deudor de la cuota 1: "'+monto.toLocaleString()+'" para que la inscripción sea válida.');
  }

  function notificacionCuotaC(monto){
    alert('Recuerda que al seleccionar la opción otro monto podrás cancelar la segunda cuota en varias transacciones, sin embargo, debes alcanzar el monto deudor de la cuota 2: "'+monto.toLocaleString()+'" para culminar el pago del curso.');
  }