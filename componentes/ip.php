<?php
  $mostrar = consultarCuotaPagar($idinscripcion, $pagar, $moneda);
?>

<form action="/api.php" onsubmit="instaPago('pay-ip');" method="POST" id="instapago" name="instapago" autocomplete="off" accept-charset="UTF-8" >
    <fieldset>
        <ul>
            <li>
              <div class="row">
                <div class="col-md-6 col-xs-6 col-xxs-12 text-right"><label>Monto:</label></div>
                <div class="col-md-6 col-xs-6 col-xxs-12 text-left">
                    <?php echo $mostrar ?>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col-md-6 col-xs-6 col-xxs-12 text-right"><label for="nombre">Nombre en la Tarjeta:</label></div>
                <div class="col-md-6 col-xs-6 col-xxs-12 text-left"><input id="nombre" name="nombre" placeholder="Nombre del Titular" required /></div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col-md-6 col-xs-6 col-xxs-12 text-right"><label for="cedula">Documento de Identificación:</label></div>
                <div class="col-md-6 col-xs-6 col-xxs-12 text-left"><input type="text" id="cedula" name="cedula" pattern="[0-9]+" placeholder="Documento de Identificación" minlength="6" maxlength="8" title="Solo se permiten numeros sin espacios" required /></div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col-md-6 col-xs-6 col-xxs-12 text-right"><label for="tdc">Numero de Tarjeta:</label></div>
                <div class="col-md-6 col-xs-6 col-xxs-12 text-left"><input type="text" id="tdc" name="tdc" pattern="[0-9]+" placeholder="4444 3333 2222 1111" minlength="15" maxlength="16" title="Solo se permiten numeros sin espacios" required /></div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col-md-6 col-xs-6 col-xxs-12 text-right"><label for="cod">Código Secreto:</label></div>
                <div class="col-md-6 col-xs-6 col-xxs-12 text-left"><input type="password" id="cod" name="cod" pattern="[0-9]+" placeholder="321" minlength="3" maxlength="3" title="Solo se permiten numeros sin espacios" required /></div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col-md-6 col-xs-6 col-xxs-12 text-right"><label for="fv">Fecha de Vencimiento:</label></div>
                <div class="col-md-6 col-xs-6 col-xxs-12 text-left"><select name="month" id="month" required>
                    <option value="">Mes</option>
                    <option value="1">01</option>
                    <option value="2">02</option>
                    <option value="3">03</option>
                    <option value="4">04</option>
                    <option value="5">05</option>
                    <option value="6">06</option>
                    <option value="7">07</option>
                    <option value="8">08</option>
                    <option value="9">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
                <select name="year" id="year" onchange="isDateGreaterThanToday();" required>
                    <option value="">Año</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                    <option value="2027">2027</option>
                    <option value="2028">2028</option>
                    <option value="2029">2029</option>
                    <option value="2030">2030</option>
                </select></div>
              </div>
            </li>
        </ul>
        <input name="tipo" value="ip" type="hidden"/>
        <input name="idinscripcion" value="<?php echo $idinscripcion ?>" type="hidden"/>
        <div class="text-center mt-10" id="btn-pagar"><input type="submit" id="pay-ip" class="btn btn-info" value="Pagar" /></div>
        <div class="text-center mt-20">Esta transacción será procesada de forma segura gracias a la plataforma de:</div>
        <div class="text-center"><img width="160px" src="/componentes/images/ip.png"></div>
    </fieldset>
</form>