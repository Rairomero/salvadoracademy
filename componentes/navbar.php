<!-- Header -->
<header id="header" class="header">
  <!-- 1st Navbar -->
    <div class="header-top bg-theme-color-2 sm-text-center p-0">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="widget no-border m-0">
              <ul class="list-inline font-13 sm-text-center mt-5">
                <!--li>
                  <a class="text-white" href="/faq.php">FAQ</a>
                </li>
                <li class="text-white">|</li-->
                <li>
                  <a class="text-white" href="/admin/login.php">Acceder</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-8">

            <div class="widget no-border m-0 mr-0 pull-right flip sm-pull-none sm-text-center">
              <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                <li><a target="_blank" href="https://www.facebook.com/academiasalvador/"><i class="fa fa-facebook text-white"></i></a></li>
                <li><a target="_blank" href="https://www.instagram.com/salvadoracademy/"><i class="fa fa-instagram text-white"></i></a></li>
                <li><a target="_blank" href="https://twitter.com/salvadoracademy"><i class="fa fa-twitter text-white"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>


            <?php 

              $opciones = "";
              switch ($ubicacion) {
                case 'inicio':
                  $opciones = '<ul class="menuzord-menu">
                                  <li class="widget no-border m-0 logo-academy"><a class="no-border p-0" href="/index.php"><img src="/componentes/images/logos/logo.png"></a></li>
                                  <li class="active"><a href="/index.php">Inicio</a></li>
                                  <li><a href="/nosotros.php">Nosotros</a></li>
                                  <li><a href="/programas/index.php">Programas de Formación</a>
                                    <ul class="dropdown">
                                      <li><a href="/programas/cursos.php">Cursos</a></li>
                                      <li><a href="/programas/talleres.php">Talleres</a></li>
                                    </ul>
                                  </li>
                                  <li><a href="/ubicaciones.php">Ubicaciones</a></li>
                                  <li><a href="/contacto.php">Contacto</a>
                                  </li>
                                </ul>';
                  break;
                  case 'nosotros':
                    $opciones = '<ul class="menuzord-menu">
                                    <li class="widget no-border m-0 logo-academy"><a class="no-border p-0" href="/index.php"><img src="/componentes/images/logos/logo.png"></a></li>
                                    <li><a href="/index.php">Inicio</a></li>
                                    <li class="active"><a href="/nosotros.php">Nosotros</a></li>
                                    <li><a href="/programas/index.php">Programas de Formación</a>
                                      <ul class="dropdown">
                                        <li><a href="/programas/cursos.php">Cursos</a></li>
                                        <li><a href="/programas/talleres.php">Talleres</a></li>
                                      </ul>
                                    </li>
                                    <li><a href="/ubicaciones.php">Ubicaciones</a></li>
                                    <li><a href="/contacto.php">Contacto</a>
                                    </li>
                                  </ul>';
                    break;
                    case 'programas':
                      $opciones = '<ul class="menuzord-menu">
                                      <li class="widget no-border m-0 logo-academy"><a class="no-border p-0" href="/index.php"><img src="/componentes/images/logos/logo.png"></a></li>
                                      <li><a href="/index.php">Inicio</a></li>
                                      <li><a href="/nosotros.php">Nosotros</a></li>
                                      <li class="active"><a href="/programas/index.php">Programas de Formación</a>
                                        <ul class="dropdown">
                                          <li><a href="/programas/cursos.php">Cursos</a></li>
                                          <li><a href="/programas/talleres.php">Talleres</a></li>
                                        </ul>
                                      </li>
                                      <li><a href="/ubicaciones.php">Ubicaciones</a></li>
                                      <li><a href="/contacto.php">Contacto</a>
                                      </li>
                                    </ul>';
                      break;
                      case 'ubicaciones':
                        $opciones = '<ul class="menuzord-menu">
                                        <li class="widget no-border m-0 logo-academy"><a class="no-border p-0" href="/index.php"><img src="/componentes/images/logos/logo.png"></a></li>
                                        <li><a href="/index.php">Inicio</a></li>
                                        <li><a href="/nosotros.php">Nosotros</a></li>
                                        <li><a href="/programas/index.php">Programas de Formación</a>
                                          <ul class="dropdown">
                                            <li><a href="/programas/cursos.php">Cursos</a></li>
                                            <li><a href="/programas/talleres.php">Talleres</a></li>
                                          </ul>
                                        </li>
                                        <li class="active"><a href="/ubicaciones.php">Ubicaciones</a></li>
                                        <li><a href="/contacto.php">Contacto</a>
                                        </li>
                                      </ul>';
                        break;
                        case 'contacto':
                          $opciones = '<ul class="menuzord-menu">
                                          <li class="widget no-border m-0 logo-academy"><a class="no-border p-0" href="/index.php"><img src="/componentes/images/logos/logo.png"></a></li>
                                          <li><a href="/index.php">Inicio</a></li>
                                          <li><a href="/nosotros.php">Nosotros</a></li>
                                          <li><a href="/programas/index.php">Programas de Formación</a>
                                            <ul class="dropdown">
                                              <li><a href="/programas/cursos.php">Cursos</a></li>
                                              <li><a href="/programas/talleres.php">Talleres</a></li>
                                            </ul>
                                          </li>
                                          <li><a href="/ubicaciones.php">Ubicaciones</a></li>
                                          <li class="active"><a href="/contacto.php">Contacto</a>
                                          </li>
                                        </ul>';
                          break;
                          case 'none':
                            $opciones = '<ul class="menuzord-menu">
                                          <li class="widget no-border m-0 logo-academy"><a class="no-border p-0" href="/index.php"><img src="/componentes/images/logos/logo.png"></a></li>
                                          <li><a href="/index.php">Inicio</a></li>
                                          <li><a href="/nosotros.php">Nosotros</a></li>
                                          <li><a href="/programas/index.php">Programas de Formación</a>
                                            <ul class="dropdown">
                                              <li><a href="/programas/cursos.php">Cursos</a></li>
                                              <li><a href="/programas/talleres.php">Talleres</a></li>
                                            </ul>
                                          </li>
                                          <li><a href="/ubicaciones.php">Ubicaciones</a></li>
                                          <li><a href="/contacto.php">Contacto</a>
                                          </li>
                                        </ul>';
                            break;
                
                default:
                  # code...
                  break;
              }

              if (session_status() === PHP_SESSION_ACTIVE) {
                if(isset($_SESSION["academy-id"])){
                  if (!empty($_SESSION["academy-id"])) {
                    echo '<!-- 2nd Navbar -->
                      <div class="header-nav">
                        <div class="header-nav-wrapper navbar-scrolltofixed bg-white border-bottom-theme-color-2-1px">
                          <div class="container">
                            <nav id="menuzord" class="menuzord bg-white pull-left flip menuzord-responsive">

                            <ul class="menuzord-menu">
                              '.$opciones.'
                            </ul>

                          </nav>
                        </div>
                      </div>
                    </div>';
                  } 
                } 
              } else {
                    echo '<!-- 2nd Navbar -->
                      <div class="header-nav">
                        <div class="header-nav-wrapper navbar-scrolltofixed bg-white border-bottom-theme-color-2-1px">
                          <div class="container">
                            <nav id="menuzord" class="menuzord bg-white pull-left flip menuzord-responsive">

                            <ul class="menuzord-menu">
                              '.$opciones.'
                            </ul>

                            <ul class="pull-right flip hidden-sm hidden-xs">
                              <li>
                                <!-- Modal: Book Now Starts -->
                                <a class="btn btn-colored btn-flat bg-theme-color-2 text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15" data-toggle="modal" data-target="#regModal">Regístrate</a>
                                <!-- Modal: Book Now End -->
                              </li>
                            </ul>

                          </nav>
                        </div>
                      </div>
                    </div>';
                  }

             ?>

</header>

<?php 

  if (session_status() === PHP_SESSION_ACTIVE) {

    if(isset($_SESSION["academy-id"])){

      if (!empty($_SESSION["academy-id"])) {

      }

    }

  } else{

?> 

<!-- Modal -->
<div class="modal fade" id="regModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-theme-colored">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-white" id="modal-label">Formulario de Registro</h4>
      </div>
      <div class="modal-body">
        <div class="p-40" id="var">
          <form id="reg-inscripcion" name="reservation_form" class="reservation-form" method="POST"><h3 class="mt-0 line-bottom mb-40">Inscríbete y Aprende un<span class="text-theme-colored font-weight-600"> Arte!</span></h3>
            <div class="row">
              <div class="st-modalform"><h4><i class="fa fa-user pr-15"></i> Datos Personales del Estudiante</h4></div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input placeholder="Número de Identificación" type="text" name="ident2" id="ident2" required="" class="form-control">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input placeholder="Nombres" type="text" name="name" id="name"  required="" class="form-control">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input placeholder="Apellidos" type="text" name="lastname" id="lastname" required="" class="form-control">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <select name="civil" id="civil" class="form-control" required="">
                      <option value="">- Estado Civil -</option>
                      <option value="1"> Soltero </option>
                      <option value="2"> Casado </option>
                      <option value="3"> Concubinato </option>
                      <option value="4"> Divorciado </option>
                      <option value="5"> Viudo </option>
                      <option value="6"> Otro </option>
                    </select>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input placeholder="Profesión" type="text" name="profesion" id="profesion" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <input name="ffnn" id="ffnn" class="form-control" type="text" placeholder="Fecha de Nacimiento" required="" aria-required="true">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <select name="country" id="country" class="form-control bfh-countries" data-country="" required="">
                  </select>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group mb-20">
                  <div class="styled-select">
                    <select name="sexo" id="sexo" class="form-control" required="">
                      <option value="">- Sexo -</option>
                      <option value="1">Femenino</option>
                      <option value="2">Masculino</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="st-modalform"><h4><i class="fa fa-phone pr-15"></i> Datos de Contacto del Estudiante</h4></div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Correo electrónico" type="email" name="email" id="email" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Teléfono de Habitación" type="text" name="hphone" id="hphone" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Teléfono Celular 1" type="text" name="cphone1" id="cphone1" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Teléfono Celular 2" type="text" name="cphone2" id="cphone2" class="form-control">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group mb-20">
                  <textarea placeholder="Dirección de Residencia" name="address" id="address" class="form-control" required="" cols="20" rows="2"></textarea>
                </div>
              </div>
            </div>
            <div class="text-right">
              <button type="button" class="btn btn-colored btn-lg btn-flat border-left-theme-colored-4px" data-dismiss="modal">Cerrar</button>
              <button type="submit" name="reg-insc" id="regModal-insc" class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-colored-4px" data-loading-text="Por favor espera...">Registrar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("#ffnn").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+0",
    });
  });
</script>

<?php

  }

?>