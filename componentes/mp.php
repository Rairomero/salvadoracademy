<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

  <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>

  <script type="text/javascript">
  	Mercadopago.setPublishableKey("TEST-7c7eac17-a257-49c6-bbfa-9f5117e2c116");
  </script>

  <form action="" method="post" id="pay" name="pay" >
    <fieldset>
        <ul>
            <li>
                <label for="email">Email</label>
                <input id="email" name="email" value="test_user_19653727@testuser.com" type="email" placeholder="your email"/>
            </li>
            <li>
                <label for="cardNumber">Credit card number:</label>
                <input type="text" id="cardNumber" data-checkout="cardNumber" placeholder="4509 9535 6623 3704" onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off />
            </li>
            <li>
                <label for="securityCode">Security code:</label>
                <input type="text" id="securityCode" data-checkout="securityCode" placeholder="123" onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off />
            </li>
            <li>
                <label for="cardExpirationMonth">Expiration month:</label>
                <input type="text" id="cardExpirationMonth" data-checkout="cardExpirationMonth" placeholder="12" onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off />
            </li>
            <li>
                <label for="cardExpirationYear">Expiration year:</label>
                <input type="text" id="cardExpirationYear" data-checkout="cardExpirationYear" placeholder="2015" onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off />
            </li>
            <li>
                <label for="cardholderName">Card holder name:</label>
                <input type="text" id="cardholderName" data-checkout="cardholderName" placeholder="APRO" />
            </li>
            <li>
                <label for="docType">Document type:</label>
                <select id="docType" data-checkout="docType"></select>
            </li>
            <li>
                <label for="docNumber">Document number:</label>
                <input type="text" id="docNumber" data-checkout="docNumber" placeholder="12345678" />
            </li>
        </ul>
        <input type="hidden" name="paymentMethodId" />
        <input type="submit" value="Pay!" />
    </fieldset>
  </form>

  <script type="text/javascript">
  	// Obtener Credenciales
  	Mercadopago.getIdentificationTypes();


  	function addEvent(el, eventName, handler){
	    if (el.addEventListener) {
	           el.addEventListener(eventName, handler);
	    } else {
	        el.attachEvent('on' + eventName, function(){
	          handler.call(el);
	        });
	    }
	};

	function getBin() {
	    var ccNumber = document.querySelector('input[data-checkout="cardNumber"]');
	    return ccNumber.value.replace(/[ .-]/g, '').slice(0, 6);
	};

	function guessingPaymentMethod(event) {
	    var bin = getBin();

	    if (event.type == "keyup") {
	        if (bin.length >= 6) {
	            Mercadopago.getPaymentMethod({
	                "bin": bin
	            }, setPaymentMethodInfo);
	        }
	    } else {
	        setTimeout(function() {
	            if (bin.length >= 6) {
	                Mercadopago.getPaymentMethod({
	                    "bin": bin
	                }, setPaymentMethodInfo);
	            }
	        }, 100);
	    }
	};

	function setPaymentMethodInfo(status, response) {
	    if (status == 200) {
	        // do somethings ex: show logo of the payment method
	        var form = document.querySelector('#pay');

	        if (document.querySelector("input[name=paymentMethodId]") == null) {
	            var paymentMethod = document.createElement('input');
	            paymentMethod.setAttribute('name', "paymentMethodId");
	            paymentMethod.setAttribute('type', "hidden");
	            paymentMethod.setAttribute('value', response[0].id);

	            form.appendChild(paymentMethod);
	        } else {
	            document.querySelector("input[name=paymentMethodId]").value = response[0].id;
	        }
	    }
	};

	addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'keyup', guessingPaymentMethod);
	addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'change', guessingPaymentMethod);

	//
	doSubmit = false;
	addEvent(document.querySelector('#pay'), 'submit', doPay);
	function doPay(event){
	    event.preventDefault();
	    if(!doSubmit){
	        var $form = document.querySelector('#pay');

	        Mercadopago.createToken($form, sdkResponseHandler); // The function "sdkResponseHandler" is defined below

	        return false;
	    }
	};

	//
	function sdkResponseHandler(status, response) {
	    if (status != 200 && status != 201) {
	        alert("verify filled data");
	    }else{
	        var form = document.querySelector('#pay');
	        var card = document.createElement('input');
	        card.setAttribute('name', 'token');
	        card.setAttribute('type', 'hidden');
	        card.setAttribute('value', response.id);
	        form.appendChild(card);
	        doSubmit=true;
	        form.submit();
	    }
	};

  </script>


</body>
</html>

<?php

	/*require_once ('mercadopago.php');

	$mp = new MP ("8756010045362715", "ZrwiOlcy2eWjTZnxndWZigsvReY9FvDk");*/
	
  # SDK

  /*require_once '../pagos/vendor/autoload.php';

  MercadoPago\SDK::setClientId("8756010045362715");
  MercadoPago\SDK::setClientSecret("ZrwiOlcy2eWjTZnxndWZigsvReY9FvDk");

  # Create an item object
  $item = new MercadoPago\Item();
  $item->id = "1234";
  $item->title = "Sleek Granite Clock";
  $item->quantity = 5;
  $item->currency_id = "VEF";
  $item->unit_price = 23.64;
  # Create a payer object
  $payer = new MercadoPago\Payer();
  $payer->email = "macey_macgyver@yahoo.com";
  # Setting preference properties
  $preference->items = array($item);
  $preference->payer = $payer;
  # Save and posting preference
  $preference->save();



  $payer = new MercadoPago\Payer();
  $payer->name = "Charles";
  $payer->surname = "Montez";
  $payer->email = "charles@gmail.com";
  $payer->date_created = "2018-06-02T12:58:41.425-04:00";
  $payer->phone = array(
    "area_code" => "",
    "number" => "960.306.745"
  );
  $payer->identification = array(
    "type" => "DNI",
    "number" => "12345678"
  );
  $payer->address = array(
    "street_name" => "Calle Ana Medina",
    "street_number" => 1363,
    "zip_code" => "34357"
  );




  $shipments = new MercadoPago\Shipments();
  $shipments->receiver_address = array(
    "zip_code" => "34357",
    "street_number" => 1363,
    "street_name" => "Calle Ana Medina",
    "floor" => 19,
    "apartment" => "C"
  );*/

?>