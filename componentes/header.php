<!-- Meta Tags -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="Salvador Academy | Educación y Cursos en Materia de Belleza" />
<meta name="keywords" content="academy, academia, curso, educación, belleza, taller, corte, secado, barberia, estilista, maquillaje, colorimetría" />
<meta name="author" content="UX Web VE" />

<!-- Favicon and Touch Icons -->
<link href="/componentes/images/favicon.png" rel="shortcut icon" type="image/png">
<link href="/componentes/images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="/componentes/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="/componentes/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="/componentes/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="/componentes/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/componentes/css/animate.css" rel="stylesheet" type="text/css">
<link href="/componentes/css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="/componentes/css/menuzord-skins/menuzord-rounded-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="/componentes/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="/componentes/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="/componentes/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="/componentes/css/responsive.css" rel="stylesheet" type="text/css">
<link href="/componentes/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<link  href="/componentes/js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="/componentes/js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="/componentes/js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="/componentes/css/colors/theme-skin-color-set-1.css" rel="stylesheet" type="text/css">

<!-- CSS | DataTables -->
<link rel="stylesheet" type="text/css" href="/componentes/DataTables/datatables.min.css"/>
<link rel="stylesheet" type="text/css" href="/componentes/DataTables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="/componentes/DataTables/Buttons-1.5.1/css/buttons.bootstrap4.min.css"/>

<!-- CSS | Custom -->
<link href="/componentes/css/custom.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/componentes/css/jquery-ui.min.css">

<!-- external javascripts -->
<script src="/componentes/js/jquery-2.2.4.min.js"></script>
<script src="/componentes/js/bootstrap.min.js"></script>
<script src="/componentes/js/jquery-plugin-collection.js"></script>
<script src="/componentes/js/jquery-ui.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="/componentes/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="/componentes/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper" class="clearfix">
  <!-- preloader -->
  <!--div id="preloader">
    <div id="spinner">
      <div class="preloader-dot-loading">
        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
      </div>
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div-->