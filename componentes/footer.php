<!-- Footer -->
  <footer id="footer" class="footer divider">
    <div class="container" style="padding-bottom: 10px; padding-top: 60px;">
      <div class="row border-bottom">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <img class="mb-5" alt="" src="/componentes/images/logos/logo.png">
            <h3><i class="fa fa-globe text-theme-color-2 mr-5"></i> <a href="#"> Panamá</a></h3>
            <p class="text-black">
              <i class="fa fa-map-marker text-theme-color-2 mr-5" aria-hidden="true"></i>Sortis Hotel, Spa & Casino, calle 56 y 57 local LC-23A Obarrio, Panamá.<br>
              <i class="fa fa-phone text-theme-color-2 mr-5"></i> <a href="#">507-388.64.55</a>
            </p>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h3 class="mt-0"><i class="fa fa-globe text-theme-color-2 mr-5"></i> <a href="#"> Venezuela</a></h3>
            <p class="text-black">
              <i class="fa fa-map-marker text-theme-color-2 mr-5" aria-hidden="true"></i>Calle 67 Cecilio Acosta con Av. 8A Edif. Egeón, Maracaibo, Venezuela.<br>
              <i class="fa fa-phone text-theme-color-2 mr-5"></i> <a href="#">0261-797.25.96</a>
            </p>
            <p class="text-black">
              <i class="fa fa-map-marker text-theme-color-2 mr-5" aria-hidden="true"></i>Av. 15 C.C. Delicias Norte 178 Sector Delicias Norte, Maracaibo, Venezuela.<br>
              <i class="fa fa-phone text-theme-color-2 mr-5"></i> <a href="#">0261-797.25.96</a>
              <!--i class="fa fa-phone text-theme-color-2 mr-5"></i> <a href="#">0261-741.00.62 / 0261-743.28.45</a-->
            </p>
          </div>
        </div>

        <!--div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h4 class="widget-title">Enlaces de Interés</h4>
            <ul class="list angle-double-right list-border">
              <li><a href="page-about-style1.html">Salvador Hairdressing</a></li>
              <li><a href="page-course-list.html">Beauty Store</a></li>
              <li><a href="page-pricing-style1.html">fs magazine</a></li>
              <li><a href="page-gallery-3col.html">Cicara Caffe</a></li>
            </ul>
          </div>
        </div-->
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h4 class="widget-title line-bottom-theme-colored-2">Horario de Atención</h4>
            <div class="opening-hours">
              <ul class="list-border text-black">
                <li class="clearfix"> <span> Lun - Vier :  </span>
                  <div class="value pull-right"> 8:00 am - 6:00 pm </div>
                </li>
                <li class="clearfix"> <span> Sáb :</span>
                  <div class="value pull-right"> 8:00 am - 4:00 pm </div>
                </li>
                <li class="clearfix"> <span> Dom : </span>
                  <div class="value pull-right"> 10:00 am - 2:00 pm </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="widget dark">
            <h4 class="widget-title line-bottom-theme-colored-2">Conecta con nosotros</h4>
            <ul class="styled-icons icon-bordered icon-sm">
              <li><a target="_blank" href="https://facebook.com/academiasalvador/"><i class="fa fa-facebook"></i></a></li>
              <li><a target="_blank" href="https://www.instagram.com/salvadoracademy/"><i class="fa fa-instagram"></i></a></li>
              <li><a target="_blank" href="https://twitter.com/salvadoracademy"><i class="fa fa-twitter"></i></a></li>
            </ul>
            <ul class="list-inline mt-5">
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-color-2 mr-5"></i> <a href="#">academy@salvadoracademy.com</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-theme-color-2 mr-5"></i> <a target="_blank" href="http://www.salvadorhairdressing.com/">www.salvadorhairdressing.com</a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom bg-black-333">
      <div class="container pt-20 pb-20">
        <div class="row">
          <div class="col-md-6">
            <p class="font-14 text-black m-0 pt-5">&copy; 2018 Salvador Academy. Todos los derechos reservados</p>
          </div>
          <div class="col-md-6 text-right">
            <div class="widget no-border m-0">
              <ul class="list-inline sm-text-center mt-5 font-12">
                <!--li>
                  <a class="text-black" href="/faq.php">FAQ</a>
                </li>
                <li class="text-black">|</li-->
                <li>
                  <a class="text-black" href="/admin/login.php">Acceder</a>
                </li>
                <!--li class="text-black">|</li>
                <li>
                  <a class="text-black" href="#">Support</a>
                </li-->
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <!-- JS - DataTables -->
  <script type="text/javascript" src="/componentes/DataTables/datatables.min.js"></script>

  <!-- Custom JS -->
  <script src="/componentes/js/funciones.js"></script>

  <!-- Form Helpers -->
  <script type="text/javascript" src="/componentes/js/formhelpers/bootstrap-formhelpers-countries.js"></script>
  <script type="text/javascript" src="/componentes/js/formhelpers/lang/es_ES/bootstrap-formhelpers-countries.es_ES.js"></script>

  <!-- Tooltip -->
  <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
  </script>