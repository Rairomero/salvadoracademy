<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Inicio | Salvador Academy</title>

  <?php 
  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  include 'componentes/header.php';

  $ubicacion = "inicio";

  include 'componentes/navbar.php'; 
  //cabecera("inicio"); 

  if($_SERVER["REQUEST_METHOD"] == "POST"){
    if($_SERVER["REQUEST_METHOD"] == "GET"){
      if(isset($_GET["e"])){
        if ($_GET["e"] == 1) {
          $mensajealert = '<div class="alert alert-danger" style="color: black!important;">El correo electrónico y/o la contraseña que has ingresado son inválidos, por favor verifica los datos e intenta nuevamente.</div>';
        }
      }
    } 
  }

  ?>

  <!-- Start main-content -->
  <div class="main-content">
  
    <!-- Section: home -->
    <section id="home">
      <div class="container-fluid p-0">
        
        <!-- Slider Revolution Start -->
        <div class="rev_slider_wrapper">
          <div class="rev_slider" data-version="5.0">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="/componentes/images/main/bg-1.jpg" data-rotate="0" data-saveperformance="off" data-title="Academia de Belleza" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/componentes/images/main/bg-1.jpg"  alt=""  data-bgposition="center 10%" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway"
                  id="rs-1-layer-1"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-50']" 
                  data-fontsize="['100']"
                  data-lineheight="['110']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:700; text-shadow: 2px 2px #000; color: #e32028!important;">ACADEMIA
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-uppercase bg-dark-transparent-6 text-white font-raleway pl-20 pr-20" 
                  id="rs-1-layer-2"

                  data-x="['left']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['30']"
                  data-fontsize="['60']"
                  data-lineheight="['70']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:500;">DE BELLEZA
                </div>
              </li>

              <!-- SLIDE 2 -->
              <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="/componentes/images/main/bg-2.jpg" data-rotate="0" data-saveperformance="off" data-title="Instalaciones Modernas" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/componentes/images/main/bg-2.jpg"  alt=""  data-bgposition="center 40%" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway pl-30 pr-30"
                  id="rs-2-layer-1"

                  data-x="['center']"
                  data-hoffset="['0']"
                  data-y="['middle']"
                  data-voffset="['-10']"
                  data-fontsize="['64']"
                  data-lineheight="['70']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:700; border-radius: 30px; color: #e32028!important; text-shadow: 2px 2px #000;"> INSTALACIONES MODERNAS
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-uppercase bg-dark-transparent-6 text-white font-raleway pl-20 pr-20" 
                  id="rs-2-layer-2"

                  data-x="['center']"
                  data-hoffset="['0']"
                  data-y="['middle']"
                  data-voffset="['40']"
                  data-fontsize="['22']"
                  data-lineheight="['32']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:500;">Comodidad y Aprendizaje para todos nuestros Estudiantes.
                </div>
              </li>

              <!-- SLIDE 3 -->
              <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="/componentes/images/main/bg-113.jpg" data-rotate="0" data-saveperformance="off" data-title="Belleza Integral" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/componentes/images/main/bg-113.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway"
                  id="rs-3-layer-1"

                  data-x="['right']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-80']" 
                  data-fontsize="['72']"
                  data-lineheight="['72']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:700; color: #e32028!important; text-shadow: 2px 2px #000;">BELLEZA INTEGRAL
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-uppercase bg-dark-transparent-6 text-white font-raleway pl-20 pr-20"
                  id="rs-3-layer-2"

                  data-x="['right']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['-25']" 
                  data-fontsize="['32']"
                  data-lineheight="['42']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:600;">VARIEDAD EN PROGRAMAS DE FORMACIÓN
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme" 
                  id="rs-3-layer-3"

                  data-x="['right']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['180']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                  data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg btn-flat btn-theme-colored btn-theme-colored pl-20 pr-20" href="/programas/index.php">Ver Programas</a>
                </div>
              </li>

            </ul>
          </div>
          <!-- end .rev_slider -->
        </div>
        <!-- end .rev_slider_wrapper -->
        <script>
          $(document).ready(function(e) {
            $(".rev_slider").revolution({
              sliderType:"standard",
              sliderLayout: "auto",
              dottedOverlay: "none",
              delay: 5000,
              navigation: {
                  keyboardNavigation: "off",
                  keyboard_direction: "horizontal",
                  mouseScrollNavigation: "off",
                  onHoverStop: "off",
                  touch: {
                      touchenabled: "on",
                      swipe_threshold: 75,
                      swipe_min_touches: 1,
                      swipe_direction: "horizontal",
                      drag_block_vertical: false
                  },
                arrows: {
                  style:"zeus",
                  enable:true,
                  hide_onmobile:true,
                  hide_under:600,
                  hide_onleave:true,
                  hide_delay:200,
                  hide_delay_mobile:1200,
                  tmp:'<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                  left: {
                    h_align:"left",
                    v_align:"center",
                    h_offset:30,
                    v_offset:0
                  },
                  right: {
                    h_align:"right",
                    v_align:"center",
                    h_offset:30,
                    v_offset:0
                  }
                },
                bullets: {
                  enable:true,
                  hide_onmobile:true,
                  hide_under:600,
                  style:"metis",
                  hide_onleave:true,
                  hide_delay:200,
                  hide_delay_mobile:1200,
                  direction:"horizontal",
                  h_align:"center",
                  v_align:"bottom",
                  h_offset:0,
                  v_offset:30,
                  space:5,
                  tmp:'<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title">{{title}}</span>'
                }
              },
              responsiveLevels: [1240, 1024, 778],
              visibilityLevels: [1240, 1024, 778],
              gridwidth: [1170, 1024, 778, 480],
              gridheight: [650, 768, 960, 720],
              lazyType: "none",
              parallax: {
                  origo: "slidercenter",
                  speed: 1000,
                  levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                  type: "scroll"
              },
              shadow: 0,
              spinner: "off",
              stopLoop: "on",
              stopAfterLoops: 0,
              stopAtSlide: -1,
              shuffle: "off",
              autoHeight: "off",
              fullScreenAutoWidth: "off",
              fullScreenAlignForce: "off",
              fullScreenOffsetContainer: "",
              fullScreenOffset: "0",
              hideThumbsOnMobile: "off",
              hideSliderAtLimit: 0,
              hideCaptionAtLimit: 0,
              hideAllCaptionAtLilmit: 0,
              debugMode: false,
              fallbacks: {
                  simplifyAll: "off",
                  nextSlideOnWindowFocus: "off",
                  disableFocusListener: false,
              }
            });
          });
        </script>
        <!-- Slider Revolution Ends -->

      </div>
    </section>

    <!-- Section:about-->
    <section>
      <div class="container pb-60">
        <div class="section-content">
          <div class="row">
            <div class="col-md-8">
              <div class="meet-doctors">
                <h2 class="text-uppercase mt-0 line-height-1">Bienvenido a la <span class="text-theme-colored">Excelencia</span></h2>
                <h6 class="text-uppercase letter-space-5 line-bottom title font-playfair text-uppercase">Salvador Academy te la da bienvenida</h6>
                <p>Disponemos de instalaciones modernas que ofrecen las mejores condiciones para facilitar la comodidad y el aprendizaje de nuestros alumnos en materia de belleza. Disponemos de una gran variedad de cursos en diferentes áreas:</p>
              </div>
              <div class="row mb-sm-30">
                <div class="col-sm-6 col-md-6">
                  <div class="icon-box p-0 mb-20">
                   <a class="icon icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip" href="#">
                    <i class="fa fa-scissors" aria-hidden="true"></i>
                   </a>
                   <div class="ml-70 ml-sm-0">
                    <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Corte</h5>
                    <p class="text-gray">Te enseñamos lo más actual en corte de cabello segun tus necesidades.</p>
                   </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6">
                  <div class="icon-box p-0 mb-20">
                   <a class="icon icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip" href="#">
                    <i class="fa fa-male" aria-hidden="true"></i>
                   </a>
                   <div class="ml-70 ml-sm-0">
                    <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Barbería</h5>
                    <p class="text-gray">Porque ellos buscan una rasurada impecable y una barba bien definida.</p>
                   </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6">
                  <div class="icon-box p-0 mb-20">
                   <a class="icon icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip" href="#">
                    <i class="fa fa-paint-brush" aria-hidden="true"></i>
                   </a>
                   <div class="ml-70 ml-sm-0">
                    <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Color</h5>
                    <p class="text-gray">Para un cabello perfectamente coloreado disponemos de cursos en colorimetría.</p>
                   </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6">
                  <div class="icon-box p-0 mb-20">
                   <a class="icon icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip" href="#">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                   </a>
                   <div class="ml-70 ml-sm-0">
                    <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Maquillaje</h5>
                    <p class="text-gray">Las mejores técnicas para perfeccionar el maquillaje.</p>
                   </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6">
                  <div class="icon-box p-0">
                   <a class="icon icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip" href="#">
                    <i class="fa fa-female" aria-hidden="true"></i>
                   </a>
                   <div class="ml-70 ml-sm-0">
                    <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Peinados</h5>
                    <p class="text-gray">Las tendencias más modernas en peinados para todo tipo de cabello.</p>
                   </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6">
                  <div class="icon-box p-0">
                   <a class="icon icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip" href="#">
                    <i class="fa fa-bed" aria-hidden="true"></i>
                   </a>
                   <div class="ml-70 ml-sm-0">
                    <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Masajes</h5>
                    <p class="text-gray">Un beneficio tanto para nuestro cuerpo como para la mente.</p>
                   </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
             <div class="p-30 mt-0 mb-20 bg-access">
              <h3 class="title-pattern mt-0 mb-0">Acceso <span class="text-theme-color-2">Estudiante</span></h3>
              <!-- Appilication Form Start-->
              <form name="estudiante" class="reservation-form mt-10" method="POST" action="api.php">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input placeholder="Correo" type="text" name="user" required="" class="form-control">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input placeholder="Clave" type="password" name="password" class="form-control" required="">
                    </div>
                  </div>
                  <h6 class="text-center">
                  <a href="/estudios/recuperar.php">¿Olvidaste tu contraseña?</a> | <a class="bs-modal-ajax-load" data-toggle="modal" data-target="#regModal" href="">¿Aún no tienes usuario?</a>
                  </h6>
                  <div class="col-sm-12">
                    <div class="form-group mb-0 mt-10">
                      <input name="origen" class="form-control" type="hidden" value="est">
                      <input name="tipo" class="form-control" type="hidden" value="login">
                      <button type="submit" name="send" class="btn btn-colored btn-theme-color-2 text-white btn-lg btn-block" data-loading-text="Cargando...">Ingresar</button>
                    </div>
                  </div>
                </div>
              </form>
              <!-- Application Form End-->
              </div>
            </div>


            <div class="col-md-4">
             <div class="p-30 mt-0 bg-access mb-10">
              <h3 class="title-pattern mt-0">Acceso <span class="text-theme-color-2">Docente</span></h3>
              <!-- Appilication Form Start-->
              <form name="docente" class="reservation-form mt-10" method="POST" action="api.php">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input placeholder="Correo" type="text" name="user" required="" class="form-control">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input placeholder="Clave" type="password" name="password" class="form-control" required="">
                    </div>
                  </div>
                  <h6 class="text-center text-white">
                  <a href="/docente/recuperar.php">¿Olvidaste tu contraseña?</a>
                  </h6>
                  <div class="col-sm-12">
                    <div class="form-group mb-0 mt-10">
                      <input name="origen" class="form-control" type="hidden" value="doc">
                      <input name="tipo" class="form-control" type="hidden" value="login">
                      <button type="submit" name="login-docente" class="btn btn-colored btn-theme-color-2 text-white btn-lg btn-block" data-loading-text="Cargando...">Ingresar</button>
                    </div>
                  </div>
                </div>
              </form>
              <!-- Application Form End-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: COURSES -->
    <section class="bg-lighter">
      <div class="container pb-40">
        <div class="section-title mb-0">
        <div class="row">
          <div class="col-md-8">
            <h2 class="text-uppercase font-28 line-bottom mt-0 line-height-1">Nuestros <span class="text-theme-color-2 font-weight-400">CURSOS Y TALLERES</span></h2>
            <h4 class="pb-20">Últimos cursos y talleres en materia de belleza.</h4>
         </div>
        </div>
        </div>
        <div class="section-content">
          <div class="row">
          
          <?php 
            include "includes/funciones.php";
            echo armarCyT();
          ?>
          
          </div>
         <div class="row text-right font-20" style="padding-right: 15px;">
           <a href="/programas/cursos.php" class="btn-read-more pr-15">Cursos</a>
           <a href="/programas/talleres.php" class="btn-read-more">Talleres</a>
         </div>
        </div>
      </div>
    </section>

    <!-- Section: Why Choose Us -->
    <section id="event">
      <div class="container">
        <div class="section-content">
         <div class="row">
            <div class="col-md-5"> 
             <img src="/componentes/images/main/why.jpg" class="img-fullwidth" alt="">
            </div>
            <div class="col-md-7 pb-sm-20">
              <h3 class="text-uppercase title line-bottom mb-20 font-28 mt-0 line-height-1">¿Por qué <span class="text-theme-color-2 font-weight-400">elegirnos</span> ?</h3>
              <p class="mb-20">Somos un equipo apasionado por la belleza, que trabaja bajo los parámetros de la excelencia. Garantizamos el profesionalismo en cada curso impartido y en cada instructor de los programas de estudio. Técnica y responsabilidad social van de la mano para garantizar que el estudiante aprenda a conjugar el arte con la solidaridad.</p>
              <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="icon-box text-center pl-0 pr-0 mb-0">
                  <a href="#" class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-md">
                    <i class="pe-7s-star text-white"></i>
                  </a>
                  <h5 class="icon-box-title mt-15 mb-10 letter-space-4 text-uppercase"><strong>Excelencia</strong></h5>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="icon-box text-center pl-0 pr-0 mb-0">
                  <a href="#" class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-md">
                    <i class="pe-7s-users text-white"></i>
                  </a>
                  <h5 class="icon-box-title mt-15 mb-10 letter-space-4 text-uppercase"><strong>Profesionalismo</strong></h5>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="icon-box text-center pl-0 pr-0 mb-0">
                  <a href="#" class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-md">
                    <i class="pe-7s-check text-white"></i>
                  </a>
                  <h5 class="icon-box-title mt-15 mb-0 letter-space-4 text-uppercase"><strong>Calidad Educativa</strong></h5>
                </div>
              </div>
             </div>
           </div>
         </div>
       </div>
    </section>

    <!-- Divider: Funfact -->
    <section class="divider parallax layer-overlay overlay-theme-colored-9" data-bg-img="http://placehold.it/1920x1280" data-parallax-ratio="0.7">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-smile mt-5 icon-count"></i>
              <h2 data-animation-duration="2000" data-value="826" class="animate-number text-white mt-0 font-38 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase mb-0">Estudiantes Satisfechos</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-note2 mt-5 icon-count"></i>
              <h2 data-animation-duration="2000" data-value="45" class="animate-number text-white mt-0 font-38 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase mb-0">Nuestros Cursos</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-users mt-5 icon-count"></i>
              <h2 data-animation-duration="2000" data-value="31" class="animate-number text-white mt-0 font-38 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase mb-0">Nuestros Profesores</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-0">
            <div class="funfact text-center">
              <i class="pe-7s-map-marker mt-5 icon-count"></i>
              <h2 data-animation-duration="2000" data-value="3" class="animate-number text-white mt-0 font-38 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase mb-0">Salvador Academy</h5>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: blog -->
    <section id="blog" class="">
      <div class="container pt-60 pb-60">
        <div class="section-title mb-0">
          <div class="row">
            <div class="col-md-8">
              <h2 class="mt-0 text-uppercase font-28 line-bottom">Últimas <span class="text-theme-color-2 font-weight-400">Noticias</span></h2>
              <h4 class="pb-20">Conoce las últimas noticias en materia de belleza.</h4>
           </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="owl-carousel-3col" data-dots="true">
              
              <?php 
                  echo noticias();
              ?>

            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Call To Action -->
    <section class="bg-theme-color-2">
      <div class="container pt-10 pb-20">
        <div class="row">
          <div class="call-to-action">
            <div class="col-md-6">
              <h3 class="mt-5 mb-5 text-white vertical-align-middle"><i class="pe-7s-mail mr-10 font-48 vertical-align-middle"></i> SUSCRÍBETE A NUESTRO BOLETÍN</h3>
            </div>
            <div class="col-md-6">
              <!-- Mailchimp Subscription Form Starts Here -->
              <form id="mailchimp-subscription-form" class="newsletter-form mt-10">
                <div class="input-group">
                  <input type="email" value="" name="EMAIL" placeholder="Ingresa tu correo" class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-call">
                  <span class="input-group-btn">
                    <button data-height="45px" class="btn bg-theme-colored text-white btn-xs m-0 font-14" type="submit">Suscribir</button>
                  </span>
                </div>
              </form>
              <!-- Mailchimp Subscription Form Validation-->
              <script type="text/javascript">
                $('#mailchimp-subscription-form').ajaxChimp({
                    callback: mailChimpCallBack,
                    url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
                });

                function mailChimpCallBack(resp) {
                    // Hide any previous response text
                    var $mailchimpform = $('#mailchimp-subscription-form'),
                        $response = '';
                    $mailchimpform.children(".alert").remove();
                    if (resp.result === 'success') {
                        $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    } else if (resp.result === 'error') {
                        $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    }
                    $mailchimpform.prepend($response);
                }
              </script>
              <!-- Mailchimp Subscription Form Ends Here -->
            </div>
          </div>
        </div>
      </div>
    </section>
    
  <!-- end main-content -->
  </div>

  <?php include 'componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="/componentes/js/custom.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="/componentes/js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="/componentes/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="/componentes/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="/componentes/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/componentes/js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="/componentes/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/componentes/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="/componentes/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/componentes/js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="/componentes/js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="/componentes/js/jquery-ui.min.js"></script>

</body>
</html>