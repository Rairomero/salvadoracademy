<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Iniciar Sesión | Salvador Academy</title>

  <?php 

  include '../componentes/header.php'; 

  $ubicacion = "none";

  include '../componentes/navbar.php'; 
  //cabecera("inicio"); 

  if($_SERVER["REQUEST_METHOD"] == "GET"){
    if(isset($_GET["e"])){
      if ($_GET["e"] == 1) {
        $mensajealert = '<div class="alert alert-danger" style="color: black!important;">El correo electrónico y/o la contraseña que has ingresado son inválidos, por favor verifica los datos e intenta nuevamente.</div>';
      } elseif ($_GET["e"] == 9) {
        $mensajealert = '<div class="alert alert-danger" style="color: black!important;">Actualmente tu usuario se encuentra inactivo, para mayor información comunícate con la dirección de la Academia.</div>';
      } elseif ($_GET["e"] == 4) {
        $mensajealert = '<div class="alert alert-danger"><strong>Error:</strong> El cambio de contraseña no fue exitoso, intenta nuevamente.</div>';
      } elseif ($_GET["e"] == 5) {
        $mensajealert = '<div class="alert alert-success"><strong>Operación Exitosa:</strong> Recibirás un mensaje a tu correo con la nueva contraseña.</div>';
      }
    }
  }

  ?>
  
  <!-- Start main-content -->
  <div class="main-content">

    <section class="common-background">
      <div class="container">
        <div class="section-content" >
          <div class="row">
            <div class="card-academy">        
              <form action="/api.php" method="post">
                <div class="icon-box text-center mb-30 p-0">
                  <h4><b><i class="fa fa-sign-in" style="font-size: 20px;"></i> Iniciar Sesión</b><br><small>Administrador - Docente</small></h4>
                  <?php if (isset($mensajealert)) { echo $mensajealert; } ?>
                </div>
                <!-- <hr> -->
                <div class="row text-center">
                  <div class="form-group mb-20 w-70">
                    <i class="fa fa-envelope" style="position: absolute; padding: 8px;"></i>
                    <input name="user" class="modern-input" type="text" placeholder="Correo Electrónico">
                  </div>
                  <div class="form-group w-70">
                  </div>
                  <div class="form-group w-70">
                    <i class="fa fa-lock" style="position: absolute; padding: 8px;"></i>
                    <input name="password"  class="modern-input" type="password" placeholder="Contraseña">
                  </div>
                </div>
                <div class="clear text-center pt-10">
                  <a class="text-theme-colored font-weight-600 font-11" href="recuperar.php">¿Olvidaste tu contraseña?</a>
                </div>
                <div class="form-group text-center mt-20">
                  <input name="origen" class="form-control" type="hidden" value="doc">
                  <input name="tipo" class="form-control" type="hidden" value="login">
                  <button class="btn btn-colored btn-theme-color-2 btn-sm text-white" type="submit">Entrar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
    <?php include '../componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Custom script for all pages --> 
<script src="/componentes/js/custom.js"></script>

</body>
</html>