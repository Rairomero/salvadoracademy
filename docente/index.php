<?php
  include '../componentes/seguro.php';

  if (isset($_SESSION["acesso"])) {
    if ($_SESSION["acesso"] != "doc") {
      header("location: ../docente/login.php");
    }
  } else {
    header("location: /login.php");
  }
?>
<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Profesor | Salvador Academy</title>

  <?php

  include '../componentes/header.php';

  $ubicacion = "docente";

  include '../componentes/navbar.php';
  //cabecera("inicio");

  include '../includes/respuestas.php';

  ?>

  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Docente</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-sx-12 col-sm-4 col-md-3">
              <div class="info">
                <div class="list-group">
                  <li class="list-group-item" style="background-color: #eee;">
                    <strong><i class="fa fa-user"></i> <?php echo $usernombre ." ". $userapellido ?></strong>
                    <input type="hidden" id="userid" value="<?php echo $userid; ?>">
                  </li>
                  <a href="" class="list-group-item pointer active-int"><i class="fa fa-home"></i> Inicio</a>
                  <a onclick="showContent(1);" id="opt-1" class="list-group-item pointer"><i class="fa fa-book"></i> Cursos - Talleres</a>
                  <a onclick="showContent(2);" id="opt-2" class="list-group-item pointer"><i class="fa fa-users"></i> Estudiantes</a>
                  <a onclick="showContent(3);" id="opt-3" class="list-group-item pointer"><i class="fa fa-pencil-square-o"></i> Cargar Notas</a>
                  <a href="logout.php" class="list-group-item pointer"><i class="fa fa-sign-out"></i> Cerrar Sesión</a>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9">
              <div class="list-group">
                <li class="list-group-item" style="background-color: #eee;"><strong><span id="title-section"> Bienvenido: <?php echo $usernombre ." ". $userapellido ?></span></strong></li>
                <li class="list-group-item pt-20 pb-20">
                  <span id="content-section">
                    <?php if (isset($mensaje)) { echo $mensaje; } ?>
                    <h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-40">Salvador <span class="text-theme-colored">Academy</span></h2>
                  </span>
                </li>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->

  <!-- Modal -->
  <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-theme-colored">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-white"><span id="myModalLabel"></span></h4>
        </div>
        <div class="modal-body">
          <span id="parte1"></span>
        </div>
        <div class="modal-footer">
          <span id="opcion-btn"></span>
          <button type="button" class="btn btn-default btn-lg btn-flat" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
    <?php include '../componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="/componentes/js/custom.js"></script>
<script src="/componentes/js/docente.js?v=1.4"></script>

</body>
</html>