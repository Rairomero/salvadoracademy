<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Recuperar Contraseña - Docente | Salvador Academy</title>

  <?php 

  include '../componentes/header.php'; 

  $ubicacion = "none";

  include '../componentes/navbar.php'; 

  if($_SERVER["REQUEST_METHOD"] == "GET"){
    if(isset($_GET["e"])){
      if ($_GET["e"] == 8) {
        $mensajealert = "<div class='alert alert-danger'><strong>Error:</strong> El correo electrónico no coincide con el documento de identificación ingresado.</div>";
      } elseif ($_GET["e"] == 9) {
        $mensajealert = "<div class='alert alert-danger'><strong>Error:</strong> El documento de identificación ingresado no está registrado.</div>";
      }
    }
  }

  ?>
  
  <!-- Start main-content -->
  <div class="main-content">

    <section class="common-background">
      <div class="container">
        <div class="section-content" >
          <div class="row">
            <div class="card-academy">        
              <form action="/api.php" method="post">
                <div class="icon-box text-center mb-30 p-0">
                  <h4><b><i class="fa fa-key" style="font-size: 20px;"></i> Recuperar Contraseña</b><br><small>Administrador - Docente</small></h4>
                  <?php if (isset($mensajealert)) { echo $mensajealert; } ?>
                </div>
                <!-- <hr> -->
                <div class="row text-center">
                  <div class="form-group mb-20 w-70">
                    <i class="fa fa-id-card-o" style="position: absolute; padding: 8px;"></i>
                    <input name="documento" class="modern-input" type="text" placeholder="Documento de Identificación">
                  </div>
                  <div class="form-group w-70">
                  </div>
                  <div class="form-group w-70">
                    <i class="fa fa-envelope" style="position: absolute; padding: 8px;"></i>
                    <input name="email"  class="modern-input" type="email" placeholder="Correo Electrónico">
                  </div>
                </div>
                <div class="clear text-center pt-10">
                  <a class="text-theme-colored font-weight-600 font-11" href="login.php">Inicio de Sesión</a>
                </div>
                <div class="form-group text-center mt-20">
                  <input name="origen" class="form-control" type="hidden" value="doc">
                  <input name="tipo" class="form-control" type="hidden" value="recover">
                  <button class="btn btn-colored btn-theme-color-2 btn-sm text-white" type="submit">Entrar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
      <?php include '../componentes/footer.php'; ?>

    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
  <!-- end wrapper --> 

  <!-- Footer Scripts --> 
  <!-- JS | Custom script for all pages --> 
  <script src="/componentes/js/custom.js"></script>

  </body>
  </html>