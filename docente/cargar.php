<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Carga de Notas | Salvador Academy</title>

  <?php 

  include '../componentes/header.php'; 

  $ubicacion = "none";

  include '../componentes/navbar.php'; 
  //cabecera("inicio"); 

  ?>

  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Carga de Notas</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>
      
    <!-- Section: Doctor Details -->
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active">
                    <h3 class="title pl-8 mb-40">Nombre del Curso</h3>
                    <table class="table table-hover">
                       <thead>
                          <tr>
                             <th>Estudiante</th>
                             <th>Cédula de Identidad</th>
                             <th>Teléfono</th>
                             <th>Estado</th>
                             <th>Nota Definitiva</th>
                          </tr>
                       </thead>
                       <tbody>
                          <tr>
                             <th scope="row">Nombre Apellido</th>
                             <td>24252016</td>
                             <td>04240000000</td>
                             <td>Solvente</td>
                             <td><input type="text" name="nota"></td>
                          </tr>
                          <tr>
                             <th scope="row">Nombre Apellido</th>
                             <td>24252016</td>
                             <td>04240000000</td>
                             <td>Solvente</td>
                             <td><input type="text" name="nota"></td>
                          </tr>
                          <tr>
                             <th scope="row">Nombre Apellido</th>
                             <td>24252016</td>
                             <td>04240000000</td>
                             <td>Solvente</td>
                             <td><input type="text" name="nota"></td>
                          </tr>
                          <tr>
                             <th scope="row">Nombre Apellido</th>
                             <td>24252016</td>
                             <td>04240000000</td>
                             <td>En Mora</td>
                             <td><input type="text" name="nota"></td>
                          </tr>
                       </tbody>
                    </table>
                    <div class="text-right"><button class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-colored-4px">Cargar</button></div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
    <?php include '../componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Custom script for all pages --> 
<script src="/componentes/js/custom.js"></script>

</body>
</html>