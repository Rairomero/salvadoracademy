<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Noticias | Salvador Academy</title>

  <?php 

  include '../componentes/header.php'; 

  $ubicacion = "none";

  include '../componentes/navbar.php'; 

  if($_SERVER["REQUEST_METHOD"] == "GET"){
    if(isset($_GET["id"])){
      $idnoticia = $_GET["id"];
    }
  } 

  ?>

  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Noticias</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>
   
    <!-- Section: Blog -->
    <section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">
          <div class="col-md-9">
            <div class="blog-posts single-post">
              
              <?php 
                  include "../includes/funciones.php";
                  echo infonoticia($idnoticia);
              ?>

            </div>
          </div>
          <div class="col-md-3">
            <div class="sidebar sidebar-right mt-sm-30">
              <div class="widget">
                <h5 class="widget-title line-bottom">Noticias Recientes</h5>
                <div class="latest-posts">
                  <?php 
                    echo noticiasrecientes();
                  ?>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Instagram</h5>
                <script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/bbb6e265a3ca55cd90d8993c9b5803f8.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> 

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <?php include '../componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Custom script for all pages --> 
<script src="/componentes/js/custom.js"></script>

</body>
</html>