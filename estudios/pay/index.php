<?php include_once '../../componentes/seguro.php';

      include_once '../../includes/conexion.php';
      include_once '../../includes/estudios.php';

  $vista = 0;
  if($_SERVER["REQUEST_METHOD"] == "GET"){
    if(isset($_GET["sessioni"])){
      $idinscripcion = $_GET["sessioni"];
      $vista = 1;

      $variables = selectPayment(1, $idinscripcion);
      $pagar = $variables[0];
      $moneda = $variables[1];

      $mostrar = consultarCuotaPagar($idinscripcion, $pagar, $moneda);

    } else{
      $mostrar = "Hubo un error al procesar los parámetros.";
    }
  } else if($_SERVER["REQUEST_METHOD"] == "POST"){

    $error = 0;
    if(isset($_POST["opc-pago"])){
      if(!empty($_POST["opc-pago"])){
        $pago1= $_POST["opc-pago"];       
      } else {$error = 1;}
    } else {$error = 1;}

    if(isset($_POST["idinscripcion"])){
      if(!empty($_POST["idinscripcion"])){
        $idinsc= $_POST["idinscripcion"];       
      } else {$error = 1;}
    } else {$error = 1;}

    if(isset($_POST["monto"])){
      $monto = $_POST["monto"];       
    } else {$error = 1;}

    if($error == 0){
      $vista = 2;
      if($pago1 == "otro"){
        $montototal = intval($monto);
      } else {
        $montototal = intval($pago1);
      }

    }
  }

?>


<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Estudiante | Salvador Academy</title>

  <?php 

  include_once '../../componentes/header.php'; 

  $ubicacion = "none";

  include_once '../../componentes/navbar.php'; 
  //cabecera("inicio"); 

  //include '../componentes/mercadopago.php';
  include_once '../../includes/respuestas.php' ;

  list($n1) = explode(" ", $usernombre);
  list($a1) = explode(" ", $userapellido);

  ?>

  <style type="text/css">
    .icon-course{
      padding-left: 6%;
    }
  </style>

  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Mi Cuenta</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>
      
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-sx-12 col-sm-4 col-md-3">
              <div class="info">
                <div class="list-group">
                  <li class="list-group-item" style="background-color: #eee;">
                    <strong><i class="fa fa-user"></i> <?php echo $n1 ." ". $a1 ?></strong>
                    <input type="hidden" id="userid" value="<?php echo $userid; ?>">
                  </li>
                  <a id="opt-1" href="../index.php" class="list-group-item pointer"><i class="fa fa-home"></i> Inicio</a>
                  <a onclick="showContent(2);" id="opt-2" class="list-group-item pointer"><i class="fa fa-info-circle"></i> Datos Personales</a>
                  <a onclick="showContent(3);" id="opt-3" class="list-group-item pointer"><i class="fa fa-pencil-square-o"></i> Inscripción</a>
<!--              <a onclick="showContent(4);" id="opt-4" class="list-group-item pointer"><i class="fa fa-bar-chart"></i> Notas</a>
 -->              <a onclick="showContent(5);" id="opt-5" class="list-group-item pointer active-int"><i class="fa fa-credit-card"></i> Pago Online</a>
                  <a onclick="showContent(6);" id="opt-6" class="list-group-item pointer"><i class="fa fa-history"></i> Historial de Cursos</a>
                  <a onclick="showContent(7);" id="opt-7" class="list-group-item pointer"><i class="fa fa-money"></i> Historial de Pagos</a>
                  <a href="logout.php" class="list-group-item pointer"><i class="fa fa-sign-out"></i> Cerrar Sesión</a>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9">
              <div class="list-group">
                <li class="list-group-item" style="background-color: #eee;"><strong><span id="title-section"><i class="fa fa-home"></i> Bienvenido: <?php echo $usernombre ." ". $userapellido ?></span></strong></li>
                <li class="list-group-item p-20">
                  <span id="content-section">
                    <?php if($vista == 1){ ?>
                    <form action="index.php" method="POST" id="mercadopago" name="mercadopago" autocomplete="off" accept-charset="UTF-8" >
                      <fieldset>
                          <ul>
                              <li>
                                <div class="row">
                                  <div class="col-md-6 col-xs-6 col-xxs-12 text-right"><label>Monto:</label></div>
                                  <div class="col-md-6 col-xs-6 col-xxs-12 text-left">
                                      <?php echo $mostrar ?>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          <input name="idinscripcion" value="<?php echo $idinscripcion ?>" type="hidden"/>
                          <div class="text-center mt-10" id="btn-pagar"><input type="submit" id="pay-mp" class="btn btn-info" value="Continuar" /></div>
                          <div class="text-center mt-20">Esta transacción será procesada de forma segura gracias a la plataforma de:</div>
                          <div class="text-center"><img width="160px" src="/componentes/images/mp.png"></div>
                      </fieldset>
                    </form>
                    <?php } else if($vista == 2){ ?>

                      <form action="procesar.php" method="POST">
                        <input name="idinscripcion" value="<?php echo $idinsc ?>" type="hidden"/>
                        <input name="montototal" value="<?php echo $montototal ?>" type="hidden"/>
                        <input name="tipopago" value="<?php echo $pago1 ?>" type="hidden"/>

                        <script
                          src="https://www.mercadopago.com.ve/integrations/v1/web-tokenize-checkout.js"
                          data-public-key="TEST-7c7eac17-a257-49c6-bbfa-9f5117e2c116"
                          data-transaction-amount="<?php echo $montototal; ?>">
                        </script>
                      </form>

                    <?php } ?>
                  </span>
                </li>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->

    <!-- Modal -->
<div class="modal fade" id="cursoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-theme-colored">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-white">Curso: <span id="myModalLabel"></span></h4>
      </div>
      <div class="modal-body">
        
        <span id="parte1"></span>
        
      </div>
      <div class="modal-footer">
        <a onclick="showContent(3);" class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-colored-4px pointer" data-dismiss="modal">Inscribir</a>
        <button type="button" class="btn btn-default btn-lg btn-flat" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-theme-colored">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-white"><span id="title-modal"></span></h4>
        </div>
        <div class="modal-body">
          <span id="body-modal"></span>
        </div>
        <div class="modal-footer">
          <span id="opcion-btn"></span>
          <button type="button" class="btn btn-default btn-lg btn-flat" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Footer -->
    <?php include_once '../../componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Custom script for all pages --> 
<script src="/../componentes/js/custom.js"></script>
<script src="/../componentes/js/estudios.js?ver=1.4.1"></script>

</body>
</html>