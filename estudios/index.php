<?php 
  include_once '../componentes/seguro.php';

  if (isset($_SESSION["acesso"])) {
    if ($_SESSION["acesso"] != "est") {
      header("location: ../estudios/login.php");
    }
  } else {
    header("location: /login.php");
  }
 ?>
<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Estudiante | Salvador Academy</title>

  <?php 

  include_once '../componentes/header.php'; 

  $ubicacion = "estudios";

  include_once '../componentes/navbar.php';
  //cabecera("inicio"); 

  //include '../componentes/mercadopago.php';
  include_once '../includes/respuestas.php' ;

  list($n1) = explode(" ", $usernombre);
  list($a1) = explode(" ", $userapellido);

  ?>

  <style type="text/css">
    .icon-course{
      padding-left: 6%;
    }
  </style>

  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="/componentes/images/main/prueba.jpg">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Mi Cuenta</h2>
              <h6 class="text-uppercase letter-space-5 font-playfair text-uppercase text-white mb-40">Salvador Academy</h6>
            </div>
          </div>
        </div>
      </div>
    </section>
      
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-sx-12 col-sm-4 col-md-3">
              <div class="info">
                <div class="list-group">
                  <li class="list-group-item" style="background-color: #eee;">
                    <strong><i class="fa fa-user"></i> <?php echo $n1 ." ". $a1 ?></strong>
                    <input type="hidden" id="userid" value="<?php echo $userid; ?>">
                  </li>
                  <a href="" id="opt-1" class="list-group-item pointer active-int"><i class="fa fa-home"></i> Inicio</a>
                  <a onclick="showContent(2);" id="opt-2" class="list-group-item pointer"><i class="fa fa-info-circle"></i> Datos Personales</a>
                  <a onclick="showContent(3);" id="opt-3" class="list-group-item pointer"><i class="fa fa-pencil-square-o"></i> Inscripción</a>
<!--              <a onclick="showContent(4);" id="opt-4" class="list-group-item pointer"><i class="fa fa-bar-chart"></i> Notas</a>
 -->              <a onclick="showContent(5);" id="opt-5" class="list-group-item pointer"><i class="fa fa-credit-card"></i> Pago Online</a>
                  <a onclick="showContent(6);" id="opt-6" class="list-group-item pointer"><i class="fa fa-history"></i> Historial de Cursos</a>
                  <a onclick="showContent(7);" id="opt-7" class="list-group-item pointer"><i class="fa fa-money"></i> Historial de Pagos</a>
                  <a href="logout.php" class="list-group-item pointer"><i class="fa fa-sign-out"></i> Cerrar Sesión</a>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9">
              <div class="list-group">
                <li class="list-group-item" style="background-color: #eee;"><strong><span id="title-section"><i class="fa fa-home"></i> Bienvenido: <?php echo $usernombre ." ". $userapellido ?></span></strong></li>
                <li class="list-group-item p-20">
                  <span id="content-section">
                    <?php if (isset($mensaje)) { echo $mensaje; } ?>
                    <h2 class="text-uppercase font-26 line-bottom mt-0 line-height-1 mb-10">Inscríbete y Aprende un <span class="text-theme-colored">Arte</span></h2>
                    <center>
                      <h5 class="mt-30 mb-20"><b>Últimos programas añadidos</b></h5>
                      <div id="grid" class="gallery-isotope grid-3 gutter clearfix mb-20"><?php include_once '../includes/estudios.php'; echo ultimosCyT(); ?></div>
                    </center>
                  </span>
                </li>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->

    <!-- Modal -->
<div class="modal fade" id="cursoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-theme-colored">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-white">Curso: <span id="myModalLabel"></span></h4>
      </div>
      <div class="modal-body">
        
        <span id="parte1"></span>
        
      </div>
      <div class="modal-footer">
        <a onclick="showContent(3);" class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-colored-4px pointer" data-dismiss="modal">Inscribir</a>
        <button type="button" class="btn btn-default btn-lg btn-flat" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-theme-colored">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-white"><span id="title-modal"></span></h4>
        </div>
        <div class="modal-body">
          <span id="body-modal"></span>
        </div>
        <div class="modal-footer">
          <span id="opcion-btn"></span>
          <button type="button" class="btn btn-default btn-lg btn-flat" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Footer -->
    <?php include_once '../componentes/footer.php'; ?>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Custom script for all pages --> 
<script src="/componentes/js/custom.js"></script>
<script src="/componentes/js/estudios.js?ver=1.4"></script>

</body>
</html>