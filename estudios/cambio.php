<?php include'../componentes/seguro.php' ?>
<!DOCTYPE html>
<html lang="es">
<head>

<!-- Page Title -->
<title>Cambiar Contraseña | Salvador Academy</title>

  <?php 

  include '../componentes/header.php'; 

  $ubicacion = "none";

  include '../componentes/navbar.php'; 

  if($_SERVER["REQUEST_METHOD"] == "GET"){
    if(isset($_GET["e"])){
      if ($_GET["e"] == 7) {
        $mensaje = "<div class='alert alert-danger'>La nueva contraseña no coincide, intenta nuevamente.</div>";
      } elseif ($_GET["e"] == 6) {
        $mensaje = "<div class='alert alert-danger'>La contraseña actual no coincide con la información ingresada.</div>";
      } elseif ($_GET["e"] == 99) {
        $mensaje = "<div class='alert alert-danger'>La información que has ingresado no fue encontrada, te invitamos a registrarte en la página de inicio.</div>";
      } elseif ($_GET["e"] == 2) {
        $mensaje = "<div class='alert alert-danger'>Para continuar, debes cambiar tu contraseña.</div>";
      }
    }
  }

  ?>

  <!-- Start main-content -->
  <div class="main-content">
   
    <section class="common-background">
      <div class="container">
        <div class="section-content" >
          <div class="row">
            <div class="card-academy">        
              <form action="/api.php" method="post">
                <div class="icon-box text-center mb-20 p-0">
                  <h4><i class="fa fa-lock" style="font-size: 20px;"></i> Cambiar Contraseña</h4>
                  <p>Por tu seguridad, te recomendamos cambiar tú contraseña actual.</p>
                  <?php if (isset($mensaje)) { echo $mensaje; } ?>
                </div>
                <!-- <hr> -->
                <div class="row text-center">
                  <div class="form-group w-70">
                    <i class="fa fa-unlock" style="position: absolute; padding: 8px;"></i><input name="old_password" class="modern-input" type="password" placeholder="Contraseña Actual">
                  </div>
                  <div class="form-group w-70">
                    <i class="fa fa-lock" style="position: absolute; padding: 8px;"></i><input name="new_password_1" class="modern-input" type="password" placeholder="Nueva Contraseña">
                  </div>
                  <div class="form-group w-70">
                    <i class="fa fa-lock" style="position: absolute; padding: 8px;"></i><input name="new_password_2"  class="modern-input" type="password" placeholder="Repetir Nueva Contraseña">
                  </div>
                </div>
                <div class="form-group text-center mt-20">
                  <input type="hidden" name="tipo" value="change" >
                  <input type="hidden" name="origen" value="est" >
                  <input type="hidden" name="userid" value="<?php echo $userid ?>" >
                  <button class="btn btn-colored btn-theme-color-2 btn-sm text-white" type="submit">Guardar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
      <?php include '../componentes/footer.php'; ?>

    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
  <!-- end wrapper --> 

  <!-- Footer Scripts --> 
  <!-- JS | Custom script for all pages --> 
  <script src="/componentes/js/custom.js"></script>

  </body>
  </html>